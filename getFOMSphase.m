function phase = getFOMSphase(F,ME,nu)
% getFOMSphase creates an orthogonal phase array for identifying MIMO systems...
% 
%      phase = getFOMSphase(F,ME,nu);
% 
% In the following, we use F to indicate the amount of tones in the multisine. Length
% of the output cell arrays is entirely  user-defined and can be given to function
% by modifying the  order of one the input. The cell length will be equal to 2*(prod(order))-1.
% The factor 2 being present because both negative and positive BHA have to be extracted.
% Can be used for MIMO systems with an arbitrary number of inputs. The obtained Multisines
% are nicely orthogonal and hence are referred to in the literature  as 'Full Orthogonal
% Multisines'.
%
% Example: DFT matrix for nu=3             
% [R1a R1b R1c]                [1      1               1       ]
% [R2a R2b R2c] = (1/sqrt(nu))*[1 exp(1j*2*pi/3) exp(1j*4*pi/3)]
% [R3a R3b R3c]                [1 exp(1j*4*pi/3) exp(1j*8*pi/3)]
%
% Required inputs:
% - F      check: @isinteger
%      Number of frequency bins for which orthogonal phase is requested
% - ME      check:  @isinteger
%      Realizations X Nr. of Experiments
% - nu      check: @isinteger
%      Number of input ports of the system
% 
% Outputs:
% - phase      Type: Array of [F X ME X nu]
%      Orthogonal phase array.
% 
% FOMS are made using the DFT matrix as the backbone: http://en.wikipedia.org/wiki/DFT_matrix
% 
% Piet Bronders, ELEC, VUB
% 
% Version info:
%  14-05-2015  Version 1.
% 
% This documentation was generated with the generateFunctionHelp function at 14-May-2015

%% Parse the input
p = inputParser();
p.FunctionName = 'getFOMSphase';

% Number of frequency bins for which orthogonal phase is requested
p.addRequired('F',@isscalar);

% Realizations X Nr. of Experiments 
p.addRequired('ME', @isscalar);

% Number of input ports of the system
p.addRequired('nu',@isscalar);

p.StructExpand = true;
p.parse(F,ME,nu);
args = p.Results();

%% Create the orthogonal phase array
if(mod(args.ME,args.nu)~=0)
    error('ME should be a multiple of nu!')
end

phase=zeros(args.F,args.ME,args.nu);

for ii=1:ME/nu
    ph = 2*pi*rand(args.nu,args.F); %Phaseshift changes depending on input (Rows)
    ph_extra = 2*pi*rand(args.nu,args.F); %Extra phaseshift per experiment (Columns)

    for jj=1:args.nu %Columns of excitation matrix
      for kk=1:args.nu %Rows of excitation matrix
          phase(:,args.nu*ii+(jj-args.nu),kk) = ph(kk,:)+ph_extra(jj,:);
          phase(:,args.nu*ii+(jj-args.nu),kk) = phase(:,args.nu*ii+(jj-args.nu),kk)+2*pi*(kk-1)*(jj-1)/args.nu;
      end
    end   
end

end

% @generateFunctionHelp
% @author Piet Bronders
% @institution ELEC, VUB

% @tagline creates an orthogonal phase array for identifying MIMO systems...

% @description In the following, we use F to indicate the amount of tones
% @description in the multisine. Length of the output cell arrays is entirely 
% @description user-defined and can be given to function by modifying the 
% @description order of one the input. The cell length will be equal to
% @description 2*(prod(order))-1. The factor 2 being present because both
% @description negative and positive BHA have to be extracted.

% @description Can be used for MIMO systems with an arbitrary number of inputs. The obtained Multisines
% @description are nicely orthogonal and hence are referred to in the literature 
% @description as 'Full Orthogonal Multisines'.
% @description Example: DFT matrix for nu=3             
% @description [R1a R1b R1c]                [1      1               1       ]
% @description [R2a R2b R2c] = (1/sqrt(nu))*[1 exp(1j*2*pi/3) exp(1j*4*pi/3)]
% @description [R3a R3b R3c]                [1 exp(1j*4*pi/3) exp(1j*8*pi/3)]

% @version 14-05-2015  Version 1.
% @version 01-06-2015  Changed parsercheck to @isscalar instead of @isinteger.

% @output1 Orthogonal phase array.
% @outputType1 Array of [F X ME X nu]

% @extra FOMS are made using the DFT matrix as the backbone:
% @extra http://en.wikipedia.org/wiki/DFT_matrix
%% generateFunctionHelp: old help, backed up at 14-May-2015. leave this at the end of the function
% getFOMSphase creates an orthogonal phase array for identifying MIMO systems...
% 
%      phase = getFOMSphase(F,ME,spec);
% 
% In the following, we use F to indicate the amount of tones in the multisine. Length
% of the output cell arrays is entirely  user-defined and can be given to function
% by modifying the  order of one the input. The cell length will be equal to 2*(prod(order))-1.
% The factor 2 being present because both negative and positive BHA have to be extracted.
% Can be used for MIMO systems with an arbitrary number of inputs. The obtained Multisines
% are nicely orthogonal and hence are referred to in the literature  as 'Full Orthogonal
% Multisines'. 
