function [G, CvecG, CZ, Z] = extractMIMO_BLA(U,Y,varargin)
% extractMIMO_BLA Calculates the MIMO BLA starting from pre-processed data arrays.
% 
%      [G,CvecG,CZ,Z] = extractMIMO_BLA(U,Y);
%      [G,CvecG,CZ,Z] = extractMIMO_BLA(U,Y,'ParamName',paramValue,...);
% 
% In the following, we use F to indicate the amount of tones in the multisine and
% M for the amount of phase realisations.
% 
% Required inputs:
% - U      check: @checkData
%      The input spectral data. [nu X nu X M X F]
% - Y      check: @checkData
%      The input spectral data. [ny X nu X M X F]
% 
% Parameter/Value pairs:
% - 'R'     default: []  check: @checkData
%      The reference spectral data. [nu X nu X M X F]
% - 'excitedBins'     default: []  check: @(x) isnumeric(x) & isvector(x)
%      The excited frequency bins of the given spectral data.
%    
% Outputs:
% - G      Type: array
%      The calculated MIMO BLA [ny X nu X F]
% - CvecG      Type: array
%      the Covariance matrix of the BLA  [ny*nu X nu*nu X F]
% - CZ      Type: array
%      the Covariance matrix of the stacked input-output vectors [ny+nu
%      X ny+nu X nu X F]
% - Z      Type: array
%      stacked input-output spectra, averaged over the realisations
%      [(ny+nu) X nu X F]
% 
% Rik Pintelon, Piet Bronders, Adam Cooman, ELEC, VUB
% 
% Version info:
%  22-04-2013  Version 1.
% 
% This documentation was generated with the generateFunctionHelp function at 13-May-2015

%% parse the input

p = inputParser();
p.FunctionName = 'extractMIMO_BLA';

% The input spectral data. [nu X nu X M X F]
p.addRequired('U',@checkData);

% The input spectral data. [ny X nu X M X F]
p.addRequired('Y',@checkData);

% The reference spectral data. [nu X nu X M X F]
p.addParameter('R',[],@checkData);

% The excited frequency bins of the given spectral data.
p.addParameter('excitedBins',[],@(x) isnumeric(x) & isvector(x));

p.StructExpand = true;
p.parse(U,Y,varargin{:});
args = p.Results();

%% Initialisation of the variables

[nu,ny,M,F_all]=checkInputs(args.U,args.Y,args.R);         % Check the consistency of the input data

if isempty(args.excitedBins)
    args.excitedBins=1:F_all;
end
args.excitedBins = args.excitedBins(:).';    % excited harmonic numbers
F = length(args.excitedBins);                % number of excited frequencies 

try
    if isempty(args.R)
        Reference = 0;
    else
        Reference = 1;
    end % if
catch
    args.R = [];
    Reference = 0;
end % try

CZ = [];
G = zeros(ny, nu, F);
CvecG = [];

% Create the Z_all matrix, stacked vector of U and Y ( ny+nu x nu x M x F )
Z_all = [args.Y; args.U];

Z = zeros(ny+nu, nu, F); %The averaged (over realizations) version of Z_all

if (M == 1) % if only one realisation is available
    Z(:, :, :) = squeeze(Z_all);
    G = FRM(Z);
else % if more than one realization 
    CvecG = zeros(ny*nu, ny*nu, F);
    CZ= zeros(ny+nu, ny+nu,nu, F);
    switch Reference
        case 1 % the reference signal is available
            
            % check the orthogonality of R
            condR = zeros(M,F);
            for mm=1:M
                for ff=1:F
                    condR(mm,ff) = cond(args.R(:,:,mm,ff));
                end
            end
            if any(any(condR>1e3))
                warning('your reference signal is badly conditioned. max(cond(R))=%d',max(max(condR)));
            end
            
            % DFT spectra of the reference signal for all realisations and all experiments
            Tphase = args.R./abs(args.R)/sqrt(nu); % Get the reference phases from the reference spectra
            
            % turning the phases of the input-output spectra for all realisations and all frequencies
            if nu == 1
                Z_all = Z_all .* repmat(conj(Tphase), [ny+nu, 1, 1, 1]);
            else % nu > 1
                for mm = 1:M
                    for kk = 1:F
                        Z_all(:, :, mm, kk) = Z_all(:, :, mm, kk) * squeeze(Tphase(:, :, mm, kk))';
                    end % kk, frequencies
                end % mm, realisations
            end % if one input
            
            % sample mean input-output spectra over the M realisations
            Zmean = mean(Z_all, 3); % equation (46)
            
            % input-output residuals
            Zres_all = Z_all - repmat(Zmean, [1, 1, M, 1]);
            
            % remove the singular realisation dimension
            Z(:,:,:) = squeeze(Zmean);
            
            % calculate the BLA using the FRFs from reference to input and from reference to output
            [G, SUInv] = FRM(Z); % equation (13)
            
            % calculate the covariance of the Z matrix
            Zres_all = permute(Zres_all, [3 1 2 4]);
            for kk = 1:F
                for ee = 1:nu
                    CZ(:, :, ee, kk) = cov(squeeze(Zres_all(:, :, ee, kk))); % equation (45, part 1)
                end
            end
            CZ = CZ/M; % total covariance of the mean value
            
            % calculate the distortion introduced by the system.
            systemdistortion = zeros(1,F);
            temp = zeros(ny,nu,M);
            temp2 = zeros(ny,M);
            for ff=1:F
                for mm=1:M
                    for ee=1:nu
                        % Ys = Y - G * U
                        temp(:,ee,mm) = [eye(ny) -G(:,:,ff)] * Z_all(:,ee,mm,ff);
                    end
                    temp2(:,mm) = rms(squeeze(temp(:,:,mm)));
                end
                Ys = rms(temp2);
                % just sum over the outputs to obtain 1 number per frequency
                systemdistortion(ff) = sum(Ys);
            end
            
            % calculate the total distortion at the output node
            outputdistortion = zeros(1,F);
            temp=zeros(ny+nu,nu,F);
            for ff=1:F
                for mm=1:M
                    for ee=1:nu
                        temp(:,ee,mm) = Z_all(:,ee,mm,ff) - Z(:,ee,ff);
                    end
                    temp2(:,mm) = rms(squeeze(temp(:,:,mm)));
                end
                Yt = rms(temp2);
                % just sum over the different outputs as well
                outputdistortion(ff) = sum(Yt);
            end
            
            % determine whether the system is the dominant source of distortion, or the generator
            if sum(outputdistortion)>20*sum(systemdistortion)
                errorSource='Generator';
            else
                errorSource='System';
            end
            
            disp(['The dominant source of errors is the ' errorSource]);
            
            switch errorSource
                case 'System' % process errors dominant, use the classic method to determine the covariance
                    
                    % CZ = covariance matrix of the output and input spectra on top of each other of the nu experiments [size (ny+nu) x (ny+nu) x nu x F]
                    CvecG = Cov_FRM(G,SUInv, CZ);  % total covariance matrix FRM
                    
                case 'Generator' % generator error dominant, special case, use section 3.2.3 from the paper to calculate the covariance
                    
                    G_m = zeros(ny, nu, M, F); % G_m is the G^[m] of the paper.
                    for mm = 1:M
                        G_m(:,:,mm,:) = FRM(reshape(squeeze(Z_all(:,:,mm,:)),[ny+nu,nu,F])); % FRM and inverse input matrix
                    end
                    
                    % calculate the BLA without reference
                    G_noref = reshape(squeeze(mean(G_m, 3)),[ny,nu,F]); % equation (56)
                    
                    % calculate the residuals vectors
                    vecGres_all = zeros(ny*nu,M,F);
                    for mm=1:M
                        for ff=1:F
                            vecGres_all(:,mm,ff) = vec(squeeze(G_m(:,:,mm,ff))) - vec( G_noref(:,:,ff) ); % equation (55), part 2
                        end
                    end
                    
                    % calculate the covariance matrix for all frequencies
                    for ff=1:F
                        CvecG(:,:,ff) = cov(squeeze(vecGres_all(:,:,ff)).'); % equation (55), part 1
                    end
                    
            end % if r(t) is available
            
        otherwise % the reference signal is not available
            
            G_m = zeros(ny, nu, M, F); % G_m is the G^[m] of the paper.
            for mm = 1:M
                G_m(:,:,mm,:) = FRM(reshape(squeeze(Z_all(:,:,mm,:)),[ny+nu,nu,F])); % FRM and inverse input matrix
            end
            
            % calculate the BLA without reference
            G = reshape(squeeze(mean(G_m, 3)),[ny,nu,F]); % equation (56)

            % calculate the residuals vectors
            vecGres_all = zeros(ny*nu,M,F);
            for mm=1:M
                for ff=1:F
                    vecGres_all(:,mm,ff) = vec(squeeze(G_m(:,:,mm,ff))) - vec( G(:,:,ff) ); % equation (55), part 2
                end
            end

            % calculate the covariance matrix for all frequencies
            for ff=1:F
                CvecG(:,:,ff) = cov(squeeze(vecGres_all(:,:,ff)).'); % equation (55), part 1
            end
            
    end % if M > 1 and r(t) is not available
end
end


function [nu,ny,M,F_all]=checkInputs(U,Y,R)
    % get all the dimensions out of Y
    [ny,nu,M,F_all] = size(Y);
    
    % check the dimensions of U
    [nut1,nut2,Mt,Ft]=size(U);
    Uerror = 'size of the input should be [nu x nu x M x F]. Either U or Y is wrong';
    if nut1~=nu
        error(Uerror);
    end
    if nut2~=nu
        error(Uerror);
    end
    if Mt~=M
        error(Uerror);
    end
    if Ft~=F_all
        error(Uerror);
    end
    
    % if R is passed, Check R
    if ~isempty(R)
        [nut1,nut2,Mt,Ft]=size(R);
        Rerror = 'size of the reference should be [nu x nu x M x F] Either R or Y is wrong';
        if nut1~=nu
            error(Rerror);
        end
        if nut2~=nu
            error(Rerror);
        end
        if Mt~=M
            error(Rerror);
        end
        if Ft~=F_all
            error(Rerror);
        end
    end
    
end

function res=checkData(s)
if ~isempty(s)
    if max(size(size(s)))~=4
        error('The dimension of the input data was incorrect.')
    end
end
res=1;
end

% @generateFunctionHelp
% @author Rik Pintelon,
% @author Piet Bronders,
% @author Adam Cooman
% @institution ELEC, VUB

% @tagline Calculates the MIMO BLA starting from pre-processed data arrays.

% @description In the following, we use F to indicate the amount of tones
% @description in the multisine and M for the amount of phase realisations.

% @version 22-04-2013  Version 1.

% @output1 The calculated MIMO BLA [ny X nu X F]
% @outputType1 array

% @output2 the Covariance matrix of the BLA  [ny*nu X nu*nu X F]
% @outputType2 array

% @output3 the Covariance matrix of the stacked input-output vectors [ny+nu X ny+nu X nu X F]
% @outputType3 array

% @output4 stacked input-output spectra, averaged over the realisations [(ny+nu) X nu X F]
% @outputType4 array

%% generateFunctionHelp: old help, backed up at 13-May-2015. leave this at the end of the function
% extractMIMO_BLA Calculates the MIMO BLA starting from pre-processed data arrays.
% 
%      [G,CvecG,CZ,Z] = extractMIMO_BLA(U,Y);
%      [G,CvecG,CZ,Z] = extractMIMO_BLA(U,Y,'ParamName',paramValue,...);
% 
% In the following, we use F to indicate the amount of tones in the multisine and
% M for the amount of realisations of te MS.
% 
% Required inputs:
% - U      check: @checkData
%      The input spectral data. [nu X nu X M X F]
% - Y      check: @checkData
%      The input spectral data. [ny X nu X M X F]
% 
% Parameter/Value pairs:
% - 'R'     default: []  check: @checkData
%      The reference spectral data. [nu X nu X M X F]
% - 'excitedBins'     default: []  check: @(x) isnumeric(x) & isvector(x)
%      The excited frequency bins of the given spectral data.
% 
% Outputs:
% - G      Type: array
%      The calculated MIMO BLA [ny X nu X F]
% - CvecG      Type: array
%      Total variance of the BLA  [ny X nu X F]
% - CZ      Type: array
%      the Covariance matrix of the BLA [ny*nu X ny*nu X F]
% - Z      Type: array
%      stacked input-output spectra, averaged over the realisations
%      [(ny+nu) X nu X F]
% 
% Rik Pintelon Piet Bronders, Adam Cooman, ELEC, VUB
% 
% Version info:
%  22-04-2013  Version 1.
% 
% This documentation was generated with the generateFunctionHelp function at 22-Apr-2015
