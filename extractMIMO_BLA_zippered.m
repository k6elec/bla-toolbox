function [BLA] = extractMIMO_BLA_zippered(spec,varargin)
% extractMIMO_BLA_zippered combines experiments with zippered multisines to determine the MIMO BLA
% 
%      BLA = extractMIMO_BLA_zippered(spec);
%      BLA = extractMIMO_BLA_zippered(spec,'ParamName',paramValue,...);
% 
% The function first estimates the SISO BLA from each of the references to the input
% and output at the frequency grid of that reference signal. Then, the SISO BLA is
% interpolated to the full frequency grid.  In a second step, the different BLAs are
% combined in a MIMO identification. The frequency grid is determined by the overlap
% of all multisine excitation signals.
% 
% Required inputs:
% - spec      check: @isstruct
%      contains the simulation data
% 
% Parameter/Value pairs:
% - 'input'     default: {'in'}  check: @(x) checkInputs(x,spec)
%      signal names of the inputs.
% - 'output'     default: {'out'}  check: @(x) checkInputs(x,spec)
%      signal names of the outputs
% - 'reference'     default: {'ref'}  check: @(x) checkInputs(x,spec)
%      signal names of the reference nodes. Make sure the reference
%      signals are  clean, because they will be used to detect the
%      excited frequency bins
% 
% Outputs:
% - BLA      Type: struct
%      Contains the result of the BLA estimation. See below for a description
%      of the fields
% 
% Example:
%   The BLA struct contains the following fields:
%      BLA.G       NxMxF matrix that contains the MIMO BLA 
%      BLA.freq    frequency axis of the BLA
%      BLA.CvecG   covariance matrix of vec(G), size NxM x NxM x F
%      BLA.varG    variance of the estimate of the BLA (size NxMxF)
% 
% Adam Cooman, ELEC, VUB
% 
% Version info:
%  01/06/2015 Version 1
% 
% This documentation was generated with the generateFunctionHelp function at 01-Jun-2015

p=inputParserEgon();
% contains the simulation data
p.addRequired('spec',@(x) isstruct(x)||iscell(x));
% signal names of the inputs.
p.addParamValue('input',{'in'},@(x) checkInputs(x,spec));
% singal names of the outputs
p.addParamValue('output',{'out'},@(x) checkInputs(x,spec));
% signal names of the reference nodes. Make sure the reference signals are
% clean, because they will be used to detect the excited frequency bins
p.addParamValue('reference',{'ref'},@iscell);
% if this is set to true, some of the results are plotted
p.addParameter('plot',false,@islogical);
% if this is set to true, the intermediate SIMO BLAs from reference to the
% stacked input/output are added to the result struct
p.addParameter('addZinfo',false,@islogical)
p.parse(spec,varargin{:});
args = p.Results();

% if only one simulation is passed in a struct, put it in a cell array
if isstruct(args.spec)
    args.spec = {args.spec};
end
if iscellstr(args.reference)
    args.reference = {args.reference};
end

% ns is the amount of simulations
ns = length(args.spec);
% nr is a cell array which contains the amount of reference signals per simulation
for ss=1:ns
    nr(ss) = length(args.reference{ss});
end
% nu is the amount of inputs
nu = length(args.input);
% ny is the amount of outputs
ny = length(args.output);

% M is the number of realisations
% F is the amount of frequency points in a single simulation
[M,~,F] = size(args.spec{1}.(args.reference{1}{1}));

% gather the vector X which contains the stacked output-input signals for
% each simulation
X = cell(ns,1);
for ss=1:ns
    X{ss} = zeros(ny+nu,M,F);
    for yy=1:ny
        X{ss}(yy,:,:)    = args.spec{ss}.(args.output{yy})(:,end,:);
    end
    for uu=1:nu
        X{ss}(ny+uu,:,:) = args.spec{ss}.(args.input{uu})(:,end,:);
    end
end

% also gather the R signals in a convenient matrix
R = cell(ns,1);
for ss=1:ns;
    R{ss} = zeros(nr(ss),M,F);
    for rr=1:nr(ss)
        R{ss}(rr,:,:)    = args.spec{ss}.(args.reference{ss}{rr})(:,end,:);
    end
end

% first estimate the SIMO BLA from each reference to the stacked
% output-input vector on the different non-overlapping frequency grids
exBins = cell(ns,1);
G = cell(ns,1);
CvecG = cell(ns,1);
for ss=1:ns
    exBins{ss} = cell(nr(ss),1);
    G{ss}      = cell(nr(ss),1);
    CvecG{ss}  = cell(nr(ss),1);
    for rr=1:nr(ss)
        % find the excited bins for the specific reference signal
        exBins{ss}{rr} = find(db(args.spec{ss}.(args.reference{ss}{rr})(1,end,2:end))>(max(db(args.spec{ss}.(args.reference{ss}{rr})(1,end,2:end)))-10))+1;
        % determine the BLA from the reference signal to the stacked output-input vector
        F = length(exBins{ss}{rr});
        G{ss}{rr} = zeros(ny+nu,1,F);
        CvecG{ss}{rr} = zeros(ny+nu,ny+nu,F);
        for ff=1:F
            % G = X./R
            G{ss}{rr}(:,1,ff) = mean(X{ss}(:,:,exBins{ss}{rr}(ff))./repmat(R{ss}(rr,:,exBins{ss}{rr}(ff)),[ny+nu 1]),2);
            % determine the uncertainty on this BLA.
            % CvecG = cov(X - G*R)/M/|R|^2
            % with the extra division by M because we are considering the uncertainty on the average
            CvecG{ss}{rr}(:,:,ff) = cov((X{ss}(:,:,exBins{ss}{rr}(ff)) - squeeze(G{ss}{rr}(:,1,ff))*R{ss}(rr,:,exBins{ss}{rr}(ff))).')/M/abs(R{ss}(rr,1,exBins{ss}{rr}(ff)))^2;
        end
    end
end


% if args.plot
%     h=figure(1001);
%     set(h,'name','amplitude of the Z-matrix of stacked input and output vectors');
%     for ii=1:ny+nu
%         for rr=1:nr
%             subplot(ny+nu,nr,(ii-1)*nr+rr);
%             errorbar_mimo(args.spec.freq(exBins{rr}),G{rr}(ii,1,:),sqrt(CvecG{rr}(ii,ii,:)),'b',@db);
%         end
%     end
%     h=figure(1002);
%     set(h,'name','phase of the Z-matrix of stacked input and output vectors');
%     for ii=1:ny+nu
%         for rr=1:nr
%             subplot(ny+nu,nr,(ii-1)*nr+rr);
%             errorbar_mimo(args.spec.freq(exBins{rr}),G{rr}(ii,1,:),sqrt(CvecG{rr}(ii,ii,:)),'b',@angle);
%         end
%     end
% end

% the frequency band at which we will calculate the BLA is determined by
% the maximum frequency of all the minima and the minimum frequency of all
% the maxima
for rr=1:nr(1)
    mins(rr) = min(exBins{1}{rr});
    maxs(rr) = max(exBins{1}{rr});
end
fmin = args.spec{1}.freq(max(mins));
fmax = args.spec{1}.freq(min(maxs));
% now extract the actual frequency axis from the first BLA
inds =  fmin<=args.spec{1}.freq & args.spec{1}.freq<=fmax;
BLA.freq = args.spec{1}.freq(inds);
F = length(BLA.freq);

% combine the different simulations in one big Z matrix
ind = 2;
clear CvecGc
for ss=1:ns
    % the first in the list is always the big multisine. That one should be
    % handled separately
    Gc{1}(:,ss,:) = G{ss}{1};
    CvecGc{1}(ss,:,:,:) = CvecG{ss}{1};
    exBinsc{1} = exBins{ss}{1};
    for rr=2:nr(ss)
        Gc{ind} = G{ss}{rr};
        CvecGc{ind} = CvecG{ss}{rr};
        exBinsc{ind} = exBins{ss}{rr};
        ind = ind+1;
    end
end
Gc{1} = mean(Gc{1},2);
CvecGc{1} = squeezeDim(mean(CvecGc{1},1),1)/ns;

ne = 1+sum(nr-1);
% interpolate the different obtained BLAs to obtain the Z matrix
Z = zeros(ny+nu,ne,F);
CvecZ = zeros((ny+nu)*ne,(ny+nu)*ne,F);
for ee=1:ne
    Z(:,ee,:) = interpolate_MIMO(args.spec{1}.freq(exBinsc{ee}),Gc{ee},BLA.freq);
    % also just interpolate the covariance matrix. This will introduce a
    % small error, but hey
    CvecZ((ny+nu)*(ee-1)+1:(ny+nu)*(ee),(ny+nu)*(ee-1)+1:(ny+nu)*(ee),:) = ...
        interpolate_MIMO(args.spec{1}.freq(exBinsc{ee}),CvecGc{ee},BLA.freq);
end

% plot the info from the interpolated Z and CvecZ matrices
if args.plot
    varZ = reshape(diag_FRM(CvecZ),[ny+nu ne F]);
    h=figure(1001);
    set(h,'name','Amplitude of the estimated SIMO BLA from reference to output-input');
    for yy=1:(ny+nu)
        for ee=1:ne
            subplot(ny+nu,ne,(yy-1)*ne+ee)
            errorbar_mimo(BLA.freq,Z(yy,ee,:),sqrt(varZ(yy,ee,:)),'r+',@db);
            title(['signal ' num2str(yy) ' experiment ' num2str(ee)]);
        end
    end
    h=figure(1002);
    set(h,'name','Phase of the estimated SIMO BLA from reference to output-input');
    for yy=1:(ny+nu)
        for ee=1:ne
            subplot(ny+nu,ne,(yy-1)*ne+ee)
            errorbar_mimo(BLA.freq,Z(yy,ee,:),sqrt(varZ(yy,ee,:)),'r+',@angle);
            title(['signal ' num2str(yy) ' experiment ' num2str(ee)]);
        end 
    end
end

% now calculate the actual BLA
BLA.G = FRM(Z,nu);

% we calculate the covariance of the BLA by using the uncertainty on the estimate of Z
BLA.CvecG = zeros(ny*nu,ny*nu,F);
for ff=1:F
    Vt = [eye(ny) -BLA.G(:,:,ff)];
    Ut = pinv(Z(ny+1:end,:,ff));
    T=kron(Ut.',Vt);
    BLA.CvecG(:,:,ff) = T*CvecZ(:,:,ff)*T';
end

% ignore the covariance and just save the variance in a matrix that has the
% same size as the BLA itself
BLA.varG = zeros(size(BLA.G));
for ff = 1:F
    BLA.varG(:,:,ff) = reshape(diag(squeeze(BLA.CvecG(:,:,ff))) , [ ny , nu ] );
end

if args.plot
    h=figure(1003);
    set(h,'name','Amplitude of the estimated BLA');
    errorbar_mimo(BLA.freq,BLA.G,sqrt(BLA.varG),[],@db);
    h=figure(1004);
    set(h,'name','Phase of the estimated BLA');
    errorbar_mimo(BLA.freq,BLA.G,sqrt(BLA.varG),[],@angle);
end



end

function [tf,x] = checkInputs(x,spec)
    if ischar(x)
        x = {x};
    end
    % spec can be a struct or a cell of structs
    if isstruct(spec)
        tf = all(isfield(spec,x));
    else
        for ii=1:length(spec)
            tf(ii) = all(isfield(spec{ii},x));
        end
        tf = all(tf);
    end
end


% @generateFunctionHelp

% @tagline combines experiments with zippered multisines to determine the MIMO BLA

% @output1 Contains the result of the BLA estimation. See below for a
% @output1 description of the fields
% @outputType1 struct

% @output2 Contains the BLAs from reference to intput and output for the
% @output2 different reference signals
% @outputType2 struct

% @description The function first estimates the SISO BLA from each of the
% @description references to the input and output at the frequency grid of that
% @description reference signal. Then, the SISO BLA is interpolated to the full
% @description frequency grid. 
% @description In a second step, the different BLAs are combined in a MIMO
% @description identification. The frequency grid is determined by the overlap of all
% @description multisine excitation signals.

% @example The BLA struct contains the following fields:
% @example    BLA.G       NxMxF matrix that contains the MIMO BLA 
% @example    BLA.freq    frequency axis of the BLA
% @example    BLA.CvecG   covariance matrix of vec(G), size NxM x NxM x F
% @example    BLA.varG    variance of the estimate of the BLA (size NxMxF)

% @author Adam Cooman
% @institution ELEC, VUB

% @version 01/06/2015 Version 1

%% generateFunctionHelp: old help, backed up at 01-Jun-2015. leave this at the end of the function
% extractMIMO_BLA_zippered combines experiments with zippered multisines to determine the MIMO BLA
% 
%      BLA = extractMIMO_BLA_zippered(spec);
%      BLA = extractMIMO_BLA_zippered(spec,'ParamName',paramValue,...);
% 
% 
% Required inputs:
% - spec      check: @isstruct
%      contains the simulation data
% 
% Parameter/Value pairs:
% - 'input'     default: {'in'}  check: @(x) checkInputs(x,spec)
%      signal names of the inputs.
% - 'output'     default: {'out'}  check: @(x) checkInputs(x,spec)
%      singal names of the outputs
% - 'reference'     default: {'ref'}  check: @(x) checkInputs(x,spec)
%      signal names of the reference nodes. Make sure the reference
%      signals are  clean, because they will be used to detect the
%      excited frequency bins
% 
% Outputs:
% - BLA      Type: struct
%      Contains the result of the BLA estimation. See below for a description
%      of the fields
% 
% Example:
%   The BLA struct contains the following fields:
%      BLA.G       NxMxF matrix that contains the MIMO BLA 
%      BLA.freq    frequency axis of the BLA
%      BLA.CvecG   covariance matrix of vec(G), size NxM x NxM x F
%      BLA.varG    variance of the estimate of the BLA (size NxMxF)
% 
% The function first estimates the SISO BLA from each of the references to the input
% and output at the frequency grid of that reference signal. Then, the SISO BLA is
% interpolated to the full frequency grid.  In a second step, the different BLAs are
% combined in a MIMO identification. The frequency grid is determined by the overlap
% of all multisine excitation signals
% 
% Adam Cooman, ELEC, VUB
% 
% Version info:
%  01/06/2015 Version 1
% 
% This documentation was generated with the generateFunctionHelp function at 01-Jun-2015
