function [Theta,Cost,MDL_cost] = parametric_MIMO_FRF(S,freq,varargin)
% this function performs a parametric estimation on a system represented by its port matrix
%
%   [Theta ] = parametricFRF(S,varS,freq)
%
% Inputs
%   S       P x P x F frequency response matrix (FRM) of the system, 
%            where P is the amount of ports and F is the amount of frequencies
%   freq    1 x F vector which contains the frequencies at whick the FRM is measured
%
% Parameter     default     description
%   varS      zero matrix     P x P x F matrix which contains the variance on the frequency response matrix
%   order         [2 2]       vector that contains the model order to be used. The maximum is used for the amount of poles
%                             the minimum indicates the amount of zeros. (To get proper systems)
%   ModelVar  ModelVar.Transient=0;      Struct that contains info about the model. See Rik's MIMO_ML function for explanation
%             ModelVar.PlantPlane='s';
%             ModelVar.NoisePlane='s';
%             ModelVar.RecipPlant=0;
%             ModelVar.Struct= 'EIV';         
%   IterVar   IterVar.LM       = 1;      Struct that contains info for the optimisation algorithm.
%             IterVar.MaxIter  = 100;    See Rik's MIMO_ML function for more info.
%             IterVar.TolParam = 1e-10;
%             IterVar.TolCost  = 1e-5;
%             IterVar.TraceOn  = 1;                    
%   showResults   false       Boolean. If you set this to true, the result of the estimation will be shown
%
% Outputs
%   Theta   Struct that contains the model parameters
%               Theta.A     1 x max(Order+1) vector that contains the coefficients of the polynomial 
%                                          in the denominator of the model
%               Theta.B     ny x nu x min(Order+1)  matrix that contain the values of the matrix polynomial 
%                                                 in the numerator of the 
%
% See also: MIMO_NL, MIMO_WGTLS, MIMO_BTLS
%
%
% Adam Cooman, Piet Bronders, ELEC VUB

p=inputParser;
p.addRequired('S',@isnumeric);
p.addRequired('freq',@isvector);
p.addParamValue('varS',[],@isnumeric);
p.addParamValue('order',[2 2],@isvector);
p.addParamValue('ModelVar',[],@isstruct);
p.addParamValue('IterVar',[],@isstruct);
p.addParamValue('showResults',false,@islogical)
p.parse(S,freq,varargin{:});
args = p.Results();
clear S freq varargin

% get the amount of ports
[ny,nu,F]=size(args.S);
if ny~=nu
    error('function is written for port representations, which have square FRMs');
else
    P=nu;
    clear ny nu
end

% check whether the variance of S has been provided
if isempty(args.varS)
    noVariance = true;
    args.varS = eps*ones([P P F]);
else
    noVariance = false;
end

% create the data struct for the estimation
data = struct('Y', [], 'U', [], 'freq', [], 'CY', [], 'CU', [], 'CYU', []);
data.Y=args.S;
data.U=repmat(eye(P),[1 1 F]);
if noVariance
    data.CY = eps*rand([P,P,P,F]);
else
    data.CY = zeros([P,P,P,F]);
    for pp=1:P
        for ff=1:F
            data.CY(:,:,pp,ff) = diag(squeeze(args.varS(:,pp,ff)));
        end
    end
end
data.CU = zeros([P,P,P,F]);
data.CYU= zeros([P,P,P,F]);
data.freq = args.freq;
data.Ts = 1/(4*max(args.freq));

% Options for the final parametric model
if isempty(args.ModelVar)
    ModelVar = struct('Transient', [], 'PlantPlane', [], 'NoisePlane', [], 'RecipPlant', []);
    ModelVar.Transient=0;
    ModelVar.PlantPlane='s';
    ModelVar.NoisePlane='s';
    ModelVar.RecipPlant=0;
    ModelVar.Struct= 'OE';
else
    ModelVar = args.ModelVar;
end

% Maxima and minima for iteration 
if isempty(args.IterVar)
    IterVar = struct('LM', [], 'MaxIter', [], 'TolParam', [], 'TolCost', [], 'TraceOn', []);
    IterVar.LM       = 1;
    IterVar.MaxIter  = 100;
    IterVar.TolParam = 1e-10;
    IterVar.TolCost  = 1e-5;
    IterVar.TraceOn  = 0;
else
    IterVar = args.IterVar;
end

% Order
OrdA=max(args.order);
OrdB=min(args.order);

Sel = struct('A',[],'B',[]); %'Ig', []
Sel.A = ones(1,OrdA+1);
Sel.B = ones(P,P,OrdB+1);

% Generate starting values with Weighted General Total Least Squares
if noVariance
    [Theta, ~, ~, ~] = MIMO_WTLS(data, Sel, ModelVar);
else
    [Theta, ~, ~, ~] = MIMO_WGTLS(data, Sel, ModelVar);
end

% if args.showResults
%     figure('name','WGTLS result')
%     
%     G = evalParametric(Theta.A,Theta.B,args.freq);
%     plot_mimo(args.freq,db(G));
%     plot_mimo(args.freq,db(args.S),'r+');
% %     plot_mimo(args.freq,db(args.varS),'ro');
% end

if P~=1 %MIMO_BTLS code has some issues with SISO systems for some reason...
    % apply beatles to get even better initial values.
    [Theta, ~, ~, ~, ~] = MIMO_BTLS(data, Sel, Theta, ModelVar, IterVar);

    if args.showResults
        figure('name','BTLS result')
        G = evalParametric(Theta.A,Theta.B,args.freq);
        plot_mimo(args.freq,db(G));
        plot_mimo(args.freq,db(args.S),'r+');
    end
end

% Get a final estimate with Sampled Maximum Likelihood
[Theta, Cost, ~, ~, ~] = MIMO_ML(data, Sel, Theta, ModelVar, IterVar);

if args.showResults
    figure('name','ML result')
    G = evalParametric(Theta.A,Theta.B,args.freq);
    plot_mimo(args.freq,db(G));
    plot_mimo(args.freq,db(args.S),'r+');
end

% Calculate MDL (p. 479)
ntheta = (OrdA+1) + P*P*(OrdB+1);

MDL_cost=(2*F/(2*F-ntheta))*Cost*(1+log(2*2*P*F)*ntheta/(2*F-ntheta));

end  



