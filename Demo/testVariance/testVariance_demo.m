% this script shows how to run the testVariance function on an FRF.
close all
clear all
clc

% the variance of the noise added to the FRF
VARIANCE = 0.001;

% generate the frequency axis
f = logspace(-2,3,5000);

% and generate a fancy filter function
[b,a] = besself(5,2*pi);

% get the filter FRF
G0 = freqs(b,a,2*pi*f);

% add some noise with the correct variance
NOISE = sqrt(VARIANCE)*(randn(size(G0))+1i*randn(size(G0)));

% add the noise to the FRF
GN = G0 + NOISE;

% call the testvariance function on the noisy FRF
[varEst,ax] = testVariance(GN,5);

%% plot all the stuff

% show the noisy FRF
plot(log10(f),db(GN));
hold on
% and also show the noiseless FRF
plot(log10(f),db(G0),'r.');
% show the correct variance
plot(log10(f),db(VARIANCE.*ones(size(f)))/2,'r','linewidth',2);
% and the estimated variance of the varEst function
plot(log10(f(ax)),db(squeeze(varEst))/2,'m','linewidth',2);


ylim([-60 10])
legend('Noisy FRF','Noiseless FRF','STD of the noise','Estimated STD')

% we can see that the variance is underestimated a little, but we are close. 
% outside of the band, the variance is completely wrong, but that's due to
% the fact that there's no FRF there