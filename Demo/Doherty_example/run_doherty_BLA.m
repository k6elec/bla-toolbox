% test the BLA_analysis on a Doherty PA example from ADS
cd V:\Toolbox\ADS_MIMOid\Demo\Doherty_example
clear all
clc
close all



BW = 4e6;
tones = 21;
MSdef.f0 = BW/(tones-1);
fc = 2.140e9;
MSdef.fmin = fc - (tones-1)/2*MSdef.f0;
MSdef.fmax = fc + 20*MSdef.f0;
MSdef = MSgridInit(MSdef,'full',3);
MSdef.grid = MSgrid(MSdef);
MSdef.ampl = 0.5*ones(size(MSdef.grid));
MSdef.freq = MSdef.f0*MSdef.grid;
MSdef.Rout = 50;
MSdef.MSnode = 'MS_node';


[Sp , Sd , Sd_lin , Sd_var , bigwaves] = BLA_Analysis('dohertyPA.net',MSdef,'oversample',10,'numberOfRealisations',10,'outOfBandFactor',2,'ticklerDistance',1e3,'showResults',true);
