clear all
close all
clc

% load the simulation data of the low noise amplifier. This contains a 2x2 S-matrix simulated on a 
% logarithmic frequency grid
data = ADSimportSimData('LNA_sparam.mat');

% we do some sweeping on the model order to determine the optimal order
maxOrd = 10;
minOrd = 6;


MDL_matrix = Inf(maxOrd,maxOrd);
Cost_matrix = Inf(maxOrd,maxOrd);
% Theta_matrix = cell(maxOrd,maxOrd);
for na=minOrd:maxOrd
    for nb=na:maxOrd
        disp(['Fitting ' num2str(na) ',' num2str(nb) ' through the data.'])
        [Theta,Cost_matrix(na,nb),MDL_matrix(na,nb)] = parametric_MIMO_FRF(data.sim1_S.S,data.sim1_S.freq,'order',[na nb],'showResults',false);
        Theta_matrix(na,nb) = Theta;
    end
end

Cost_matrix = Cost_matrix(minOrd:maxOrd,minOrd:maxOrd);
MDL_matrix = MDL_matrix(minOrd:maxOrd,minOrd:maxOrd);
Theta_matrix = Theta_matrix(minOrd:maxOrd,minOrd:maxOrd);

figure('name','normal cost')
surf(db(Cost_matrix));
figure('name','MDL cost')
surf(db(MDL_matrix));

[naopt,nbopt]=find(MDL_matrix==min(min(MDL_matrix)));
Theta_opt = Theta_matrix(naopt,nbopt);

figure('name','Best model according to MDL');
G = evalParametric(Theta_opt.A,Theta_opt.B,data.sim1_S.freq);
plot_mimo(data.sim1_S.freq,db(G));
plot_mimo(data.sim1_S.freq,db(data.sim1_S.S),'+');


