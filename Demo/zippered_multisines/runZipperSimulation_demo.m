% This demo shows how to run a simulation with several zippered multisines
% to estimate the MIMO BLA of an op-amp in feedback.

clear all
close all
clc

% generate the main multisine
MSdef = MScreate(1e3,100e3,'grid','odd','Rout',0,'DC',1.65,'rms',50e-3);
MSdef.MSnode = 'MS_node';

% run the simulation with the zippered multisines
spec = runZipperSimulation('zipperSimulation.net',MSdef,...
    'TicklerNodes','out',...
    'outOfBandFactor',1,'numberOfRealisations',10,'measure',true);
% calculate the waves
spec = makeallwaves(spec,50,'pseudo',false);


% and plot the results
figure('name','output voltage')
plot(spec.freq,db(squeeze(spec.V_TOTAL_p2(:,1,:))).','+')
title('V_TOTAL_p2');

% estimate the BLA of the circuit
BLA = extractMIMO_BLA_zippered(spec,...
    'reference',{'MS_node','I_tickle1'},...
    'input'    ,{'W_TOTAL_p1_A','W_TOTAL_p2_A'},...
    'output'   ,{'W_TOTAL_p1_B','W_TOTAL_p2_B'});

% and plot the result
plot_mimo(BLA.freq,db(BLA.G)   ,'+');
plot_mimo(BLA.freq,db(BLA.varG),'+');

