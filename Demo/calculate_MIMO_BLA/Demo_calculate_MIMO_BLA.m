%Demo that tests the ADS_MIMOid_calculate_MIMO_BLA using several known
%examples.

clear all
clc
close all

cd V:\Toolbox\ADS_MIMOid\Demo\calculate_MIMO_BLA

%% SISO example with a reference input

f0=1e5;
M=8; %Number of phase realizations of the input multisine
simulator='HB'; %Simulator that is used, ENV might be possible as well?

BB=MScreate(f0,30e6);
BB.MSnode='ref';

%HB settings
oversample=16;   
HBsettings.Freq = f0;
HBsettings.Order = (oversample*((3*length(BB.grid)+3)-1)/2+oversample-1);
HBsettings.MaxOrder = HBsettings.Order(1);

netlist='Netlists\BasebandSISO_BLA.net';
[spec] = ADSsimulateMS(netlist,BB,'simulator',simulator,'numberOfRealisations',M,'SimSettingsStruct',HBsettings);

%% Extraction of the SISO BLA
clear settings U Y R

U.nodeNames={'input'};
Y.nodeNames={'output'};
R.nodeNames={'ref'};
settings.relevantBins=BB.grid+1;

[G,varG,CvecG,Z,CZ]=calcMIMO_BHA(spec,settings,'U',U,'Y',Y,'R',R);

%% SISO example without reference input
%Also extract the HTFs around 1,2 and 3

%% MIMO example with reference
f0=1e8;
f_end=8e9;
ME=8; %Number of phase realizations of the input multisine
simulator='HB'; %Simulator that is used, ENV might be possible as well?

nu=2;
ny=2;
F=length(f0:f0:f_end);

GATE=MScreate(f0,f_end,'Rout',50);
GATE.MSnode='V_gate';

DRAIN=GATE;
DRAIN.MSnode='V_drain';

%HB settings
oversample=16;   
HBsettings.Freq = f0;
HBsettings.Order = (oversample*((3*length(GATE.grid)+3)-1)/2+oversample-1);
HBsettings.MaxOrder = HBsettings.Order(1);

MS=[GATE DRAIN];

phase = getFOMSphase(F,ME,nu,ny);
DC=cell(1,nu);
DC{1}=-3*ones(1,ME);
DC{2}=28*ones(1,ME);
ampl=cell(1,nu);
ampl{1}=1e-9*ones(F,ME);
ampl{2}=ampl{1};

netlist='Netlists\MIMO_BLA.net';
[spec] = ADSsimulateMSwaves(netlist,MS,'simulator',simulator,'numberOfRealisations',...
    ME,'SimSettingsStruct',HBsettings,'phase',phase,'DC',DC,'ampl',ampl,'combineStructs',true);

%% Extraction of the MIMO BLA
clear settings U Y R

Y.nodeNames={'W_gate_A','W_drain_A'};
R.nodeNames={'MsSource_inst10x2Eint_1','MsSource_inst20x2Eint_1'};
U.nodeNames={'W_gate_B','W_drain_B'};
settings.relevantBins=GATE.grid+1;
settings.fc=1e9;

[G,varG,CvecG,Z,CZ]=calcMIMO_BHA(spec,settings,'U',U,'Y',Y,'R',R);

plot_mimo(db(G{1}));