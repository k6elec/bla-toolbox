%% This demo attempts to verify the stability of the getEnvelopeMS-function,
% which calculates the MS struct of the envelope of an RF signal.
% (also given as an MS struct)

% This will be done for different RF signals consisting of a number of
% different grid configurations

close all
clc
clear all

fc=1e9;
f0=1e6;

%% Calculate the envelope of a full bandpass MS with carrier energy 
MS=MScreate(f0,fc,'mode','bandpass');
MS.ampl((length(MS.ampl)+1)/2)=5;
[ENV,DC]=getEnvelopeMS(MS,'checkplot',1,'ls_order',1,'showapprox',1);
title('Full bandpass MS with carrier energy (ORDER=1).')
keyboard

%% Calculate the envelope of a full bandpass MS with carrier energy 
[~,~]=getEnvelopeMS(MS,'checkplot',1,'ls_order',3,'showapprox',1);
title('Full bandpass MS with carrier energy (ORDER=3).')

%% Calculate the envelope of a full bandpass MS with SUPPRESSED carrier energy
MS.ampl((length(MS.ampl)+1)/2)=0;
[~,~]=getEnvelopeMS(MS,'checkplot',1,'ls_order',3);
title('Full bandpass MS with SUPPRESSED carrier energy.');

%% Calculate the envelope of an odd-even bandpass MS with SUPPRESSED carrier energy
MS=MScreate(f0,fc,'mode','bandpass','grid','odd-even');
[~,~]=getEnvelopeMS(MS,'checkplot',1,'ls_order',3);
title('Odd-even bandpass MS with SUPPRESSED carrier energy.');

%% Calculate the envelope of an random-odd bandpass MS with SUPPRESSED carrier energy
MS=MScreate(f0,fc,'mode','bandpass','grid','random-odd');
[~,~]=getEnvelopeMS(MS,'checkplot',1,'ls_order',3);
title('Random-odd bandpass MS with SUPPRESSED carrier energy.');

%% Calculate the envelope of a multiple bandpass MS while assuring that the same approx is used for each of them
MS=MScreate(f0,fc,'mode','bandpass');
R=9;
figure;
fixedMax=10;
for ii=1:R
    MS.phase=2*pi*rand(size(MS.phase));
    [ms,t_ms]=MScalculatePeriod(MS,16);
    [ENV,DC]=getEnvelopeMS(MS,'ls_order',3);
    [env,t_env]=MScalculatePeriod(ENV,16);
    
    subplot(3,3,ii);
    title(['Realization Nr.' num2str(ii)]);
    hold on;
    plot(t_ms,ms);
    plot(t_env,env+DC,'r');
end

suptitle('Different realizations of a full bandpass MS using the same envelope approximation.');
