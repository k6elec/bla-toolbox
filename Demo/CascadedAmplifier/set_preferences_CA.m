% hpeesofdir points to the folder where ADS is installed
setpref('ADS','hpeesofdir',fullfile('C:','Agilent','ADS2011_01'));

% simarch indicates the computer architecture
setpref('ADS','simarch','win32_64');

% the "simulationFolder" preference points to the folder where temporary simulation data can be stored.
setpref('ADS','simulationFolder',fullfile('V:','Toolbox','ADS_MIMOid','Demo','CascadedAmplifier','SimTemp'));