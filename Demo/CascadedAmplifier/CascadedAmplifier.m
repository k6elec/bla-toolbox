%Calculate the MIMO small-signal response of a Miller OPAMP in ADS and
%seperate the BLA of the 2 different blocks from the cascade

%IMPORTANT CONCLUSION REGARDING THE USED MILLER AMPLIFIER:
%------------
%When measuring the first stage of the miller opamp the S-parameters are
%biased (especially the S12 param.) if the bias network is not split between stages.
%Cutting the bias network in two results in a correct identification of
%the different stages. The reason is probable found in the fact that the
%current which flows to the second stage influences the bias network in
%such a way that the bias network of the first stage exhibits a different
%behaviour.
%------------

clear all
close all
clc

%addpath(genpath('../../..')); %Refuses to work for some reason!
% addpath(genpath('V:\Toolbox'));

% set_preferences_CA;

ny=2;
nu=2;
driveAmp=1e-3; %Amplitude of MS of input voltage at Port 1 (STANDARD VALUE=1e-5)
tickleAmpOUT=1e-6; %Amplitude of MS of input current at the output port (STANDARD VALUE=driveAmp/10)
tickleAmpINTERNAL=1e-9; %Amplitude of MS of input current at the internal port (STANDARD VALUE=driveAmp/1e4)
NrOfReal=10; %Number of wanted realizations
FigureNumber=1;

%% FIRST SIMULATION (INTERNAL TO END) (2nd Component)
    %% Generation of MS excitation

    %MS at Port A is a voltage source
    MSdefa.fmin=1e5;
    MSdefa.fmax=100e5; 
    MSdefa.f0=1e5;
    MSdefa = MSgridInit(MSdefa,'full'); 
    MSdefa.grid = MSgrid(MSdefa);
    MSdefa.freq = MSdefa.f0*sort(MSdefa.grid(:));
    MSdefa.MSnode={'VRa','sgnd'}; %The MS source should be placed between these points
    MSdefa.ampl = driveAmp*ones(size(MSdefa.freq)); 
    MSdefa.Rout=0;

    %MS at Port B is a current source
    MSdefb.fmin=1e5;
    MSdefb.fmax=100e5; 
    MSdefb.f0=1e5;
    MSdefb = MSgridInit(MSdefb,'full'); 
    MSdefb.grid = MSgrid(MSdefb);
    MSdefb.freq = MSdefa.f0*sort(MSdefb.grid(:));
    MSdefb.MSnode={'VRb','sgnd'}; %The MS source should be placed between these points
    MSdefb.ampl = tickleAmpOUT*ones(size(MSdefb.freq)); %Less injected power at output
    MSdefb.Rout=Inf;

    %Put MS in one big matrix
    MSdef=[MSdefa MSdefb];

    %% Initiation of other variables that are required & ADS Simulation
        
        %OPTION1: Save place for the MS
        %OPTION2: The simulator that is used
        %--> TRAN does strange things with phase?? Use HB instead! Check validity
        %of Fs
        %OPTION3: Number of realizations that are used
        M=NrOfReal*nu; %Number of realizations X Number of experiments necessary
        %OPTION4: Number of periods that are measured
        %   --> NO transients in HB! not necessary to measure more periods!
        %       Stick to default value=1.5
        P=1; %0.5 added in final expression
        %OPTION 5: Frequency warping on or off? -->false (freq will go to infinity otherwise?)
        %OPTION 6: Phase matrix for correct identification --> FOMS
        %phase=zeros(length(MSdef.grid),M,nu);
        phase=getFOMSphase(length(MSdefa.grid),M,nu,ny);
        %OPTION 7: Order of oversampling (Degree of nonlinear identificqtion in case of the HB simulator)
        oversample=5;
    
    [waves,spec] = ADSsimulateMSwaves('Netlists\CascadedAmplifier.net',MSdef,'sourceLocation','V:\Toolbox\ADS_MIMOid\Demo\CascadedAmplifier\SimSignals','simulator','HB','numberOfRealisations',M,'periods',P+0.5,'freqWarping',false,'phase',phase,'oversample',oversample);
    % it's not a bad thing if you get the error that VRc is floating
    fs=spec.freq(end)*2;
    N=fs/MSdef(1).f0;
    
    %% Calculation of the MIMO FRM of the 2nd component
    
    Sim.ref1=spec.VRa; % The ORIGINAL orthogonal vectors NEED to be used here!
    % Otherwise the variance will be incorrect due to the abstence of the
    % orthogonality property! :(
    Sim.ref2=spec.I_IRb;
    
    Sim.in1=waves.W_Icb_A;
    Sim.in2=waves.W_Ib_A; 
    Sim.out1=waves.W_Icb_B;
    Sim.out2=waves.W_Ib_B;
    Sim.HBfreq=spec.freq;
    
    Settings.fs=fs;
    Settings.N=N;
    Settings.M=M;
    Settings.P=P;
    Settings.nu=nu;
    Settings.ny=ny;
    Settings.ExcitedHarm=MSdef(1).grid;

%DEBUGGING CODE - IGNORE -------------------------------------------
%     U=zeros(2,2,size(spec.freq,2));
%     U(1,1,:)=squeeze(spec.VRa(9,1,:));
%     U(1,2,:)=squeeze(spec.VRa(10,1,:));
%     U(2,1,:)=squeeze(spec.I_IRb(9,1,:));
%     U(2,2,:)=squeeze(spec.I_IRb(10,1,:));
%     
%     M=zeros(2,2,size(spec.freq,2));
%     for ii=1:size(spec.freq,2)
%         M(:,:,ii)=U(:,:,ii)*U(:,:,ii)';
%     end
%DEBUGGING CODE - IGNORE -------------------------------------------
    
    [freq,G2,varG2,CvecG,Z,CZ]=calculateMIMO_BLA(Sim,Settings);
    
    save('oldmethod','G2','varG2','CvecG','Z','CZ');
    
    
    %% Plot the important system characteristics (S-matrix)
    
    FigureNumber=plotMIMO_BLA(nu,ny,freq,'Second Component',FigureNumber,G2,sqrt(varG2));

%% SECOND 'SIMULATION' (INPUT TO OUTPUT) (full component)
    
    %% Calculation of the MIMO FRF (S matrix will be identified directly)
    
    Sim.ref1=spec.VRa;
    Sim.ref2=spec.I_IRb;
    
    Sim.ref1=waves.W_IRa_A;
    Sim.ref2=waves.W_IRb_A;
    Sim.in1=waves.W_Ia_A;
    Sim.in2=waves.W_Ib_A; 
    Sim.out1=waves.W_Ia_B;
    Sim.out2=waves.W_Ib_B;
    Sim.ExcitedHarm=MSdef(1).grid;
    Sim.HBfreq=spec.freq;
    
    [~,Gtot,varGtot]=calculateMIMO_BLA(Sim,Settings);  
    
%% THIRD SIMULATION (INPUT TO INTERNAL) (1st component)
    %% Generation of MS excitation
    
    %MS at Port 2 is a current source
    MSdef(2).MSnode={'VRc','sgnd'}; %The MS source should be placed between these points
    MSdef(2).ampl = tickleAmpINTERNAL*ones(size(MSdefa.freq)); 
    
    %% Initiation of other variables that are required & ADS Simulation

        %Explanation of options in the previous simulation!!!
        M=NrOfReal*nu; %Number of realizations X Number of experiments necessary
        P=1;
        phase=getFOMSphase(length(MSdefa.grid),M,nu,ny);
        oversample=5;
        %Explanation of options in the previous simulation!!!
        
    [waves,spec] = ADSsimulateMSwaves('Netlists\CascadedAmplifier.net',MSdef,'sourceLocation','V:\Toolbox\ADS_MIMOid\Demo\CascadedAmplifier\SimSignals','simulator','HB','numberOfRealisations',M,'periods',P+0.5,'freqWarping',false,'phase',phase,'oversample',oversample);

    fs=spec.freq(end)*2;
    N=fs/MSdef(1).f0;
    
    %% Calculation of the MIMO FRF (S matrix will be identified directly)
        
    Sim.ref1=spec.VRa;
    Sim.ref2=spec.I_IRc;
    
    Sim.in1=waves.W_Ia_A;
    Sim.in2=waves.W_Ica_A; 
    Sim.out1=waves.W_Ia_B;
    Sim.out2=waves.W_Ica_B;
    Sim.ExcitedHarm=MSdef(1).grid;
    Sim.HBfreq=spec.freq;
    
%DEBUGGING CODE - IGNORE -------------------------------------------    
%     Sim.ref1=spec.VRa;
%     Sim.ref2=spec.I_IRc;
%     Sim.in1=spec.V_Ia;
%     Sim.in2=spec.I_Ica; 
%     Sim.out1=spec.I_Ia;
%     Sim.out2=spec.V_Ica;
%     Sim.ExcitedHarm=MSdef(1).grid;
%     Sim.HBfreq=spec.freq;
%DEBUGGING CODE - IGNORE -------------------------------------------   

    [~,G1,varG1]=calculateMIMO_BLA(Sim,Settings);  

    %% Plot the important system characteristics (S-matrix)

    FigureNumber=plotMIMO_BLA(nu,ny,freq,'First Component',FigureNumber,G1,sqrt(varG1));

%% COMPUTATION OF TOTAL S-MATRIX
    Stot = connectSmatrices(G1 , G2 , 2 , 1 ,0);
        
    %% Comparison of total S-matrix
    
    FigureNumber=plotMIMO_BLA(nu,ny,freq,'Comparison of Measured and Calculated total S-matrix',FigureNumber,Stot,Gtot);

%DEBUGGING CODE - IGNORE -------------------------------------------
% BLA=zeros(nu,ny,NrOfReal,size(Sim.in1,3));
% for ii=1:NrOfReal
%     IN=zeros(2,2,size(Sim.in1,3));
%     IN(1,1,:)=Sim.in1(2*ii-1,1,:);
%     IN(1,2,:)=Sim.in1(2*ii,1,:);
%     IN(2,1,:)=Sim.in2(2*ii-1,1,:);
%     IN(2,2,:)=Sim.in2(2*ii,1,:);
%     
%     OUT=zeros(2,2,size(Sim.in1,3));
%     OUT(1,1,:)=Sim.out1(2*ii-1,1,:);
%     OUT(1,2,:)=Sim.out1(2*ii,1,:);
%     OUT(2,1,:)=Sim.out2(2*ii-1,1,:);
%     OUT(2,2,:)=Sim.out2(2*ii,1,:);
%     
%     for kk=1:size(IN,3)
%         BLA(:,:,ii,kk)=OUT(:,:,kk)*pinv(IN(:,:,kk));
%     end
% end
% 
% figure();
% subplot(221)
% plot(squeeze(db(BLA(1,1,:,2:101))).');
% subplot(222)
% plot(squeeze(db(BLA(1,2,:,2:101))).');
% subplot(223)
% plot(squeeze(db(BLA(2,1,:,2:101))).');
% subplot(224)
% plot(squeeze(db(BLA(2,2,:,2:101))).');
%DEBUGGING CODE - IGNORE -------------------------------------------