function G = evalParametric(A,B,freq)
% code stolen from Rik's MIMO_ML_CalcPolyTrans
omega = 1i*2*pi*freq;
[ny,nu,~] = size(B);
F = length(freq);
PolyTransA = polyval(fliplr(A), omega.');
G = zeros(ny,nu,F);
for yy = 1:ny
    for uu = 1:nu
        G(yy, uu, :) = polyval(fliplr(squeeze(B(yy, uu, :)).'), omega.');
    end
end
dummy = zeros(1,1,F);
dummy(1,1,:) = PolyTransA;
G = G./repmat(dummy, ny, nu);
end