function [G,spec_update,settings_update]=calcMIMO_BHA(spec,settings,varargin)
% calcMIMO_BHA calculates the BLA, BMA and consequent transfer functions (user-specified) given a reference spectra, input spectra and output spectra.
% 
%      [G,spec_update,settings_update] = calcMIMO_BHA(spec,settings);
%      [G,spec_update,settings_update] = calcMIMO_BHA(spec,settings,'ParamName',paramValue,...);
% 
% Here several valid possibilities for the 'U'-cell array are given. (1) 'in1', {{'in1'}}
% or {'in1'} (1 SISO BLA);(2) {'in1','in2'} (1 MISO BLA);(3) {'in1',{'in2','in3'}}
% (1 MISO BLA/BMA);(4) U{1}={{'in1'}}, U{2}={{'in2'}} (2 SISO BLA); combinations of
% the previous examples are possible...
% 
% Required inputs:
% - spec      check: @(x) checkSpec(x) && isstruct(x)
%      spec structure containing the relevant data fields. (Output
%      of the HB simulation)
% - settings      check: @(x) checkSettings(x) && isstruct(x)
%      Struct containing several settings for the BHA extracton with
%      not all of them being  necessary. A first optional field is
%      the 'signLvl'-field. This field will  only be used when the
%      excitedBins have to be extracted from the reference  signal.
%      In that case the 'signLvl'-field will be used as the threshold
%      to  seperate excited and unexcited bins. The default value for
%      the field is -10.  The second field is 'pad', this variable
%      indicates if the spectra used  in the convolution will be zero-padded
%      or not. A non zero-padded  convolution will only be a good approximation
%      if there's enough empty  space (which is still present in the
%      spectra, but only contains noise)  between each of the harmonic
%      blocks of the spectra. A zero-padded  convolution is more correct,
%      but infinitely slower. The 'pad'-field is  set to 0 by default.
%      Lastly we have the 'excitedBins','relevantBins' and  'importantBins'-fields.
%      The user has multiple choices for these fields.  (1) The 'importantBins'-field
%      is given. (2) None of the fields are given,  but a reference
%      signal is available. (3) Both 'relevantBins' and  'excitedBins'
%      are given, 'importantBins' can be left empty in that case. 
%      (4) Only 'relevantBins' or 'excitedBins' is given.
% 
% Parameter/Value pairs:
% - 'U'     default: {'in'}  check: @(x) checkUY(x) && isvector(x) && (iscell(x) || ischar(x))
%      Cell array of cell array of strings containing the names of
%      the different input  spectra. The first cell dimension indicates
%      the number of BHA which should be  extracted, while the second
%      dimension indicates the number of input ports.  The strings
%      should be valid fields of the spec struct, but there  are exceptions.
%      A first exception is regarding the '_conj'-extension.  When
%      this extensaion is found to be present AND the 'node_conj'-field
%      is not part of  the spec struct, the function will attempt to
%      take the conjugate of the  'node'-field (which SHOULD be present
%      in spec). Secondly the "string" can be swapped  by a cell array
%      of strings instead. (A 3rd dimension) The function will  than
%      take the convolution of all of the respective fields of the
%      spec  struct. Afterwards this cell array will then be replaced
%      by a  concatenated string (Concatenation of all the strings
%      in this cell  array). Other (simplified) cell arrays can also
%      be processed (ex. when only  1 BHA needs to be extracted, the
%      first dimension of cell arrays can be removed).  Several examples
%      can be found in the function description.
% - 'Y'     default: {'out'}  check: @(x) checkUY(x) && isvector(x) && (iscell(x) || ischar(x))
%      Cell array of strings containing the names of the different
%      output  spectra. See the flavor text of 'U' for more information.
% - 'R'     default: []  check: @(x) checkUY(x) && isvector(x) && (iscell(x) || ischar(x))
%      Cell array of strings containing the names of the different
%      reference spectra,  The reference is however not strictly necessary.
%      See flavor text of 'U' for more information.
% 
% Outputs:
% - G      Type: cell array of structs
%      Cell array of struct containing the calculated BHA, its variance
%      and much more...
% - spec_update      Type: struct
%      the updated spec struct for user convenience.
% - settings_update      Type: struct
%      the updated settings struct for user convenience.
% 
% Piet Bronders, Adam Cooman, ELEC, VUB
% 
% Version info:
%  16-04-2015  Version 1.
%  23-04-2015  Fixed multiple errors in regards to the extraction of negative BMAs.
%  20-05-2015  Gigantic overhaul of the entire function. The function can now internally make the convolution of input spectra.
%  11-06-2015  The function uses the 'conv_HB'-function, a MUCH faster alternative than the slowpoke functionalities used before.
% 
% This documentation was generated with the generateFunctionHelp function at 11-Jun-2015

%% parse the input

p = inputParser();
p.FunctionName = 'calcMIMO_BHA';

% spec structure containing the relevant data fields. (Output of the HB simulation)
p.addRequired('spec',@(x) checkSpec(x) && isstruct(x));

% Cell array of cell array of strings containing the names of the different input
% spectra. The first cell dimension indicates the number of BHA which should be 
% extracted, while the second dimension indicates the number of input ports.
% The strings should be valid fields of the spec struct, but there
% are exceptions. A first exception is regarding the '_conj'-extension.
% When this extensaion is found to be present AND the 'node_conj'-field is not part of
% the spec struct, the function will attempt to take the conjugate of the
% 'node'-field (which SHOULD be present in spec). Secondly the "string" can be swapped 
% by a cell array of strings instead. (A 3rd dimension) The function will
% than take the convolution of all of the respective fields of the spec
% struct. Afterwards this cell array will then be replaced by a
% concatenated string (Concatenation of all the strings in this cell
% array). Other (simplified) cell arrays can also be processed (ex. when only 
% 1 BHA needs to be extracted, the first dimension of cell arrays can be removed). 
% Several examples can be found in the function description.
p.addParameter('U',{'in'},@(x) checkUY(x) && isvector(x) && (iscell(x) || ischar(x)));

% Cell array of strings containing the names of the different output
% spectra. See the flavor text of 'U' for more information.
p.addParameter('Y',{'out'},@(x) checkUY(x) && isvector(x) && (iscell(x) || ischar(x)));

% Cell array of strings containing the names of the different reference spectra, 
% The reference is however not strictly necessary. See flavor text of 'U' for more information.
p.addParameter('R',[],@(x) checkUY(x) && isvector(x) && (iscell(x) || ischar(x)));

% This variable indicates if the spectra used in the convolution will be 
% zero-padded or not. A non zero-padded convolution will only be a good 
% approximation if there's enough empty space (which is still present in 
% the spectra, but only contains noise) between each of the harmonic 
% blocks of the spectra. A zero-padded convolution is more correct, 
% but infinitely slower. The 'pad'-field is set to 0 by default.
p.addParameter('pad',0,@(x) @(x) isscalar(x) && isnumeric(x) );

% cell structure containing the relevantBins and excitedBins for each of
% the nodes. (present in spec) The information is sorted in a Nx3 cell
% array, in which N is the number of nodes that are given bin information.
% The first element should always be a string (referring to a node) and the
% next two signify relevantBins and excitedBins. If a node is used in the
% identification that is not in this cell structure, the properties of the
% first node (present in importantBins) will be used a substitute.
p.addParameter('importantBins',[],@(x) checkimportantBins(x) && iscell(x));

% Array of the bins which are excited and thus contain information content which is usefull for identification.
% It is recommended to use the more versatile importantBins cell structure instead of this
% parameter.
p.addParameter('excitedBins',[],@(x) isvector(x) && isnumeric(x));

% Array of the bins which are considered relevant for the identification/exctraction 
% of the BHA. This contains the excited bins as well as the detection lines
% for an odd multisine. It is recommended to use the more versatile importantBins 
% cell structure instead of this parameter.
p.addParameter('relevantBins',[],@(x) isvector(x) && isnumeric(x));

% This parameter will only be used when the excitedBins have to be extracted from the reference
% signal. In that case the signalLevel variable will be used as the threshold to
% seperate excited and unexcited bins. The default value for the field is -10.
p.addParameter('signalLevel',-10,@(x) isscalar(x) && isnumeric(x) );

p.StructExpand = true;
p.parse(spec,settings,varargin{:});
args = p.Results();

%% Do additional checks on the supplied input data and add missing fields and data

portCheck={'U','Y','R'};
uniquePorts={};
kk=1;

% Check if U has the following form
% U={{'in','out'}}
% If not we add another layer (or two) to the cell to make the data consistent.
for ii=1:length(portCheck)
    if ii~=3 || ~isempty(args.R)
        if ischar(args.(portCheck{ii}))
            %Give the correct format to the node
            args.(portCheck{ii})={{args.(portCheck{ii})}};
            %Add the port name to uniquePorts cell array
            uniquePorts{kk}= args.(portCheck{ii}){1}{1};
            kk=kk+1;
        else
            %If it ain't a cell array, we need to make it one!
            if ~iscell(args.(portCheck{ii}){1})
                    args.(portCheck{ii})={args.(portCheck{ii})};
            end
            for jj=1:length(args.(portCheck{ii}))
                %Add the port name to uniquePorts cell array
                for ll=1:length(args.(portCheck{ii}){jj})
                    if iscell(args.(portCheck{ii}){jj}{ll})
                        for mm=1:length(args.(portCheck{ii}){jj}{ll})
                            uniquePorts{kk}=args.(portCheck{ii}){jj}{ll}{mm};
                            kk=kk+1;
                        end
                    else
                        uniquePorts{kk}=args.(portCheck{ii}){jj}{ll};
                        kk=kk+1;
                    end
                end
            end
        end
    end
end
uniquePorts=unique(uniquePorts);

% Check if U,Y and R have the same length
if length(args.U)==length(args.Y)
    if ~isempty(args.R)
        if length(args.U)~=length(args.R)
            error('Length of U and R should be the same if R is given.');
        end
    end
else
    error('Length of the cell arrays U and Y should be the same.');
end

% Check if the bins have the correct format
% Either importantBins is given --> Needs to be checked
% Or relevantBins/excitedBins is given --> Needs to be converted
if ~isfield(args,'importantBins')
    %Check if excitedBins exists, is yes we assume that:
    %relevantBins == excitedBins
    if ~isfield(args,'excitedBins')
        %If excitedBins is empty, we attempt to extract it from the
        %reference signal.
        if  isempty(args.R)
            if ~isfield(args,'relevantBins')
                %Where the hell do you want me to extract the BLA, you retarded caveman?
                error('Please supply at least one of the following: excitedBins, relevantBins, importantBins or a reference signal.')
            else
                %No reference signal was found... just assume that all
                %relevant bins are excited.
                args.excitedBins=args.relevantBins;
            end
        else
            %Extract the excitedBins from the reference
            %The first realisation of the first reference will be used. The multisine should use the same grid anywhere anyway.
            %NOTE: Possible extension for zippered MS?
            args.excitedBins = find(db(args.spec.(args.R{1}{1})(1,1,:)>max(args.spec.(args.R{1}{1})(1,1,:))+args.signalLevel));
            args.excitedBins = args.excitedBins(args.excitedBins~=1);% remove DC if it is present
        end
    end
    if ~isfield(args,'relevantBins')
        args.relevantBins=args.excitedBins;
    end
    %importantBins needs to be created! Convention for all further
    %operations in the function.
    args.importantBins=cell(length(uniquePorts),3);
    for jj=1:length(uniquePorts)
        args.importantBins{jj,1}=uniquePorts{jj};
        args.importantBins{jj,2}=args.relevantBins;
        args.importantBins{jj,3}=args.excitedBins;
    end
else
    %importantBins exists and needs to be checked and additional fields need to be
    %created. First find the ports which are undefined.
    undefinedPorts=uniquePorts;
    for jj=1:length(undefinedPorts)
        if (sum(strcmp(args.importantBins(:,1),undefinedPorts{jj})))~=0
            undefinedPorts{jj} = [];
        end
    end
    undefinedPorts=undefinedPorts(~cellfun('isempty',undefinedPorts));
    
    idx=size(args.importantBins,1)+1;
    %Add the undefinedports to the importantBins cell array
    for jj=1:length(undefinedPorts)
        args.importantBins{idx,1}=undefinedPorts{jj};
        %Check if the undefinedPort is actually a conjugate of a given port
        if ~isempty(regexp(undefinedPorts{jj},'_conj','ONCE')) && (sum(strcmp(args.importantBins(:,1),undefinedPorts{jj}(1:regexp(undefinedPorts{jj},'_conj')-1)))) 
            kk=find(strcmp(args.importantBins(:,1),undefinedPorts{jj}(1:regexp(undefinedPorts{jj},'_conj')-1)),1);
        else %undefinedPort was truly not given and its bins should thus be added
            warning(['The important bins of one of the ports (' undefinedPorts{jj} ') were undefined.']);
            kk=1;
        end
        args.importantBins{idx,2}=args.importantBins{kk,2};
        args.importantBins{idx,3}=args.importantBins{kk,3};
        idx=idx+1;
    end
end

% Check if every field requested in U,Y or R is present in the spec
% Two possibilities exist:
% - We need the conjugate of a spectrum (U={{'in_conj'}})
% - A convolution is requested (U={{'in1','in2'}})
% The new spectra are calculated and added to the spec struct
% The U,Y and R cell arrays are also updated with the correect name of the new
% spectra.
M_check=[];
for ii=1:length(portCheck)
    for jj=1:length(args.(portCheck{ii}))       
        for kk=1:length(args.(portCheck{ii}){jj})
            if ischar(args.(portCheck{ii}){jj}{kk})
                if ~(isfield(args.spec,args.(portCheck{ii}){jj}{kk}))
                    %It's not a field in spec...so we most likely need 
                    %to calculate the conjugate and add it.
                    str=args.(portCheck{ii}){jj}{kk};
                    idx=regexp(str,'_conj');
                    if (isfield(args.spec,str(1:idx-1))) && ~isempty(idx)
                        %The conjugate is needed, but not present in the
                        %spec array. Thus it is added...
                        args.spec.(str)=conj(args.spec.(str(1:idx-1)));
                    else
                        error(['A string was found in ' portCheck{ii} ' which was unavailable in the spec struct.']);
                    end
                else
                    [M,P,N]=size(args.spec.(args.(portCheck{ii}){jj}{kk}));
                    if isempty(M_check)
                        M_check=M;
                    end
                    if ~(P==1 && N==length(args.spec.freq) && M==M_check)
                        error(['The dimensions of the field: ' (args.(portCheck{ii}){jj}) ' of spec are incorrect.'])
                    end
                end
            elseif iscell(args.(portCheck{ii}){jj}{kk})
                %We will need to convolve multiple spectra into one
                str_t=strcat(args.(portCheck{ii}){jj}{kk}{:});
                %Matbe the spectra is already present?
                if ~(isfield(args.spec,str_t))
                    %Check if every spectra is present
                    for ll=1:length(args.(portCheck{ii}){jj}{kk})
                        str=args.(portCheck{ii}){jj}{kk}{ll};
                        if ~(isfield(args.spec,str))
                            % It's most likely a conjugate? 
                            idx=regexp(str,'_conj');
                            if (isfield(args.spec,str(1:idx-1))) && ~isempty(idx)
                                args.spec.(str)=conj(args.spec.(str(1:idx-1)));
                            else
                                error(['A string was found in ' portCheck{ii} ' which was unavailable in the spec struct.']);
                            end
                        else
                            [M,P,N]=size(args.spec.(str));
                            if isempty(M_check)
                                M_check=M;
                            end
                            if ~(P==1 && N==length(args.spec.freq) && M==M_check)
                                error(['The dimensions of the field: ' str ' of spec are incorrect.'])
                            end
                        end
                    end
                 % Calculate the spectra and put it in spec using convolution in 
                 % the frequency domain.
                 [args.spec,args.importantBins]=convSpectra(args.(portCheck{ii}){jj}{kk},args.spec,args.importantBins,args.pad);
                end
                % Change the cell array to a concatenated string
                args.(portCheck{ii}){jj}{kk}=str_t;
            else
                error(['The ' portCheck{ii} '-cell array contained elements which were not recognized by the function.'])
            end
        end
    end
end

%Update the spectrum for user convenience
spec_update=args.spec;

%Update the settings structure for user convenience
settings_update.importantBins=args.importantBins;

%% Initiate variables (nu,ny,F,...)

NrBHA=length(args.U);

%Pre-allocation of the G struct
G=cell(1,NrBHA);
% G will contain the following fields:
% - mean
% - CvecG
% - varG
% - Z
% - CZ
    
%% Loop for extracting the different BLA, BMA, .... BHA

for ii=1:NrBHA
        %Get the number of ref,input and output ports
        nu=length(args.U{ii});
        ny=length(args.Y{ii});
        if ~isempty(args.R)
            if nu~=length(args.R{ii})
                error('Length of nr and nu should be the same.')
            end
        else
            nr=0;
        end
        
        %Do an additional check on M, is it a multiple of nu?
        if isempty(M_check)
            [M,~,~]=size(args.spec.(args.U{ii}{1}));
        end
        if rem(M,nu)
            error('The number of realizations (ME) is not a multiple of the number of input ports (nu).')
        end
        
        %Define the correct relevantBins and excitedBins from the
        %importantBins cell array
        Bins=cell(1,2);
        for kk=1:2 %Relevant and ExcitedBins have to be found
            for jj=1:(nu+ny)
                if jj==1
                    I=find(strcmp({args.importantBins{:,1}},args.U{ii}{jj})==1,1);
                    Bins{kk}=args.importantBins{I,kk+1};
                elseif jj>nu
                    I=find(strcmp({args.importantBins{:,1}},args.Y{ii}{jj-nu})==1,1);
                    Bins{kk}=intersect(Bins{kk},args.importantBins{I,kk+1});
                else
                    I=find(strcmp({args.importantBins{:,1}},args.U{ii}{jj})==1,1);
                    Bins{kk}=intersect(Bins{kk},args.importantBins{I,kk+1});
                end
            end
        end
        
        NrelevantBins=length(Bins{2}); %Total amount of bins at which relevant information is present
        NexcitedBins=length(Bins{1}); %Number of bins that are actually excited
        
        %pre-allocation of input data
        if nr~=0
            R=zeros(nu,nu,round(M/nu),NrelevantBins);
        else
            R=[];
        end
        U=zeros(nu,nu,round(M/nu),NrelevantBins);
        Y=zeros(ny,nu,round(M/nu),NrelevantBins);
        
        %Get the spectra (U,Y and R if neccessary) in the correct format
        for jj=1:round(M/nu)
            for kk=1:nu
                for ll=1:nu
                    U(ll,kk,jj,:)=args.spec.(args.U{ii}{ll})(kk+(jj-1)*(nu),1,Bins{1});
                    if ~isempty(args.R)
                         R(ll,kk,jj,:)=args.spec.(args.R{ii}{ll})(kk+(jj-1)*(nu),1,Bins{1});
                    end
                end
                for ll=1:ny
                    Y(ll,kk,jj,:)=args.spec.(args.Y{ii}{ll})(kk+(jj-1)*(nu),1,Bins{1});
                end
            end
        end

        if isempty(Bins{2}) || isempty(Bins{1})
            error(['The collection of common frequency bins between input and output was found to be empty ( NrBHA=' num2str(jj) ' )']);
        end
        
        %Run the BLA extraction function.
        [G{ii}.mean,G{ii}.CvecG,G{ii}.Z,G{ii}.CZ]=extractMIMO_BLA(U(:,:,1:round(M/nu),:),Y(:,:,1:round(M/nu),:),'R',R,'excitedBins',Bins{2}-Bins{1}(1)+1);
        
        %Extraction of the total covariance matrix from the robust method
        if M/nu~=1
            G{ii}.varG = zeros(ny, nu, NexcitedBins);
            for ff = 1:NexcitedBins
                G{ii}.varG(:,:,ff) = reshape(diag(squeeze(G{ii}.CvecG(:,:,ff))) , [ ny , nu ] );
            end
        end
        
end           

end
    
%% function to check the spectrum passed to the function
function res = checkSpec(s)
    % s should contain a freq field
    if ~isfield(s,'freq')
        error('spec.freq should be provided');
    end
% if you pass all the checks, you are happy!
res = 1;
end

%% function to check the port data passed to the function
function res=checkUY(s)
    
    %Each of the elements of s should be either a string or another cell array
    %If it is another cell array we delve deeper and recursively call this
    %function again.
    if iscell(s)
        for ii=1:length(s)
            if checkUY(s{ii})
                res=1;
            else
                res=0;
            end
        end
    elseif ischar(s)
        res=1;
    else
        res=0;
    end

end

%% Function to convolute two HB spectra in a time efficient manner
% Also calculates the relevant and excited bins for the convolved spectra
% Zero pad
function [spec,importantBins]=convSpectra(node,spec,importantBins,pad)

%Get the frequency resolution
    f0=mode(spec.freq-circshift(spec.freq,[0 1]));

%Conv_HB cal;culates the convolution of the relevant spectra
    spec = conv_HB(spec,node,pad);

%Now it is time to combine the importantBins cell arrays of the convoluted
%spectra using kronsum, In this way we find the relevant bins of our new
%spectra...
    bin_dummy=cell(1);
    %We add a new row to the importantBins cell array
    importantBins_new=cell(length(importantBins)+1,3);
    %Get the old data in the new cell array
    importantBins_new(1:size(importantBins,1),:)=importantBins;
    %The node name is already known
    importantBins_new{length(importantBins)+1,1}=(strcat(node{:}));
    %NOTE: The minus 1 is there because DC should be at 0 and not 1!
    for jj=1:2
        for ii=1:length(node)
            I=find(strcmp(importantBins(:,1),node{ii})==1,1);
            grid=round(spec.freq(importantBins{I,jj+1})/f0);
            if ii>1 %Time for kronecker sum!
                bin_dummy{1}=unique(kronsum(bin_dummy{1},[-grid grid]));
            else %Only one binarray available....
                %Positive AND negative frequencies need to be accounted
                %for!
                bin_dummy{1}=unique([-grid grid]);
            end
        end
        % Extend importantBins correctly with the new bins
        bin_dummy{1}=unique(abs(bin_dummy{1}));
        for ii=1:length(bin_dummy{1})
            bin_dummy{1}(ii)=find(round(spec.freq/f0)==bin_dummy{1}(ii),1);
        end
        importantBins_new{length(importantBins)+1,jj+1}=bin_dummy{1};
    end

    importantBins=importantBins_new;
end

%% function to check the importantBins passed to the function
function res=checkimportantBins(s)
    %Check the cell dimensions
    [N,E]=size(s);
    
    if E~=3
        error(['Dimension of the importantBins parameter was incorrect! (Dim2=3 was expected, but ' num2str(E) ' was given instead)'])
    end
    
    for ii=1:N
        if ~ischar(s{ii,1})
            error('First element of the importantBins parameter is supposed to be string.');
        end
        if ~(isvector(s{ii,2}) && isnumeric(s{ii,2})) || ~(isvector(s{ii,3}) && isnumeric(s{ii,3}))
            error('Second or third element of the importantBins parameter is incorrect.')
        end
    end
    
    res=1;
end

%% Kronecker sum to calculate the grid of two convoluted spectra
function z = kronsum(x, y)
    z = kron(x, ones(size(y))) + kron(ones(size(x)), y);
end

% @generateFunctionHelp
% @author Piet Bronders,
% @author Adam Cooman
% @institution ELEC, VUB

% @tagline calculates the BLA, BMA and consequent transfer functions (user-specified) given a reference spectra, input spectra and output spectra.

% @description Here several valid possibilities for the 'U'-cell array are
% @description given. (1) 'in1', {{'in1'}} or {'in1'} (1 SISO BLA);(2) {'in1','in2'} (1 MISO BLA);(3)
% @description {'in1',{'in2','in3'}} (1 MISO BLA/BMA);(4) U{1}={{'in1'}}, U{2}={{'in2'}}
% @description (2 SISO BLA); combinations of the previous examples are possible...

% @version 16-04-2015  Version 1.
% @version 23-04-2015  Fixed multiple errors in regards to the extraction of negative BMAs.
% @version 20-05-2015  Gigantic overhaul of the entire function. The function can now internally make the convolution of input spectra.
% @version 11-06-2015  The function uses the 'conv_HB'-function, a MUCH faster alternative than the slowpoke functionalities used before.

% @output1 Cell array of struct containing the calculated BHA, its variance and much more...
% @outputType1 cell array of structs

% @output2 the updated spec struct for user convenience.
% @outputType2 struct

% @output3 the updated settings struct for user convenience.
% @outputType3 struct

% @note Several possibilities for giving the correct bin content to the function are possible and are listed here:
% @note (1) The 'importantBins'-parameter is given. 
% @note (2) None of the fields are given, but a reference signal is available. 
% @note (3) Both 'relevantBins' and 'excitedBins' are given, 'importantBins' can be left empty in that case.
% @note (4) Only 'relevantBins' or 'excitedBins' is given.

%% generateFunctionHelp: old help, backed up at 11-Jun-2015. leave this at the end of the function
% calcMIMO_BHA_v2 calculates the BLA, BMA and consequent transfer functions (user-specified) given a reference spectra, input spectra and output spectra.
% 
%      [G,spec_update,settings_update] = calcMIMO_BHA_v2(spec,settings);
%      [G,spec_update,settings_update] = calcMIMO_BHA_v2(spec,settings,'ParamName',paramValue,...);
% 
% Several valid possibilities for the 'U'-cell array are given: 
% (1) 'in1', {{'in1'}} or {'in1'} (1 SISO BLA);
% (2) {'in1','in2'} (1 MISO BLA);
% (3) {'in1',{'in2','in3'}} (1 MISO BLA/BMA);
% (4) U{1}={{'in1'}}, U{2}={{'in2'}} (2 SISO BLA); 
% combinations of the previous examples are possible...
% 
% Required inputs:
% - spec      check: @(x) checkSpec(x) && isstruct(x)
%      spec structure containing the relevant data fields. (Output
%      of the HB simulation)
% - settings      check: @(x) checkSettings(x) && isstruct(x)
%      Struct containing several settings for the BHA extracton with
%      not all of them being  necessary. A first optional field is
%      the 'signLvl'-field. This field will  only be used when the
%      excitedBins have to be extracted from the reference  signal.
%      In that case the 'signLvl'-field will be used as the threshold
%      to  seperate excited and unexcited bins. The default value for
%      the field is -10.  The second field is 'pad', this variable
%      indicates if the spectra used in the convolution will be zero-padded
%      or not. A non zero-padded  convolution will only be a good approximation
%      if there's enough empty  space (which is still present in the
%      spectra, but only contains noise) between each of the harmonic
%      blocks of the spectra. A zero-padded  convolution is more correct,
%      but infinitely slower. The 'pad'-field is  set to 0 by default.
%      Lastly we have the 'excitedBins','relevantBins' and  'importantBins'-fields.
%      The user has multiple choices for these fields.  (1) The 'importantBins'-field
%      is given. (2) None of the fields are given,  but a reference
%      signal is available. (3) Both 'relevantBins' and  'excitedBins'
%      are given, 'importantBins' can be left empty in that case. 
%      (4) Only 'relevantBins' or 'excitedBins' is given.
% 
% Parameter/Value pairs:
% - 'U'     default: {'in'}  check: @(x) checkUY(x) && isvector(x) && (iscell(x) || ischar(x))
%      Cell array of cell array of strings containing the names of
%      the different input  spectra. The first cell dimension indicates
%      the number of BHA which should be  extracted, while the second
%      dimension indicates the number of input ports.  The strings
%      should be valid fields of the spec struct, but there  are exceptions.
%      A first exception is regarding the '_conj'-extension.  When
%      this extensaion is found to be present AND the 'node_conj'-field
%      is not part of  the spec struct, the function will attempt to
%      take the conjugate of the  'node'-field (which SHOULD be present
%      in spec). Secondly the "string" can be swapped  by a cell array
%      of strings instead. (A 3rd dimension) The function will  than
%      take the convolution of all of the respective fields of the
%      spec  struct. Afterwards this cell array will then be replaced
%      by a  concatenated string (Concatenation of all the strings
%      in this cell  array). Other (simplified) cell arrays can also
%      be processed (ex. when only  1 BHA needs to be extracted, the
%      first dimension of cell arrays can be removed).  Several examples
%      can be found in the function description.
% - 'Y'     default: {'out'}  check: @(x) checkUY(x) && isvector(x) && (iscell(x) || ischar(x))
%      Cell array of strings containing the names of the different
%      output  spectra. See the flavor text of 'U' for more information.
% - 'R'     default: []  check: @(x) checkUY(x) && isvector(x) && (iscell(x) || ischar(x))
%      Cell array of strings containing the names of the different
%      reference spectra,  The reference is however not strictly necessary.
%      See flavor text of 'U' for more information.
% 
% Outputs:
% - G      Type: cell array of structs
%      Cell array of struct containing the calculated BHA, its variance
%      and much more...
% - spec_update      Type: struct
%      the updated spec struct for user convenience.
% - settings_update      Type: struct
%      the updated settings struct for user convenience.
% 
% Piet Bronders, Adam Cooman, ELEC, VUB
% 
% Version info:
%  16-04-2015  Version 1.
%  23-04-2015  Fixed multiple errors in regards to the extraction of negative BMAs.
%  20-05-2015  Gigantic overhaul of the entire function. The function can now internally make the convolution of input spectra.
% 
% This documentation was generated with the generateFunctionHelp function at 20-May-2015
