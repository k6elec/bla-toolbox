function [G, CvecG, Z, CvecZ, G_noref, CvecG_noref] = extractMIMO_BLA_v2(U,Y,varargin)
% extractMIMO_BLA Calculates the MIMO BLA starting from pre-processed data arrays.
% 
%      [G,CvecG,CvecZ,Z] = extractMIMO_BLA(U,Y);
%      [G,CvecG,CvecZ,Z] = extractMIMO_BLA(U,Y,'ParamName',paramValue,...);
% 
% In the following, we use 
%   - F to indicate the amount of tones in the multisine
%   - M for the amount of phase realisations.
%   - nu for the amount of inputs of the system
%   - ny for the amount of outputs of the system
%   - ne for the amount of experiments
% 
% Required inputs:
% - U      check: @checkData
%      The input spectral data. [nu X ne X M X F]
% - Y      check: @checkData
%      The input spectral data. [ny X ne X M X F]
% 
% Parameter/Value pairs:
% - 'R'     default: []  check: @checkData
%      The reference spectral data. [nu X nu X M X F]
% - 'excitedBins'     default: []  check: @(x) isnumeric(x) & isvector(x)
%      The excited frequency bins of the given spectral data.
% 
% Outputs:
% - G      Type: array
%      The calculated MIMO BLA [ny X nu X F]
% - CvecG      Type: array
%      the Covariance matrix of the BLA  [ny*nu X nu*nu X F]
% - CZ      Type: array
%      the Covariance matrix of the stacked input-output vectors [ny+nu
%      X ny+nu X nu X F]
% - Z      Type: array
%      stacked input-output spectra, averaged over the realisations
%      [(ny+nu) X nu X F]
% 
% Rik Pintelon Piet Bronders, Adam Cooman, ELEC, VUB
% 
% Version info:
%  22-04-2013  Version 1.
% 
% This documentation was generated with the generateFunctionHelp function at 13-May-2015

%% parse the input

p = inputParser();
p.FunctionName = 'extractMIMO_BLA';
% The input spectral data. [nu X ne X M X F]
p.addRequired('U',@checkData);
% The input spectral data. [ny X ne X M X F]
p.addRequired('Y',@checkData);
% The reference spectral data. [ne X ne X M X F]
p.addParameter('R',[],@checkData);
% The excited frequency bins of the given spectral data.
p.addParameter('excitedBins',[],@(x) isnumeric(x) & isvector(x));
% if plot is set to true, several plots are 
p.addParameter('plot',false,@islogical)
p.StructExpand = true;
p.parse(U,Y,varargin{:});
args = p.Results();
clear U Y p

%% Initialisation of the variables

% Check the consistency of the input data
[nu,ny,ne,M,F_all,Reference] = checkInputs(args.U,args.Y,args.R);        

% if the excited bins are not provided, assume every bin is an excited bin
if isempty(args.excitedBins)
    args.excitedBins=1:F_all;
end
% excited harmonic numbers
args.excitedBins = args.excitedBins(:).';
% number of excited frequencies 
F = length(args.excitedBins);                

% Create the Z_all matrix, stacked vector of U and Y. 
% Its size is [ny+nu ne M F]
Z_all = [args.Y; args.U];


if (M == 1) 
    %% if there's only one realisation, stuff is boring
    % if only one realisation is available, we can calculate G, but cannot
    % tell anything about the uncertainty on the estimate. The LPM could be
    % used here, but we don't like the LPM :)
    Z = squeezeDim(Z_all,3);
    CvecZ = [];
    % calculate the BLA using the FRMs from reference to input and from reference to output
    G = zeros(ny, nu, F);
    Yt = zeros(ny,ne);
    Ut = zeros(nu,ne);
    for ff = 1:F
        Yt(:,:) = Z(      1:ny  , : , ff );
        Ut(:,:) = Z( (ny+1):end , : , ff );
        % G is given by Y/U
        G(:, :, ff) = Yt / Ut;
    end
    clear Yt Ut
    CvecG = [];
else
    %% if there's more than one realization we actually have something to do
    if Reference
        % check the orthogonality of R
        condR = zeros(M,F);
        for mm=1:M
            for ff=1:F
                condR(mm,ff) = cond(args.R(:,:,mm,ff)/diag(diag(abs(args.R(:,:,mm,ff)))));
            end
        end
        if any(any(condR>1e3))
            warning('Your reference signal is badly conditioned. max(cond(R))=%d',max(max(condR)));
        end
        
        % determine the FRF from R to Y and U
        for mm = 1:M
            for ff = 1:F
                Z_all(:, :, mm, ff) = Z_all(:, :, mm, ff) / args.R(:,:,mm,ff);
            end
        end
        
        % sample mean input-output spectra over the M realisations
        Zmean = mean(Z_all, 3); % equation (46)
        
        % input-output residuals
        Zres_all = Z_all - repmat(Zmean, [1, 1, M, 1]);
        
        % calculate the covariance of the Z matrix
        CvecZ = zeros((ny+nu)*ne, (ny+nu)*ne , F);
        for ff = 1:F
            CvecZ(:, :, ff) = cov_matrix(Zres_all(:,:,:,ff));
        end
        % total covariance of the mean value
        CvecZ = CvecZ/M;
        
        % remove the singular realisation dimension
        Z = squeezeDim(Zmean,3); % Z is [ny+nu x ne x F]
        
        % plot the estimated Z-matrix and the different realisations
        if args.plot
            varZ = reshape(diag_FRM(CvecZ),[ny+nu ne F]);
            h=figure(1001);
            set(h,'name','amplitude of the estimated Z matrix')
            for yy=1:(ny+nu)
                for ee=1:ne
                    subplot(ny+nu,ne,(yy-1)*ne+ee)
                    errorbar_mimo(1:F,Z(yy,ee,:),sqrt(varZ(yy,ee,:)),'b+',@db);
                    hold on
                    for mm=1:M; 
                        plot_mimo(1:F,db(squeeze(Z_all(yy,ee,mm,:))),'-'); 
                    end
                end
            end
            h=figure(1002);
            set(h,'name','phase of the estimated Z matrix')
            for yy=1:(ny+nu)
                for ee=1:ne
                    subplot(ny+nu,ne,(yy-1)*ne+ee)
                    errorbar_mimo(1:F,Z(yy,ee,:),sqrt(varZ(yy,ee,:)),'b+',@angle);
                    hold on
                    for mm=1:M; 
                        plot_mimo(1:F,unwrap(angle(squeeze(Z_all(yy,ee,mm,:)))),'-'); 
                    end
                end
            end
        end
        
        % calculate the BLA using the FRMs from reference to input and from reference to output
        G = zeros(ny, nu, F);
        Yt = zeros(ny,ne);
        Ut = zeros(nu,ne);
        for ff = 1:F
            Yt(:,:) = Z(      1:ny  , : , ff );
            Ut(:,:) = Z( (ny+1):end , : , ff );
            % G is given by Y/U
            G(:, :, ff) = Yt / Ut;
        end
        clear Yt Ut
        
        % We use the first part of expression (106) to calculate CvecG
        CvecG = zeros(ny*nu,ny*nu,F);
        for ff=1:F
            Vt = [eye(ny) -G(:,:,ff)];
            Ut = pinv(Z(ny+1:end,:,ff));
            T=kron(Ut.',Vt);
            CvecG(:,:,ff) = T*CvecZ(:,:,ff)*T';
        end
        
    end % if R is available
    
    % Calculate CvecG using the second approach. This will
    % work better when the generator is the source of errors. We
    % ignore the reference signal and
    % G_all is the G^[m] of the paper.
    G_all = zeros(ny, nu, M, F);
    Yt = zeros(ny,ne);
    Ut = zeros(nu,ne);
    for mm = 1:M
        for ff=1:F
            Yt(:,:) = Z_all(      1:ny  , : , mm , ff );
            Ut(:,:) = Z_all( (ny+1):end , : , mm , ff );
            G_all(:, :, mm , ff) = Yt / Ut;
        end
    end
    clear Ut Yt
    
    % calculate the BLA without reference
    G_noref = mean(G_all, 3); % equation (56)
    
    % calculate the residuals
    Gres_all = G_all - repmat(G_noref, [1, 1, M, 1]);
    
    % calculate the covariance matrix
    CvecG_noref=zeros(ny*nu,ny*nu,F);
    for ff=1:F
        CvecG_noref(:,:,ff) = cov_matrix(Gres_all(:,:,:,ff))/M;
    end
    
    if args.plot
        varG = reshape(diag_FRM(CvecG_noref),[ny nu F]);
        h=figure(1003);
        set(h,'name','amplitude of the estimated G-matrix')
        for uu=1:nu
            for yy=1:ny
                subplot(nu,ny,(uu-1)*nu+yy)
                errorbar_mimo(1:F,G(yy,uu,:),sqrt(varG(yy,uu,:)),'b+',@db);
                plot(1:F,db(squeeze(G_noref(yy,uu,:))),'o')
                for mm=1:M
                    plot(1:F,db(squeeze(G_all(yy,uu,mm,:))),'b-')
                end
            end
        end
        h=figure(1004);
        set(h,'name','phase of the estimated G-matrix')
        for uu=1:nu
            for yy=1:ny
                subplot(nu,ny,(uu-1)*nu+yy)
                errorbar_mimo(1:F,G(yy,uu,:),sqrt(varG(yy,uu,:)),'b+',@angle);
                plot(1:F,angle(squeeze(G_noref(yy,uu,:))),'o')
                for mm=1:M
                    plot(1:F,unwrap(angle(squeeze(G_all(yy,uu,mm,:)))),'b-')
                end
            end
        end
    end
    
    if ~Reference
        G = G_noref;
        CvecG = CvecG_noref;
        Z = [];
        CvecZ = [];
    end
    
end
end


function [nu,ny,ne,M,F_all,Reference]=checkInputs(U,Y,R)
    % check whether U and Y are 4-D matrices
    if ndims(Y)~=4; error('Y should be a [ny x ne x M x F] matrix');end
    if ndims(U)~=4; error('U should be a [nu x ne x M x F] matrix');end
    
    % get all the dimensions out of Y
    [ny,ne,M,F_all] = size(Y);
    % check the dimensions of U
    [nu,net1,Mt,Ft]=size(U);
    
    % ne should be the same for both matrices
    if net1~=ne
        error(sptintf('The amount of experiments is not the same for U (%d) and Y (%d)',net1,ne));
    end
    % ne should be bigger than nu
    if ne<nu
        error('The amount of experiments is smaller than the amount of inputs');
    end
    % the amount of realisations should be the same for both matrices
    if Mt~=M
        error('The amount of realisations is not the same for U and Y');
    end
    % the amount of frequencies should be the same for both matrices
    if Ft~=F_all
        error('The amount of frequencies is different for U and Y');
    end
    
    % if R is passed, Check R
    if ~isempty(R)
        % check whether R is a 4-D matrix
        if ndims(Y)~=4; error('R should be a [ne x ne x M x F] matrix');end
        [net1,net2,Mt,Ft]=size(R);
        if net2~=ne
            error('The amount of experiments should be the same for U Y and R');
        end
        if net1~=ne
            error('The amount of reference signals should be the same as the amount of experiments');
        end
        if Mt~=M
            error('The number of realisations should be the same for U Y and R');
        end
        if Ft~=F_all
            error('The number of frequencies should be the same for U Y and R');
        end
        Reference = 1;
    else
        Reference = 0;
    end
end

function res=checkData(s)
if ~isempty(s)
    if max(size(size(s)))~=4
        error('The dimension of the input data was incorrect.')
    end
end
res=1;
end



% @generateFunctionHelp
% @author Rik Pintelon,
% @author Piet Bronders,
% @author Adam Cooman
% @institution ELEC, VUB

% @tagline Calculates the MIMO BLA starting from pre-processed data arrays.

% @description In the following, we use 
% @description  - F to indicate the amount of tones in the multisine
% @description  - M for the amount of phase realisations.
% @description  - nu for the amount of inputs of the system
% @description  - ny for the amount of outputs of the system
% @description  - ne for the amount of experiments

% @version 22-04-2013  Version 1.

% @output1 The calculated MIMO BLA [ny X nu X F]
% @outputType1 array

% @output2 the Covariance matrix of the BLA  [ny*nu X nu*nu X F]
% @outputType2 array

% @output3 stacked input-output spectra, averaged over the realisations [(ny+nu) X nr X F]
% @outputType3 array

% @output4 the Covariance matrix of the stacked input-output vectors [(ny+nu)*nr X (ny+nu)*nr X F]
% @outputType4 array

%% generateFunctionHelp: old help, backed up at 13-May-2015. leave this at the end of the function
% extractMIMO_BLA Calculates the MIMO BLA starting from pre-processed data arrays.
% 
%      [G,CvecG,CZ,Z] = extractMIMO_BLA(U,Y);
%      [G,CvecG,CZ,Z] = extractMIMO_BLA(U,Y,'ParamName',paramValue,...);
% 
% In the following, we use F to indicate the amount of tones in the multisine and
% M for the amount of realisations of te MS.
% 
% Required inputs:
% - U      check: @checkData
%      The input spectral data. [nu X nu X M X F]
% - Y      check: @checkData
%      The input spectral data. [ny X nu X M X F]
% 
% Parameter/Value pairs:
% - 'R'     default: []  check: @checkData
%      The reference spectral data. [nu X nu X M X F]
% - 'excitedBins'     default: []  check: @(x) isnumeric(x) & isvector(x)
%      The excited frequency bins of the given spectral data.
% 
% Outputs:
% - G      Type: array
%      The calculated MIMO BLA [ny X nu X F]
% - CvecG      Type: array
%      Total variance of the BLA  [ny X nu X F]
% - CZ      Type: array
%      the Covariance matrix of the BLA [ny*nu X ny*nu X F]
% - Z      Type: array
%      stacked input-output spectra, averaged over the realisations
%      [(ny+nu) X nu X F]
% 
% Rik Pintelon Piet Bronders, Adam Cooman, ELEC, VUB
% 
% Version info:
%  22-04-2013  Version 1.
% 
% This documentation was generated with the generateFunctionHelp function at 22-Apr-2015
