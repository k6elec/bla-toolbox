function [spec,AC] = runSubstractSimulation(Netlist,MSdef,varargin)
% runZipperSimulation adds tickler multisines to the specified nodes and runs a harmonic balance simulation
% 
%      spec = runZipperSimulation(Netlist,MSdef);
%      spec = runZipperSimulation(Netlist,MSdef,'ParamName',paramValue,...);
% 
% in a Substract simulation, Tickler sources are placed on the same
% frequency grid as the main multisine. The circuit response without
% ticklers is determined first. Then the circuit is simulated with the same
% multisine, but with added tickler sources. The response of the tickler
% sources is obtained by substracting the original response from the
% tickled response
% 
% Required inputs:
% - Netlist      check: @(x) ischar(x)||iscellstr(x)
%      ADS netlist that has to be simulated. The netlist should not
%      contain any  multisine source or simulation statement, these
%      are added by the function
% - MSdef      check: @isstruct
%      definition of the large multisine. Use MScreate to build this
%      multisine
% 
% Parameter/Value pairs:
% - 'TicklerNodes'     default: {}  check: @checknodelist
%      list of nodes where the ticklers should be connected to.
% - 'TicklerImpedances'     default: Inf  check: @isvector
%      output impedances of the tickler multisines. The default is
%      a current source
% - 'TicklerAmplitudes'     default: 1e-9  check: @isvector
%      ampltude of the tickler multisines
% - 'outOfBandFactor'     default: 1  check: @isscalar
%      ratio between the bandwidth of the tickler multisines and the
%      large  multisine. Use this to determine the out-of-band BLA
% - 'TicklerMSdefs'     default: []  check: @isstruct
%      for fancy stuff, you can provide a vector of multisine structs
%      to this  function as well. These multisines need to be on the
%      same frequency grid  as the large multisine. The function will
%      give the multisines a small  frequency offset to zipper the
%      multisines.
% - 'TicklerDistance'     default: 1  check: @isscalar
%      frequency difference between the tickler and the main multisine
% - 'oversample'     default: 10  check: @isnatural
%      estimated orded of non-linearity of the underlying system
% - 'numberOfRealisations'     default: 7  check: @isnatural
%      number of multisine realisations
% - 'measureTickler'     default: false  check: @islogical
%      if this is set to true, a current probe is added to the tickler
%      sources  to allow measuring of the reference current. The currents
%      are called  I_ticklerX in the resulting struct.
% 
% Outputs:
% - spec      Type: struct
%      contains the results of the simulation.
% 
% Adam Cooman, ELEC,VUB
% 
% Version info:
%  01/06/2015 Version 1
% 
% This documentation was generated with the generateFunctionHelp function at 01-Jun-2015


p = inputParser();
% ADS netlist that has to be simulated. The netlist should not contain any
% multisine source or simulation statement, these are added by the function
p.addRequired('Netlist',@(x) ischar(x)||iscellstr(x));
% definition of the large multisine. Use MScreate to build this multisine
p.addRequired('MSdef',@isstruct);
% list of nodes where the ticklers should be connected to.
p.addParameter('TicklerNodes',{},@checknodelist);
% output impedances of the tickler multisines. The default is a current source
p.addParameter('TicklerImpedances',Inf,@isvector);
% ampltude of the tickler multisines
p.addParameter('TicklerAmplitudes',1e-9,@isvector);
% ratio between the bandwidth of the tickler multisines and the large
% multisine. Use this to determine the out-of-band BLA
p.addParameter('outOfBandFactor',1,@isscalar);
% estimated orded of non-linearity of the underlying system
p.addParameter('oversample',10,@isnatural);
% number of multisine realisations
p.addParameter('numberOfRealisations',7,@isnatural);
% if this is set to true, a current probe is added to the tickler sources
% to allow measuring of the reference current. The currents are called
% I_ticklerX in the resulting struct.
p.addParameter('measureTickler',false,@islogical);
% if this is set to true, a full orthogonal multisine is used for the
% tickler multisines. Otherwise, the tickler signal is added one at a time,
% to obtain a diagonal excitation matrix
p.addParameter('FOMStickler',false,@islogical);
% maximum crest factor of the multisines
p.addParameter('CF',Inf,@isscalar);
% simulation technique used in the large-signal simulation
p.addParameter('simulator','HB',@(x) any(strcmpi(x,{'HB','TRAN','ENV'})));
% if ACcheck is set to true, an AC simulation will be performed from the
% different multisine sources. You can use this AC information to estimate
% the S-parameter of the stages as well
p.addParameter('ACcheck',false,@islogical)
p.parse(Netlist,MSdef,varargin{:});
args = p.Results;
clear Netlist MSdef varargin p

% put the nodelist in a correct cell array
[~,args.TicklerNodes] = checknodelist(args.TicklerNodes);

% F is the amount of excited frequencies
F = length(args.MSdef.grid);
% M is the number of realisations
M = args.numberOfRealisations;
% generate a random phase for the large multisine, but keep an eye on the
% crest factor
phase = zeros(F,M)*2*pi;
for mm=1:M
    attempt = 0;
    CF=Inf;
    while CF>=args.CF
        phase(:,mm) = 2*pi*rand(size(args.MSdef.grid));
        args.MSdef.phase = phase(:,mm);
        period = MScalculatePeriod(args.MSdef,16)-args.MSdef.DC;
        CF = max(abs(period))/sqrt(mean(period.^2));
        attempt=attempt+1;
        if attempt>99
            error('could not obtain the wanted crest factor with 100 random phase attempts, it is too strict')
        end
    end
end

% first simulate with the large multisine only
spec_large = ADSsimulateMS(args.Netlist,args.MSdef,'simulator',args.simulator,'phase',phase);

% If required, add current probes to the netlist to measure the reference current
if args.measureTickler
    % open the netlist file
    if ~iscell(args.Netlist)
        args.Netlist = readTextFile(args.Netlist);
    end
    % add the current probe to the tickler node and replace the name of the
    % node with a new 
    for ii=1:length(args.TicklerNodes)
        % Short:I_Probe1  Tickle1 Node Mode=0 SaveCurrent=yes 
        args.Netlist{end+1} = ADSaddShort(-1,sprintf('I_tickle%d',ii),...
            'sink',sprintf('Tickle%d',ii),'source',args.TicklerNodes{ii}{1},...
            'Mode',0,'SaveCurrent','yes');
        args.TicklerNodes{ii}{1} = sprintf('Tickle%d',ii);
    end
end

% if the user doesn't provide the multisine structs for the tickers
% himself, we generate them now using the other input parameters
args.TicklerMSdefs = generateTicklerMSdefs(args);
% T is the amount of ticklers
T = length(args.TicklerMSdefs);

% combine all the MSdef structs
MSdef = [args.MSdef args.TicklerMSdefs];
% and generate the phase for all the experiments
phaseT = zeros(F,T*M,T+1);
for mm=1:M
    phaseT(:,((T*(mm-1))+1):(mm*T),1) = repmat(phase(:,mm),[1,T]);
end
phaseT(:,:,2:end) = getFOMSphase(F,T*M,T);
phasecell = cell(T+1,1);
for tt=1:T+1
    phasecell{tt} = squeeze(phaseT(:,:,tt));
end

if ~args.FOMStickler
    ampcell = cell(T+1,1);
    % the main multisine is always on;
    ampcell{1} = repmat(args.MSdef.ampl(:),[1 T*M]);
    for tt=2:T+1
        ampcell{tt}=zeros(F,T*M);
        ampcell{tt}(:,(0:M-1)*T+tt-1)=1e-9;
    end
    % now run the simulation with both large MS and the ticklers active
    spec_tickled = ADSsimulateMS(args.Netlist,MSdef,'simulator',args.simulator,'phase',phasecell,'ampl',ampcell);
else
    % now run the simulation with both large MS and the ticklers active
    spec_tickled = ADSsimulateMS(args.Netlist,MSdef,'simulator',args.simulator,'phase',phasecell);
end

if args.ACcheck
    AC = ADSsimulateAC(args.Netlist,MSdef);
else
    AC=[];
end

%% combine both simulation results in the spec struct
fields = unique([fieldnames(spec_large);fieldnames(spec_tickled)]);
F = length(spec_large.freq);
for ii=1:length(fields);
    if ~strcmp(fields{ii},'freq')
        spec.(fields{ii}) = zeros(M*(T+1),1,F);
        for mm=1:M
            % the first experiment contains the large multisine only
            if isfield(spec_large,fields{ii})
                spec.(fields{ii})((mm-1)*(T+1)+1,1,:) = spec_large.(fields{ii})(mm,1,:);
            else
                spec.(fields{ii})((mm-1)*(T+1)+1,1,:) = 0;
            end
            % the other experiments contain the difference between small and large
            if isfield(spec_large,fields{ii})
                for tt=1:T
                    spec.(fields{ii})((mm-1)*(T+1)+tt+1,1,:) = spec_tickled.(fields{ii})((mm-1)*T+tt,1,:)-spec_large.(fields{ii})(mm,1,:);
                end
            else
                for tt=1:T
                    spec.(fields{ii})((mm-1)*(T+1)+tt+1,1,:) = spec_tickled.(fields{ii})((mm-1)*T+tt,1,:);
                end
            end
        end
    end
end
% add the frequency axis
spec.freq = spec_large.freq;

end

%% function to generate the different MSdefs
function MSdefs = generateTicklerMSdefs(args)
% generates the MSdef structs for the different ticklers multisines

% T is the amount of tickers added
T = length(args.TicklerNodes);
% check the impedance vector
if length(args.TicklerImpedances)==1;
    args.TicklerImpedances = args.TicklerImpedances*ones(T,1);
else
    if length(args.TicklerImpedances)~=T;
        error('The length of the vector of tickler impedances can be either one or equal the atickler nodes');
    end
end
% check the amplitude vectors
if length(args.TicklerAmplitudes)==1;
    args.TicklerAmplitudes = args.TicklerAmplitudes*ones(T,1);
else
    if length(args.TicklerAmplitudes)~=T;
        error('The length of the vector of tickler impedances can be either one or equal the atickler nodes');
    end
end
% now build the multisines for the ticklers
for ii=1:T
    MSdefs(ii) = args.MSdef;
    MSdefs(ii).ampl = args.TicklerAmplitudes(ii)*ones(size(args.MSdef(1).grid));
    MSdefs(ii).MSnode = args.TicklerNodes{ii};
    MSdefs(ii).Rout = args.TicklerImpedances(ii);
    MSdefs(ii).DC = 0;
    
end

end

%% function to check the provided node list
function [tf,n] = checknodelist(n,num)
% this function checks a nodelist
if ~exist('num','var')
    num=2;
end
err = 'the node list should be a string, a cell array of strings or a cell array of a cell array of strings';
if ~iscell(n)
    if ischar(n)
        n = {{n,'0'}};
    else
        error(err)
    end
else
    if isvector(n)
        for ii=1:length(n)
            if ischar(n{ii})
                n{ii} = {n{ii},'0'};
            else
                if ~iscellstr(n{ii})
                    error(err)
                else
                    switch length(n{ii})
                        case 1
                            % add '0' for all the other nodes
                            n{ii} = [n(ii) repmat({'0'},num-1,1)];
                        case num
                            % do nothing
                        otherwise
                        error(['the amount of nodes for each node should be ' num2str(num)])
                    end
                end
            end
        end
    else
        error(err)
    end
end
tf=true;
end

% @generateFunctionHelp

% @author Adam Cooman
% @institution ELEC,VUB
% @tagline adds tickler multisines to the specified nodes and runs a harmonic balance simulation

% @output1 contains the results of the simulation with the large multisine only.
% @outputType1 struct
% @output2 contains the results of the simulation with the ticklers where
% @output2 the large signal response has been substracted
% @outputType2 struct

% @description in a Substract simulation, Tickler sources are placed on the same
% @description frequency grid as the main multisine. The circuit response without
% @description ticklers is determined first. Then the circuit is simulated with the same
% @description multisine, but with added tickler sources. The response of the tickler
% @description sources is obtained by substracting the original response from the
% @description tickled response
% @version 01/06/2015 Version 1

