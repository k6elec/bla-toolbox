function HB2ENV=getEnvelopeHB(spec,varargin)
% FOM_getEnvelopeHB This function extracts the complex baseband signal from Harmonic Balance simulation data coming from ADS.
% 
%      HB2ENV = FOM_getEnvelopeHB(spec);
%      HB2ENV = FOM_getEnvelopeHB(spec,'ParamName',paramValue,...);
% 
% The envelope is extracted around a user-specified harmonic of the carrier frequency.
% This conversion is necessary to get an idea of the instantaneous figures of merit
% for a power amplifier.
% 
% Required inputs:
% - spec      check: @(x) checkSpec(x) && isstruct(x)
%      spec structure. (Output of the HB simulation)
% 
% Parameter/Value pairs:
% - 'fc'     default: []  check: @(x) isnumeric(x) && isscalar(x)
%      Carrier frequency of the excitation signal.  When left empty
%      the function will try to extract the fc from the data (quite
%      erroneous)
% - 'f0'     default: []  check: @(x) isnumeric(x) && isscalar(x)
%      Frequency resolution of the multisine.
% - 'fieldfilter'     default: []  check: @iscellstr
%      Cell of strings of the fieldnames of the spec struct that should
%      be converted,  when this cell array is empty all fields of spec
%      will be converted to the envelope domain.
% - 'kex'     default: [0  check: 1],@iscellstr
%      Frequency harmonics around which the envelope will be extracted,
%       this is a numeric array and can thus contain multiple frequencies
%      of interest.  (mostly DC and the carrier frequency) If left
%      empty this function will extract the  envelope around DC and
%      the carrier frequency (0 and 1)
% - 'zpadding'     default: 0  check: @(x) isnumeric(x) && isscalar(x)
%      Zeropadding of the complex BB signal. If you want an accurate
%      (but lengthy) envelope set this value high.
% 
% Outputs:
% - HB2ENV      Type: struct
%      Contains the complex baseband signal of the HB spectra given
%      in the input struct. Attempts to simulate the envelope structure
%      that would normally be expected from a legit envelope simulation
%      of ADS.
% 
% Note: To make the function work properly the HB settings should be chosen correctly.
% Order of the frequency resolution and the mixing order should be the same. No constraints
% are however given on the values of the orders as they still be chosen freely
% 
% see also: FOM_getEnvelopeMS
% 
% Piet Bronders, ELEC, VUB
% 
% Version info:
%  24-11-2014  Version 1.
%  02-04-2015  Major revisions to the inner workings of the function making it faster, more robust and all around a better function. Removed dependency on the ADSconvert2timedomain monstrosity.
%  03-04-2015  Removed a bug regarding the inconsistency between the length of the time array and envelope length. (1 sample delay fixed) Converted the function help to be compatible to the generateFunctionHelp function.
% 
% This documentation was generated with the generateFunctionHelp function at 07-Apr-2015

%% parse the input
p = inputParser();
p.FunctionName = 'getEnvelopeHB';
% spec structure. (Output of the HB simulation)
p.addRequired('spec',@(x) checkSpec(x) && isstruct(x));

% Carrier frequency of the excitation signal. 
% When left empty the function will try to extract the fc from the data (quite erroneous)
p.addParamValue('fc',[],@(x) isnumeric(x) && isscalar(x));

% Frequency resolution of the multisine.
p.addParamValue('f0',[],@(x) isnumeric(x) && isscalar(x));

% Cell of strings of the fieldnames of the spec struct that should be converted, 
% when this cell array is empty all fields of spec will be converted to the envelope domain.
p.addParamValue('fieldfilter',[],@iscellstr);

% Frequency harmonics around which the envelope will be extracted, 
% this is a numeric array and can thus contain multiple frequencies of interest.
% (mostly DC and the carrier frequency) If left empty this function will extract the
% envelope around DC and the carrier frequency (0 and 1)
p.addParamValue('kex',[0,1],@iscellstr);

%Zeropadding of the complex BB signal. If you want an accurate 
%(but lengthy) envelope set this value high.
p.addParamValue('zpadding',0,@(x) isnumeric(x) && isscalar(x));

p.StructExpand = true;
p.parse(spec,varargin{:});
args = p.Results();

%% Initialize additional settings

% Check if the elements of fieldfilter are present in spec
if ~isempty(args.fieldfilter)
    for ii=1:length(args.fieldfilter)
        if isfield(args.spec,args.fieldfilter{ii})
            error('One of the elements of fieldfilter is not available in spec');
        end
    end
else
    args.fieldfilter =args.spec;
    args.fieldfilter = rmfield(args.fieldfilter,'freq');
    args.fieldfilter=fieldnames(args.fieldfilter);
end

%Check if f0 is present, if not --> try to extract from the spectral data
if isempty(args.f0)
    f0 =round(min(abs(args.spec.freq-circshift(args.spec.freq,[0,1]))));
    disp(sprintf('Frequency resolution was extracted from the frequency array, round-off errors could be present.\n The frequency resolution was estimated to be %0.5e',f0));%#ok
else
    f0=args.f0;
end

%Check if zpadding is even, if not convert to the closest even number
if args.zpadding~=0 
    args.zpadding=round(args.zpadding);
    if rem(args.zpadding,2)~=0
        args.zpadding=args.zpadding+1;
    end
end

%Extract the RF signal Bandwidth (in bins)
dist =((abs(args.spec.freq-circshift(args.spec.freq,[0,1]))));
I=zeros(3,1);
jj=1;
for ii=1:length(dist)
    if round(dist(ii)/f0)~=1
        I(jj)=ii;
        jj=jj+1;
        if jj==4
            break
        end
    end
end
BW=I(2)-I(1)-1;

%Check if fc is present, if not -->try to extract from the spectral data
%Please give fc as this function can give terrible results in some cases
if isempty(args.fc)
    fc=round(args.spec.freq((I(3)+I(2)-1)/2));
    disp(sprintf('fc was derived from the freq array and estimated to be %0.5e',fc)); %#ok
else
    fc=args.fc;
end

%% Do some simple data prelocation

%Pre-allocate the envelope frequency array
HB2ENV.freq=args.kex.*fc;

%How much zeropadding?
Zpad=args.zpadding*2*BW;

%Pre-allocate the envelope time array
ts=1/(2*BW*f0);
HB2ENV.time=0:ts:(1/f0)-ts;

%% Convert wanted parts of frequency spectrum to a complex time domain signal

for ii=1:length(args.fieldfilter)
    %Pre-allocate the envelope data array
    HB2ENV.(args.fieldfilter{ii})=zeros(length(HB2ENV.freq),Zpad+2*BW);
    for jj=1:length(HB2ENV.freq)
        if HB2ENV.freq(jj)==0  %%it's DC! DC is special
            spec_chop=squeeze(spec.(args.fieldfilter{ii})(1,1,1:(BW+1)));
            if Zpad==0
                spec_chop=[spec_chop(1);spec_chop(2:end-1)/2;(2*real(spec_chop(end)));flip(conj(spec_chop(2:end-1)/2),1)];
                %There's a bin too much --> something has to be merged
                %The last bin is removed from both left and right spectra
                %(2 bins) and 1 new bin is added which is chosen equal to
                %2*real(spec(end))
                %
                %WHY? EXAMPLE:
                %Cosine sampled at nyquist:
                %Cos=5*cos(2*pi*1e6*(0:0.5e-6:(1e-6-0.5e-6)))
                %fft(Cos)=[0 10]
                %Sine sampled at nyquist:
                %Sin=5*sin(2*pi*1e6*(0:0.5e-6:(1e-6-0.5e-6)))
                %fft(Sin)=[0 0]
                %THUS: two samples are merged using 2*real{}
            else
                spec_chop=[spec_chop(1);spec_chop(2:end)/2;zeros(Zpad-1,1);flip(conj(spec_chop(2:end)/2),1)];
                %A sample of the zeropadding can be removed, thus we no
                %need to throw our valuable samples away!
            end
        else
            ex_I=find(round(spec.freq/f0)==round(HB2ENV.freq(jj)/f0)); %Index of wanted frequency

            spec_chop=squeeze(spec.(args.fieldfilter{ii})(1,1,(ex_I-(BW-args.kex(jj))):1:(ex_I+(BW-args.kex(jj)))));
            spec_chop=[spec_chop((BW-args.kex(jj))+1:end);zeros(Zpad+2*args.kex(jj)-1,1);(spec_chop(1:(BW-args.kex(jj))))];
            
            %We need extra samples! ( 2*args.kex(jj) )
            %WHY? Because Harmonic Balance does not compute consequent
            %harmonics with the same number of bins --> BW around each
            %carrier will start to change (2 bins removed per carrier)
            
            %ONLY if the mixing order and the order of f0 are the same
            %IS REQUIRED!
        end
            spec_chop=ifft(spec_chop)*length(spec_chop);

            HB2ENV.(args.fieldfilter{ii})(jj,:)=spec_chop;
    end
end

end

%% function to check the spectrum passed to the function
function res = checkSpec(s)

global Fields

if isstruct(s)
    % if s is a struct, it should contain a freq field and at least one
    % matrix field that has its final dimension equal to length(freq)
    if isfield(s,'freq')
        F = length(s.freq);
        s = rmfield(s,'freq');
        Fields = fieldnames(s);
        % remove non-numeric fields
        toremove=[];
        for ff=1:length(Fields)
            if ~isnumeric(s.(Fields{ff}))
                toremove = [toremove ff];
            end
        end
        if ~isempty(toremove)
            warning('non-numeric fields in spec detected. They will be ignored');
            Fields = Fields(1:length(Fields)~=toremove);
        end
        % check the size of the remaining fields in spec
        siz = zeros(length(Fields),3);
        for ff=1:length(Fields)
            siz(ff,:)=size(s.(Fields{ff}));
        end
        % the third element in the size vector should be F for all the fields
        temp = siz(:,3)~=F;
        if any(temp)
            warning('size(field,3)~=length(freq) for some of the fields. They will be ignored');
            Fields = Fields(~temp);
            siz = siz(~temp,:);
        end
        % look for weird sizes in the siz matrix.
        siz_t =siz(:,1)*max(max(siz(:,1:2)))+siz(:,2);
        M = mode(siz_t);
        if any(siz_t~=M)
            warning('some fields with a different size are detected, They will be ignored');
            Fields = Fields(siz_t==M);
            disp(Fields)
        end
    else
        error('spec.freq should be provided');
    end
else
    if ~isnumeric(s)
        % the checks for numeric spec will be performed outside of the
        % inputparser, because we need access to the freq parameter
        error('spec should be a struct or a matrix');
    end
end
% if you pass all the checks, you are happy!
res = true;
end

% @generateFunctionHelp
% @author Piet Bronders
% @institution ELEC, VUB

% @tagline This function extracts the complex baseband signal from Harmonic Balance simulation data coming from ADS.

% @description The envelope is extracted around a user-specified harmonic of the carrier frequency.
% @description This conversion is necessary to get an idea of the instantaneous figures of merit for a power amplifier.

% @extra Note: To make the function work properly the HB settings should be chosen correctly.
% @extra Order of the frequency resolution and the mixing order should be the same.
% @extra No constraints are however given on the values of the orders as they still be chosen freely

% @version 24-11-2014  Version 1.
% @version 02-04-2015  Major revisions to the inner workings of the function making it faster, more robust and all around a better function. Removed dependency on the ADSconvert2timedomain monstrosity.
% @version 03-04-2015  Removed a bug regarding the inconsistency between the length of the time array and envelope length. (1 sample delay fixed) Converted the function help to be compatible to the generateFunctionHelp function.

% @output1 Contains the complex baseband signal of the HB spectra given in
% @output1 the input struct. Attempts to simulate the envelope structure
% @output1 that would normally be expected from a legit envelope simulation of ADS.
% @outputType1 struct

% @seealso FOM_getEnvelopeMS

%% generateFunctionHelp: old help, backed up at 07-Apr-2015. leave this at the end of the function
% FOM_getEnvelopeHB This function extracts the complex baseband signal from Harmonic Balance simulation data coming from ADS.
% 
%      HB2ENV = FOM_getEnvelopeHB(spec);
%      HB2ENV = FOM_getEnvelopeHB(spec,'ParamName',paramValue,...);
% 
% The envelope is extracted around a user-specified harmonic of the carrier frequency.
% This conversion is necessary to get an idea of the instantaneous figures of merit
% for a power amplifier.
% 
% Required inputs:
% - spec      check: @(x) checkSpec(x) && isstruct(x)
%      spec structure. (Output of the HB simulation)
% 
% Parameter/Value pairs:
% - 'fc'     default: []  check: @(x) isnumeric(x) && isscalar(x)
%      Carrier frequency of the excitation signal.  When left empty
%      the function will try to extract the fc from the data (quite
%      erroneous)
% - 'f0'     default: []  check: @(x) isnumeric(x) && isscalar(x)
%      Frequency resolution of the multisine.
% - 'fieldfilter'     default: []  check: @iscellstr
%      Cell of strings of the fieldnames of the spec struct that should
%      be converted,  when this cell array is empty all fields of spec
%      will be converted to the envelope domain.
% - 'kex'     default: [0  check: 1],@iscellstr
%      Frequency harmonics around which the envelope will be extracted,
%       this is a numeric array and can thus contain multiple frequencies
%      of interest.  (mostly DC and the carrier frequency) If left
%      empty this function will extract the  envelope around DC and
%      the carrier frequency (0 and 1)
% - 'zpadding'     default: 0  check: @(x) isnumeric(x) && isscalar(x)
%      Zeropadding of the complex BB signal. If you want an accurate
%      (but lengthy) envelope set this value high.
% 
% Outputs:
% - HB2ENV      Type: struct
%      Contains the complex baseband signal of the HB spectra given
%      in the input struct. Attempts to simulate the envelope structure
%      that would normally be expected from a legit envelope simulation
%      of ADS.
% 
% Note: To make the function work properly the HB settings should be chosen correctly.
% Order of the frequency resolution and the mixing order should be the same. No constraints
% are however given on the values of the orders as they still be chosen freely
% 
% see also: FOM_getEnvelopeMS
% 
% Piet Bronders, ELEC, VUB
% 
% Version info:
%  24-11-2014  Version 1.
%  02-04-2015  Major revisions to the inner workings of the function making it faster, more robust and all around a better function. Removed dependency on the ADSconvert2timedomain monstrosity.
%  03-04-2015  Removed a bug regarding the inconsistency between the length of the time array and envelope length. (1 sample delay fixed) Converted the function help to be compatible to the generateFunctionHelp function.
% 
% This documentation was generated with the generateFunctionHelp function at 03-Apr-2015
