function [varEst,ax] = testVariance(FRM,bins)
% this function assumes an FRF is smooth and calculates the variance by
% estimating a local linear model through the data and looking at the error
%
%   [varEst,ax] = testVariance(FRF,bins)
%
% FRF is an NxMxF matrix that contains the FRF estimate
% bins is an odd number that indicates the amount of bins to work with
%
% Adam Cooman, ELEC VUB

if length(size(FRM))==2
    % a SISO system is passed
    if ~isvector(FRM)
        error('the input should be ')
    end
    temp = FRM;clear FRM
    FRM(1,1,:) = temp;
end

[N,M,F] = size(FRM);

if bins>F
    error('cannot average over the provided amount of bins');
end

R = ones(bins,2);

% do the stuff
ax=((bins+1)/2):(F-(bins+1)/2);
rng = (1:bins)-(bins+1)/2;
varEst = zeros(N,M,length(ax));
for ff=1:length(ax);
    for nn=1:N
        for mm=1:M
            % build the regressor matrix
            R(:,2) = ax(ff)+rng;
            y = abs(FRM(nn,mm,ax(ff)+rng));
            % perform a least-squares fit of a straight line on the data
            a = R\y(:);
            res = y(:) - R*a;
            % look at the sum of residuals to obtain an estimate of the variance
            varEst(nn,mm,ff)=rms(res)^2;
        end
    end
end


end