function [spec,MSdefs] = runZipperSimulation(Netlist,MSdef,varargin)
% runZipperSimulation adds tickler multisines to the specified nodes and runs a harmonic balance simulation
% 
%      spec = runZipperSimulation(Netlist,MSdef);
%      spec = runZipperSimulation(Netlist,MSdef,'ParamName',paramValue,...);
% 
% Tickler sources are placed at a small frequency offset left and right from the tones
% of the main multisine.
% 
% Required inputs:
% - Netlist      check: @(x) ischar(x)||iscellstr(x)
%      ADS netlist that has to be simulated. The netlist should not
%      contain any  multisine source or simulation statement, these
%      are added by the function
% - MSdef      check: @isstruct
%      definition of the large multisine. Use MScreate to build this
%      multisine
% 
% Parameter/Value pairs:
% - 'TicklerNodes'     default: {}  check: @checknodelist
%      list of nodes where the ticklers should be connected to.
% - 'TicklerImpedances'     default: Inf  check: @isvector
%      output impedances of the tickler multisines. The default is
%      a current source
% - 'TicklerAmplitudes'     default: 1e-9  check: @isvector
%      amplitude of the tickler multisines
% - 'outOfBandFactor'     default: 1  check: @isscalar
%      ratio between the bandwidth of the tickler multisines and the
%      large  multisine. Use this to determine the out-of-band BLA
% - 'TicklerMSdefs'     default: []  check: @isstruct
%      for fancy stuff, you can provide a vector of multisine structs
%      to this  function as well. These multisines need to be on the
%      same frequency grid  as the large multisine. The function will
%      give the multisines a small  frequency offset to zipper the
%      multisines.
% - 'TicklerDistance'     default: 1  check: @isscalar
%      frequency difference between the tickler and the main multisine
% - 'oversample'     default: 10  check: @isnatural
%      estimated orded of non-linearity of the underlying system
% - 'numberOfRealisations'     default: 7  check: @isnatural
%      number of multisine realisations
% - 'measureTickler'     default: false  check: @islogical
%      if this is set to true, a current probe is added to the tickler
%      sources  to allow measuring of the reference current. The currents
%      are called  I_ticklerX in the resulting struct.
% 
% Outputs:
% - spec      Type: struct
%      contains the results of the simulation.
% 
% Adam Cooman, ELEC,VUB
% 
% Version info:
%  01/06/2015 Version 1
% 
% This documentation was generated with the generateFunctionHelp function at 01-Jun-2015


p = inputParser();
% ADS netlist that has to be simulated. The netlist should not contain any
% multisine source or simulation statement, these are added by the function
p.addRequired('Netlist',@(x) ischar(x)||iscellstr(x));
% definition of the large multisine. Use MScreate to build this multisine
p.addRequired('MSdef',@isstruct);
% list of nodes where the ticklers should be connected to.
p.addParameter('TicklerNodes',{},@checknodelist);
% output impedances of the tickler multisines. The default is a current source
p.addParameter('TicklerImpedances',Inf,@isvector);
% ampltude of the tickler multisines
p.addParameter('TicklerAmplitudes',1e-9,@isvector);
% ratio between the bandwidth of the tickler multisines and the large
% multisine. Use this to determine the out-of-band BLA
p.addParameter('outOfBandFactor',1,@isscalar);
% estimated order of non-linearity of the circuit.
p.addParameter('oversample',10,@isnatural);
% for fancy stuff, you can provide a vector of multisine structs to this
% function as well. These multisines need to be on the same frequency grid
% as the large multisine. The function will give the multisines a small
% frequency offset to zipper the multisines.
p.addParameter('TicklerMSdefs',[],@isstruct);
% frequency difference between the tickler and the main multisine. 
% The default value is MSdef.f0/100
% If this is set to MSdef.f0/2 then special simulation settings are used. It should
% never exceed MSdef.f0/2
p.addParameter('TicklerDistance',Inf,@isscalar);
% if this is set to true, a current probe is added to the tickler sources
% to allow measuring of the reference current. The currents are called
% I_ticklerX in the resulting struct.
p.addParameter('measureTickler',false,@islogical);
p.KeepUnmatched = true;
p.parse(Netlist,MSdef,varargin{:});
args = p.Results;
SimInfo = p.Unmatched();
clear Netlist MSdef varargin p

if isinf(args.TicklerDistance)
    args.TicklerDistance = args.MSdef.f0/100;
end

% put the nodelist in a correct cell array
[~,args.TicklerNodes] = checknodelist(args.TicklerNodes);

% If required, add current probes to the netlist to measure the reference current
if args.measureTickler
    % open the netlist file
    if ~iscell(args.Netlist)
        args.Netlist = readTextFile(args.Netlist);
    end
    % add the current probe to the tickler node and replace the name of the
    % node with a new 
    for ii=1:length(args.TicklerNodes)
        % Short:I_Probe1  Tickle1 Node Mode=0 SaveCurrent=yes 
        args.Netlist{end+1} = ADSaddShort(-1,sprintf('I_tickle%d',ii),...
            'sink',sprintf('Tickle%d',ii),'source',args.TicklerNodes{ii}{1},...
            'Mode',0,'SaveCurrent','yes');
        args.TicklerNodes{ii}{1} = sprintf('Tickle%d',ii);
    end
end

% if the user doesn't provide the multisine structs for the tickers
% himself, we generate them now using the other input parameters
args.TicklerMSdefs = generateTicklerMSdefs(args);
% T is the amount of ticklers
T = length(args.TicklerMSdefs);

if (ceil(T/2)*args.TicklerDistance)>(args.MSdef.f0/2)
    error('Cannot fit all the tickler tones in the multisine properly. reduce TicklerDistance');
end

% calculate the simulation settings. 
if args.TicklerDistance==args.MSdef.f0/2
    % There is a special case when the ticklerDistance is half of the multisine base frequency.
    MSdef_temp = args.MSdef;
    MSdef_temp.f0 = MSdef_temp.f0/2;
    MSdef_temp.grid = MSdef_temp.grid*2;
    HBsettings = calculateHBSettings(MSdef_temp,args.oversample);
else
    % calculate the HB settings for normal multisine first
    HBsettings = calculateHBSettings(args.MSdef,args.oversample);
    % and add the nececary bonus bins for the ticklers
    HBsettings.Freq(end+1) = args.TicklerDistance;
    HBsettings.Order(end+1)= ceil(T/2);
end

% give each of the tickler multisines its frequency offset
rng = (ceil(-T/2) : ceil(T/2));
fdiff = args.TicklerDistance*rng(rng~=0);
for ii=1:length(args.TicklerMSdefs)
    args.TicklerMSdefs(ii).freq = args.TicklerMSdefs(ii).freq+fdiff(ii);
end

% combine all the MSdef structs
MSdefs = [args.MSdef args.TicklerMSdefs];

% run the simulation with all the multisines
spec = ADSsimulateMS(args.Netlist,MSdefs,'Simulator','HB','Simsettings',HBsettings,SimInfo);



end

%% function to generate the different MSdefs
function MSdefs = generateTicklerMSdefs(args)
% generates the MSdef structs for the different ticklers multisines

% check whether the original multisine is a lowpass or a bandpass multisine
if min(args.MSdef.grid)>10;
    mode = 'bandpass';
else
    mode = 'lowpass';
end

if isempty(args.TicklerMSdefs)
    % T is the amount of tickers added
    T = length(args.TicklerNodes);
    % check the impedance vector
    if length(args.TicklerImpedances)==1;
        args.TicklerImpedances = args.TicklerImpedances*ones(T,1);
    else
        if length(args.TicklerImpedances)~=T;
            error('The length of the vector of tickler impedances can be either one or equal the atickler nodes');
        end
    end
    % check the amplitude vectors
    if length(args.TicklerAmplitudes)==1;
        args.TicklerAmplitudes = args.TicklerAmplitudes*ones(T,1);
    else
        if length(args.TicklerAmplitudes)~=T;
            error('The length of the vector of tickler impedances can be either one or equal the atickler nodes');
        end
    end
    % now build the multisines for the ticklers
    for ii=1:T
        switch mode
            case 'lowpass'
                MSdefs(ii) = MScreate(args.MSdef.f0,args.outOfBandFactor*args.MSdef.fmax,...
                    'ampl',args.TicklerAmplitudes(ii),...
                    'Rout',args.TicklerImpedances(ii),...
                    'grid','full');
            case 'bandpass'
                % determine the bandwidth of the multisine and the center frequency
                fc = (max(args.MSdef.grid)+min(args.MSdef.grid))*args.MSdef.f0/2;
                bw = (max(args.MSdef.grid)-min(args.MSdef.grid))*args.MSdef.f0;
                to = bw*args.outOfBandFactor/args.MSdef.f0+1;
                % now generate the tickler MSdef
                MSdefs(ii) = MScreate(args.MSdef.f0,fc,'mode','bandpass','numTones',to,...
                    'ampl',args.TicklerAmplitudes(ii),...
                    'Rout',args.TicklerImpedances(ii),...
                    'grid','full');
        end
    end
    % and add the nodes to the tickler multisines
    for ii=1:T
        MSdefs(ii).MSnode = args.TicklerNodes{ii};
    end
else
    % check whether the frequency resolution of the tickler multisines is
    % the same as the one of the main multisine
    if ~all([args.TicklerMSdefs.f0]==args.MSdef.f0)
        error('The base frequency of all multisines should be the same')
    end
    % just use the MSdefs provided in the argument
    MSdefs = args.TicklerMSdefs;
end

end

%% function to check the provided node list
function [tf,n] = checknodelist(n,num)
% this function checks a nodelist
if ~exist('num','var')
    num=2;
end
err = 'the node list should be a string, a cell array of strings or a cell array of a cell array of strings';
if ~iscell(n)
    if ischar(n)
        n = {{n,'0'}};
    else
        error(err)
    end
else
    if isvector(n)
        for ii=1:length(n)
            if ischar(n{ii})
                n{ii} = {n{ii},'0'};
            else
                if ~iscellstr(n{ii})
                    error(err)
                else
                    switch length(n{ii})
                        case 1
                            % add '0' for all the other nodes
                            n{ii} = [n(ii) repmat({'0'},num-1,1)];
                        case num
                            % do nothing
                        otherwise
                        error(['the amount of nodes for each node should be ' num2str(num)])
                    end
                end
            end
        end
    else
        error(err)
    end
end
tf=true;
end

% @generateFunctionHelp

% @author Adam Cooman
% @institution ELEC,VUB
% @tagline adds tickler multisines to the specified nodes and runs a harmonic balance simulation

% @output1 contains the results of the simulation.
% @outputType1 struct

% @output2 are the MSdef structs generated by the function to perform the simulation
% @outputType2 vector of structs

% @description Tickler sources are placed at a small frequency offset left and right from
% @description the tones of the main multisine.

% @version 01/06/2015 Version 1

%% generateFunctionHelp: old help, backed up at 01-Jun-2015. leave this at the end of the function
% runZipperSimulation adds tickler multisines to the specified nodes and runs a harmonic balance simulation
% 
%      spec = runZipperSimulation(Netlist,MSdef);
%      spec = runZipperSimulation(Netlist,MSdef,'ParamName',paramValue,...);
% 
% Thickler sources are placed at a small frequency offset left and right from the
% tones of the main multisine.
% 
% Required inputs:
% - Netlist      check: @(x) ischar(x)||iscellstr(x)
%      ADS netlist that has to be simulated. The netlist should not
%      contain any  multisine source or simulation statement, these
%      are added by the function
% - MSdef      check: @isstruct
%      definition of the large multisine. Use MScreate to build this
%      multisine
% 
% Parameter/Value pairs:
% - 'TicklerNodes'     default: {}  check: @checknodelist
%      list of nodes where the ticklers should be connected to.
% - 'TicklerImpedances'     default: Inf  check: @isvector
%      output impedances of the tickler multisines. The default is
%      a current source
% - 'TicklerAmplitudes'     default: 1e-9  check: @isvector
%      ampltude of the tickler multisines
% - 'outOfBandFactor'     default: 1  check: @isscalar
%      ratio between the bandwidth of the tickler multisines and the
%      large  multisine. Use this to determine the out-of-band BLA
% - 'TicklerMSdefs'     default: []  check: @isstruct
%      for fancy stuff, you can provide a vector of multisine structs
%      to this  function as well. These multisines need to be on the
%      same frequency grid  as the large multisine. The function will
%      give the multisines a small  frequency offset to zipper the
%      multisines.
% - 'TicklerDistance'     default: 1  check: @isscalar
%      frequency difference between the tickler and the main multisine
% - 'oversample'     default: 10  check: @isnatural
%      estimated orded of non-linearity of the underlying system
% - 'numberOfRealisations'     default: 7  check: @isnatural
%      number of multisine realisations
% - 'measureTickler'     default: false  check: @islogical
%      if this is set to true, a current probe is added to the tickler
%      sources  to allow measuring of the reference current. The currents
%      are called  I_ticklerX in the resulting struct.
% 
% Outputs:
% - spec      Type: struct
%      contains the results of the simulation.
% 
% Adam Cooman, ELEC,VUB
% 
% Version info:
%  01/06/2015 Version 1
% 
% This documentation was generated with the generateFunctionHelp function at 01-Jun-2015
