function Ynew = interpolate_MIMO(X,Y,Xnew,method,extrapolation)
% interpolate_MIMO interpolates MxNxF matrices over the third dimension
%
%   Ynew = interpolate_MIMO(X,Y,Xnew)
%   Ynew = interpolate_MIMO(X,Y,Xnew,method,extrapolation)
%
% X is an 1xF matrix indicating the frequency axis of the original MIMO FRM
% Y is an MxNxF frequency response matrix that should be interpolated
% Xnew is an 1xF2 vector indicating the new frequency points.
%
% you can also pass the method of interpolation and extrapolation as
% optional arguments. See the documentation of interp1 for those.
% default is linear interpolation and linear extrapolation
%
% ADAM Cooman, ELEC, VUB

switch nargin
    case 3
        method = 'linear';
        extrapolation = 'extrap';
    case 4
        extrapolation = 'extrap';
    case 5
        % nothing to do ...
    otherwise
        error('the function requires at least 3 inputs and maximum 5 inputs')
end

siz = size(Y);
if length(siz)~=3
    error('the FRM should be 3 dimensional, MxNxF');
end
M = siz(1);N = siz(2);F =siz(3);
X = X(:);Xnew = Xnew(:);
if length(X)~=F
    error('The frequency axis of the original FRM has different length than the FRM');
end


Ynew = zeros(M,N,length(Xnew));
for mm=1:M
    for nn=1:N
        Ynew(mm,nn,:) = interp1(X,squeeze(Y(mm,nn,:)),Xnew,method,extrapolation);
    end
end

end