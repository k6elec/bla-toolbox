function res=LSSSgenerateAmpList(n)
% the excitation matrix is a diagonal matrix
mat = eye(n);
% start the list statement
res = 'list(';
% add the ones and zeros of the excitation matrix
for ii=1:n
    for jj=1:n
        res = [res num2str(mat(ii,jj)) ','];%#ok
    end
end
% there's a comma too much, throw it away
res = res(1:end-1);
% add the closing bracket
res = [res ')'];
end