function [HTF,c] = LSSSestimateWereley(U,Y)



% U should be a nu x ne cell array of wereley matrices, from reference to input(s), for the different experiments
% Y should be a ny x ne cell array of wereley matrices, from reference to output(s), for the different experiments

% TODO: maybe the for loop over the frequencies can be avoided by
%   calculating only one big wereley matrix and extracting the HTFs from that
% one. The Wereley matrix for Y and U is not square though

% check whether the inputs are cell arrays, if not, put them in one
if ~iscell(U); U = {U};end
if ~iscell(Y); Y = {Y};end

[b,a] = size(U{1,1});
H = b-a;
F = a-H;
% disp(['F=' num2str(F) ' H=' num2str(H)]);

% check some of the inputs
[nu,ne] = size(U);
if ne<nu
    error('the amount of experiments should be equal to the amount of inputs');
end
[ny,t] = size(Y);
if t~=ne
    error('the amount of experiments should be equal for both in- and output')
end

% preallocate the HTF matrix
HTF = cell(ny,nu); [HTF{:}] = deal(zeros(2*H+1,F-H));

range = -H:H;
% S.type='()';
U_C = cell(nu,ne);[U_C{:,:}] = deal(zeros(2*H+1));
Y_C = cell(ny,ne);[Y_C{:,:}] = deal(zeros(2*H+1));
for ff=1:F-H
    % get the correct pieces out of the wereley matrices and glue them
    % together into one big matrix for Y and U
%     S.subs = {H+ff+range,H+ff+range};
%     Y_T = full(cell2mat(cellfun(@(x) subsref(x,S),Y,'UniformOutput',0)));
%     U_T = full(cell2mat(cellfun(@(x) subsref(x,S),U,'UniformOutput',0)));
    for ee=1:ne
        for uu=1:nu
            U_C{uu,ee} = U{uu,ee}(H+ff+range,H+ff+range);
        end
        for yy=1:ny
            Y_C{yy,ee} = Y{yy,ee}(H+ff+range,H+ff+range);
        end
    end
    U_T = full(cell2mat(U_C));
    Y_T = full(cell2mat(Y_C));
    c(ff)=cond(U_T);
    % perform the inversion
    G_T = Y_T/U_T;
    
    
    % cut the matrix back into pieces
    G_C = mat2cell(G_T,2*H+1*ones(ny,1),2*H+1*ones(nu,1));
    % get the correct HTFs out of the result struct
    for uu=1:nu
        for yy=1:ny
            HTF{yy,uu}(:,ff) = G_C{yy,uu}(:,H+1);
        end
    end
    
end



end