function WER = LSSSextractWereley(HTFs,H)
% this function builds the wereley matrix from reference to the different nodes in the HTF struct



% do some more size stuff
% F is the amount of small signal frequencies
% E is the amount of experiments 
% H is the amount of HTFs we will estimate
% M is the amount of realisations of the time-variation
% O is the original amount of HTFs in the input

fields = fieldnames(HTFs);

M = length(HTFs);
[E,O,F] = size(HTFs(1).(fields{1}));

% preallocate the wereley matrix. Each field in the struct array should
% contain a cell array of length E filled with sparse Wereley matrices
% the following 4 lines of incomprehenisble code to just that
preall = cell(M,1);
[preall{:}] = deal(repmat({sparse(F+H+H , F+H )},1,E));% TODO: calculate how many non-zero elements there are in the wereley matrix;
preall = [fields.';repmat({preall},1,length(fields))];
WER = struct(preall{:});

% put the elements from the HTF matrices in the wereley matrices
for ii=1:length(fields)
    for mm=1:M
        for ee=1:E
            HTFs_to_use = squeeze(HTFs.(fields{ii})(ee,((O-1)/2+1-H:(O-1)/2+1+H),:));
            WER(mm).(fields{ii}){ee}(:,H+1:end) = spdiags( HTFs_to_use.' , 0:-1:-(2*H) , F+2*H , F );
            WER(mm).(fields{ii}){ee}(1:2*H+1 , 1:H ) = conj(rot90( WER(mm).(fields{ii}){ee}( 1:2*H+1 , H+2:H+1+H ) ,2));
        end
    end
end


end