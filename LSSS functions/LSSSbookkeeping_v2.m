function res = LSSSbookkeeping_v2(struct)
% LSSSbookkeeping calculates the HTFs from reference to each node in the result of a LSSS simulation
%
%   res = LSSSbookkeeping(struct)
% 
% struct is the result of the LSSS simulation 
% fsys is the speed of time-variation of the circuit
% numHTFs is the number of harmonic transfer functions that are exctracted
%   from the struct with simulation results
%
% The function returns a struct that contains [ 2*numHTFs+1 x length(ssfreq) ] 
% matrices for each field in the original struct. 
% It also returns the ssfreq vector that contains the frequencies of the
% experiments performed in ADS
%
% Adam Cooman, ELEC VUB
% 13/10/2014    version 1
%
% TODO: make this function superfast. The indexing is the same for all the
% fields in the struct, so we can calculate the indexing vectors once using
% sub2ind and ind2sub. And then process all the fields in the struct in a
% final, fast, for-loop

% TODO: test the input struct. 
% It should have at least the following fields: fsys, ssfreq, freq and one extra

%%
% get fsys from the results struct
fsys = struct(1).fsys;
% get the small signal frequencies out of the result struct
ssfreq = struct(1).ssfreq;
% extract the frequency matrix that has been generated in the experiments
freqmat = squeeze(struct(1).freq(1,:,:));

freqmata = freqmat;
freqmata(:,1:2:end-1)=-freqmat(:,1:2:end-1);

% find the HTFs that can be extacted from the data
HTFvec = ((freqmata(1,:)-ssfreq(1))/fsys).';

disp([(freqmat(end,:)/1e9).' (freqmata(end,:)/1e9).' HTFvec])


%%
keyboard

% 
% try
%     siz = size(struct.freq);
% catch
%     try 
%         struct = struct.sim2_LSSS;
%         siz = size(struct.freq);
%     catch
%         error('bad struct')
%     end
% end
% 
% switch length(siz)
%     case 2
%         F_exp = siz(1);
%         F = siz(2);
%         N_exp = 1;
%     case 3
%         F_exp = siz(1);
%         N_exp = siz(2);
%         F = siz(3);
% end
%     
% ssfreq = struct.sweepvar_ssfreq;
% switch length(siz)
%     case 2
%         freq = struct.freq;
%     case 3
%         freq = squeeze(struct.freq(:,1,:));
% end
% 
% % remove the sweepvar fields and the freq field
% fields = fieldnames(struct);
% struct = rmfield(struct,['freq';fields(cellfun(@(x) ~isempty(x),regexp(fields,'^sweepvar_.*')))]);
% fields = fieldnames(struct);
% 
% % preallocate the result fields
% for nn=1:length(fields)
% 	res.(fields{nn}) = zeros(N_exp,2*numHTFs+1,F_exp);
% end
% 
% for ee=1:N_exp
%     for ff=1:F_exp
%         % calculate the frequencies at which we can find results
%         respfreqs = ssfreq(ff)+fsys*(-numHTFs:numHTFs).';
%         % now find the bins for each of these frequencies (in absolute value)
%         for hh = 1:length(respfreqs)
%             [diff,bin] = min(abs( freq(ff,:)-abs(respfreqs(hh))) );
%             if diff>fsys/1e6
%                 warning(['deviation of ' eng(diff) 'Hz from expected frequency occurred']);
%             end
%             for nn = 1:length(fields)
%                 % get the correct value out of the matrix
%                 switch length(siz)
%                     case 2
%                         val = struct.(fields{nn})(ff,bin);
%                     case 3
%                         val = struct.(fields{nn})(ff,ee,bin);
%                 end
%                 % if the frequency is negative, take the complex conjugate
%                 if respfreqs(hh)<0
%                     res.(fields{nn})(ee,hh,ff) = conj(val);
%                 else
%                     res.(fields{nn})(ee,hh,ff) = val;
%                 end
%             end
%         end
%     end
% end
% 
% % squeeze out the extra dimension
% if length(siz)==2
%     for nn=1:length(fields)
%         res.(fields{nn}) = squeeze(res.(fields{nn}));
%     end
% end


end