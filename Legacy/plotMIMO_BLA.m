%Data of last modification: 14/02/2014

%This function simply plots a MIMO system. Two different arrays of the same
%length can be given to this function. (In most cases these would be the std 
%and mean of a certain system parameter)

%INPUT
%----------------

            %- nu: Number of input ports of the system
            %[1 X 1]

            %- ny: Number of output ports of the system
            %[1 X 1]

            %- freq: Frequency points (X-axis)
            %[1 X length(freq)]

            %- myTitle: String that contains the wanted name of the figure
            %[1 X 1]

            %- figNumbPrev: Number of the figure
            %[1 X 1]

            %- G1: 1st Function that requires plotting (Y-axis)
            %[1 X length(freq)]

            %- G2: 2nd Function that requires plotting (Y-axis)
            %[1 X length(freq)]

%OUTPUT
%----------------

            %- figNumbPrev: Incrementation of the number of the figure
            %[1 X 1]
            
%REMARK: THIS FUNCTION IS TOO LIMITED IN WHAT IT DOES AND SHOULD BE
%PROPERLY EXTENDED!

function [figNumbNow]=plotMIMO_BLA(nu,ny,freq,myTitle,figNumbPrev,G1,G2)

    f=figure(figNumbPrev);
    set(f,'Name',myTitle);
    
    kk=1;
    for ii=1:nu
        for jj=1:ny
            subplot(nu,ny,kk);
            plot(freq, db(squeeze(G1(ii,jj,:))), freq, db(squeeze(G2(ii,jj,:))));
            xlabel('Frequency (Hz)')
            ylabel('Amplitude (dB)')
            grid on
            title(['S_{[' num2str(ii) ',' num2str(jj) ']}'])
            kk=kk+1;
        end %jj
    end %ii
    
    figNumbNow=figNumbPrev+1; %Increase the figure number for the next call
    
end

