function [G,spec_update,settings_update]=calcMIMO_BHA_v2(spec,settings,varargin)
% calcMIMO_BHA_v2 calculates the BLA, BMA and consequent transfer functions (user-specified) given a reference spectra, input spectra and output spectra.
% 
%      [G,spec_update,settings_update] = calcMIMO_BHA_v2(spec,settings);
%      [G,spec_update,settings_update] = calcMIMO_BHA_v2(spec,settings,'ParamName',paramValue,...);
% 
% Several valid possibilities for the 'U'-cell array are given: 
% (1) 'in1', {{'in1'}} or {'in1'} (1 SISO BLA);
% (2) {'in1','in2'} (1 MISO BLA);
% (3) {'in1',{'in2','in3'}} (1 MISO BLA/BMA);
% (4) U{1}={{'in1'}}, U{2}={{'in2'}} (2 SISO BLA); 
% combinations of the previous examples are possible...
% 
% Required inputs:
% - spec      check: @(x) checkSpec(x) && isstruct(x)
%      spec structure containing the relevant data fields. (Output
%      of the HB simulation)
% - settings      check: @(x) checkSettings(x) && isstruct(x)
%      Struct containing several settings for the BHA extracton with
%      not all of them being  necessary. A first optional field is
%      the 'signLvl'-field. This field will  only be used when the
%      excitedBins have to be extracted from the reference  signal.
%      In that case the 'signLvl'-field will be used as the threshold
%      to  seperate excited and unexcited bins. The default value for
%      the field is -10.  The second field is 'zpad', this variable
%      indicates if the spectra used  in the convolution will be zero-padded
%      or not. A non zero-padded  convolution will only be a good approximation
%      if there's enough empty  space (which is still present in the
%      spectra, but only contains noise)  between each of the harmonic
%      blocks of the spectra. A zero-padded  convolution is more correct,
%      but infinitely slower. The 'zpad'-field is  set to 0 by default.
%      Lastly we have the 'excitedBins','relevantBins' and  'importantBins'-fields.
%      The user has multiple choices for these fields.  (1) The 'importantBins'-field
%      is given. (2) None of the fields are given,  but a reference
%      signal is available. (3) Both 'relevantBins' and  'excitedBins'
%      are given, 'importantBins' can be left empty in that case. 
%      (4) Only 'relevantBins' or 'excitedBins' is given.
% 
% Parameter/Value pairs:
% - 'U'     default: {'in'}  check: @(x) checkUY(x) && isvector(x) && (iscell(x) || ischar(x))
%      Cell array of cell array of strings containing the names of
%      the different input  spectra. The first cell dimension indicates
%      the number of BHA which should be  extracted, while the second
%      dimension indicates the number of input ports.  The strings
%      should be valid fields of the spec struct, but there  are exceptions.
%      A first exception is regarding the '_conj'-extension.  When
%      this extensaion is found to be present AND the 'node_conj'-field
%      is not part of  the spec struct, the function will attempt to
%      take the conjugate of the  'node'-field (which SHOULD be present
%      in spec). Secondly the "string" can be swapped  by a cell array
%      of strings instead. (A 3rd dimension) The function will  than
%      take the convolution of all of the respective fields of the
%      spec  struct. Afterwards this cell array will then be replaced
%      by a  concatenated string (Concatenation of all the strings
%      in this cell  array). Other (simplified) cell arrays can also
%      be processed (ex. when only  1 BHA needs to be extracted, the
%      first dimension of cell arrays can be removed).  Several examples
%      can be found in the function description.
% - 'Y'     default: {'out'}  check: @(x) checkUY(x) && isvector(x) && (iscell(x) || ischar(x))
%      Cell array of strings containing the names of the different
%      output  spectra. See the flavor text of 'U' for more information.
% - 'R'     default: []  check: @(x) checkUY(x) && isvector(x) && (iscell(x) || ischar(x))
%      Cell array of strings containing the names of the different
%      reference spectra,  The reference is however not strictly necessary.
%      See flavor text of 'U' for more information.
% 
% Outputs:
% - G      Type: cell array of structs
%      Cell array of struct containing the calculated BHA, its variance
%      and much more...
% - spec_update      Type: struct
%      the updated spec struct for user convenience.
% - settings_update      Type: struct
%      the updated settings struct for user convenience.
% 
% NOTE: The functionality of settings.zpad has NOT been implemented yet! 
% 
% Piet Bronders, Adam Cooman, ELEC, VUB
% 
% Version info:
%  16-04-2015  Version 1.
%  23-04-2015  Fixed multiple errors in regards to the extraction of negative BMAs.
%  20-05-2015  Gigantic overhaul of the entire function. The function can now internally make the convolution of input spectra.
% 
% This documentation was generated with the generateFunctionHelp function at 20-May-2015

%% parse the input

p = inputParser();
p.FunctionName = 'calcMIMO_BHA';

% spec structure containing the relevant data fields. (Output of the HB simulation)
p.addRequired('spec',@(x) checkSpec(x) && isstruct(x));

% Struct containing several settings for the BHA extracton with not all of them being
% necessary. A first optional field is the 'signLvl'-field. This field will
% only be used when the excitedBins have to be extracted from the reference
% signal. In that case the 'signLvl'-field will be used as the threshold to
% seperate excited and unexcited bins. The default value for the field is -10.
% The second field is 'zpad', this variable indicates if the spectra used
% in the convolution will be zero-padded or not. A non zero-padded
% convolution will only be a good approximation if there's enough empty
% space (which is still present in the spectra, but only contains noise)
% between each of the harmonic blocks of the spectra. A zero-padded
% convolution is more correct, but infinitely slower. The 'zpad'-field is 
% set to 0 by default. Lastly we have the 'excitedBins','relevantBins' and 
% 'importantBins'-fields. The user has multiple choices for these fields. 
% (1) The 'importantBins'-field is given. (2) None of the fields are given, 
% but a reference signal is available. (3) Both 'relevantBins' and
% 'excitedBins' are given, 'importantBins' can be left empty in that case.
% (4) Only 'relevantBins' or 'excitedBins' is given.
p.addRequired('settings',@(x) checkSettings(x) && isstruct(x));

% Cell array of cell array of strings containing the names of the different input
% spectra. The first cell dimension indicates the number of BHA which should be 
% extracted, while the second dimension indicates the number of input ports.
% The strings should be valid fields of the spec struct, but there
% are exceptions. A first exception is regarding the '_conj'-extension.
% When this extensaion is found to be present AND the 'node_conj'-field is not part of
% the spec struct, the function will attempt to take the conjugate of the
% 'node'-field (which SHOULD be present in spec). Secondly the "string" can be swapped 
% by a cell array of strings instead. (A 3rd dimension) The function will
% than take the convolution of all of the respective fields of the spec
% struct. Afterwards this cell array will then be replaced by a
% concatenated string (Concatenation of all the strings in this cell
% array). Other (simplified) cell arrays can also be processed (ex. when only 
% 1 BHA needs to be extracted, the first dimension of cell arrays can be removed). 
% Several examples can be found in the function description.
p.addParameter('U',{'in'},@(x) checkUY(x) && isvector(x) && (iscell(x) || ischar(x)));

% Cell array of strings containing the names of the different output
% spectra. See the flavor text of 'U' for more information.
p.addParameter('Y',{'out'},@(x) checkUY(x) && isvector(x) && (iscell(x) || ischar(x)));

% Cell array of strings containing the names of the different reference spectra, 
% The reference is however not strictly necessary. See flavor text of 'U' for more information.
p.addParameter('R',[],@(x) checkUY(x) && isvector(x) && (iscell(x) || ischar(x)));

p.StructExpand = true;
p.parse(spec,settings,varargin{:});
args = p.Results();

%% Do additional checks on the supplied input data and add missing fields and data

portCheck={'U','Y','R'};
uniquePorts={};
kk=1;

%If sgnLvl is not given, put it to -10
if ~isfield(args.settings,'signLvl') 
    args.settings.signLvl=-10;
end

%If Zpad is not given, put it to zero. 
%If it is given, put it to 1
if ~isfield(args.settings,'zpad')
    args.settings.zpad=0;
else
    args.settings.zpad=1;
end

% Check if U has the following form
% U={{'in','out'}}
% If not we add another layer (or two) to the cell to make the data consistent.
for ii=1:length(portCheck)
    if ii~=3 || ~isempty(args.R)
        if ischar(args.(portCheck{ii}))
            %Give the correct format to the node
            args.(portCheck{ii})={{args.(portCheck{ii})}};
            %Add the port name to uniquePorts cell array
            uniquePorts{kk}= args.(portCheck{ii}){1}{1};
            kk=kk+1;
        else
            %If it ain't a cell array, we need to make it one!
            if ~iscell(args.(portCheck{ii}){1})
                    args.(portCheck{ii})={args.(portCheck{ii})};
            end
            for jj=1:length(args.(portCheck{ii}))
                %Add the port name to uniquePorts cell array
                for ll=1:length(args.(portCheck{ii}){jj})
                    if iscell(args.(portCheck{ii}){jj}{ll})
                        for mm=1:length(args.(portCheck{ii}){jj}{ll})
                            uniquePorts{kk}=args.(portCheck{ii}){jj}{ll}{mm};
                            kk=kk+1;
                        end
                    else
                        uniquePorts{kk}=args.(portCheck{ii}){jj}{ll};
                        kk=kk+1;
                    end
                end
            end
        end
    end
end
uniquePorts=unique(uniquePorts);

% Check if U,Y and R have the same length
if length(args.U)==length(args.Y)
    if ~isempty(args.R)
        if length(args.U)~=length(args.R)
            error('Length of U and R should be the same if R is given.');
        end
    end
else
    error('Length of the cell arrays U and Y should be the same.');
end

% Check if the bins have the correct format
% Either importantBins is given --> Needs to be checked
% Or relevantBins/excitedBins is given --> Needs to be converted
if ~isfield(args.settings,'importantBins')
    %Check if excitedBins exists, is yes we assume that:
    %relevantBins == excitedBins
    if ~isfield(args.settings,'excitedBins')
        %If excitedBins is empty, we attempt to extract it from the
        %reference signal.
        if  isempty(args.R)
            if ~isfield(args.settings,'relevantBins')
                %Where the hell do you want me to extract the BLA, you retarded caveman?
                error('Please supply at least one of the following: excitedBins, relevantBins, importantBins or a reference signal.')
            else
                %No reference signal was found... just assume that all
                %relevant bins are excited.
                args.settings.excitedBins=args.settings.relevantBins;
            end
        else
            %Extract the excitedBins from the reference
            %The first realisation of the first reference will be used. The multisine should use the same grid anywhere anyway.
            %NOTE: Possible extension for zippered MS?
            args.settings.excitedBins = find(db(args.spec.(args.R{1}{1})(1,1,:)>max(args.spec.(args.R{1}{1})(1,1,:))+args.settings.sgnLvl));
            args.settings.excitedBins = args.settings.excitedBins(args.settings.excitedBins~=1);% remove DC if it is present
        end
    end
    if ~isfield(args.settings,'relevantBins')
        args.settings.relevantBins=args.settings.excitedBins;
    end
    %importantBins needs to be created! Convention for all further
    %operations in the function.
    args.settings.importantBins=cell(length(uniquePorts),3);
    for jj=1:length(uniquePorts)
        args.settings.importantBins{jj,1}=uniquePorts{jj};
        args.settings.importantBins{jj,2}=args.settings.relevantBins;
        args.settings.importantBins{jj,3}=args.settings.excitedBins;
    end
else
    %importantBins exists and needs to be checked and additional fields need to be
    %created. First find the ports which are undefined.
    undefinedPorts=uniquePorts;
    for jj=1:length(undefinedPorts)
        if (sum(strcmp(args.settings.importantBins(:,1),undefinedPorts{jj})))~=0
            undefinedPorts{jj} = [];
        end
    end
    undefinedPorts=undefinedPorts(~cellfun('isempty',undefinedPorts));
    
    idx=size(args.settings.importantBins,1)+1;
    %Add the undefinedports to the importantBins cell array
    for jj=1:length(undefinedPorts)
        args.settings.importantBins{idx,1}=undefinedPorts{jj};
        %Check if the undefinedPort is actually a conjugate of a given port
        if ~isempty(regexp(undefinedPorts{jj},'_conj','ONCE')) && (sum(strcmp(args.settings.importantBins(:,1),undefinedPorts{jj}(1:regexp(undefinedPorts{jj},'_conj')-1)))) 
            kk=find(strcmp(args.settings.importantBins(:,1),undefinedPorts{jj}(1:regexp(undefinedPorts{jj},'_conj')-1)),1);
        else %undefinedPort was truly not given and its bins should thus be added
            warning(['The important bins of one of the ports (' undefinedPorts{jj} ') were undefined.']);
            kk=1;
        end
        args.settings.importantBins{idx,2}=args.settings.importantBins{kk,2};
        args.settings.importantBins{idx,3}=args.settings.importantBins{kk,3};
        idx=idx+1;
    end
end

% Check if every field requested in U,Y or R is present in the spec
% Two possibilities exist:
% - We need the conjugate of a spectrum (U={{'in_conj'}})
% - A convolution is requested (U={{'in1','in2'}})
% The new spectra are calculated and added to the spec struct
% The U,Y and R cell arrays are also updated with the correect name of the new
% spectra.
M_check=[];
for ii=1:length(portCheck)
    for jj=1:length(args.(portCheck{ii}))       
        for kk=1:length(args.(portCheck{ii}){jj})
            if ischar(args.(portCheck{ii}){jj}{kk})
                if ~(isfield(args.spec,args.(portCheck{ii}){jj}{kk}))
                    %It's not a field in spec...so we most likely need 
                    %to calculate the conjugate and add it.
                    str=args.(portCheck{ii}){jj}{kk};
                    idx=regexp(str,'_conj');
                    if (isfield(args.spec,str(1:idx-1))) && ~isempty(idx)
                        %The conjugate is needed, but not present in the
                        %spec array. Thus it is added...
                        args.spec.(str)=conj(args.spec.(str(1:idx-1)));
                    else
                        error(['A string was found in ' portCheck{ii} ' which was unavailable in the spec struct.']);
                    end
                else
                    [M,P,N]=size(args.spec.(args.(portCheck{ii}){jj}{kk}));
                    if isempty(M_check)
                        M_check=M;
                    end
                    if ~(P==1 && N==length(args.spec.freq) && M==M_check)
                        error(['The dimensions of the field: ' (args.(portCheck{ii}){jj}) ' of spec are incorrect.'])
                    end
                end
            elseif iscell(args.(portCheck{ii}){jj}{kk})
                %We will need to convolve multiple spectra into one
                str_t=strcat(args.(portCheck{ii}){jj}{kk}{:});
                %Matbe the spectra is already present?
                if ~(isfield(args.spec,str_t))
                    %Check if every spectra is present
                    for ll=1:length(args.(portCheck{ii}){jj}{kk})
                        str=args.(portCheck{ii}){jj}{kk}{ll};
                        if ~(isfield(args.spec,str))
                            % It's most likely a conjugate? 
                            idx=regexp(str,'_conj');
                            if (isfield(args.spec,str(1:idx-1))) && ~isempty(idx)
                                args.spec.(str)=conj(args.spec.(str(1:idx-1)));
                            else
                                error(['A string was found in ' portCheck{ii} ' which was unavailable in the spec struct.']);
                            end
                        else
                            [M,P,N]=size(args.spec.(str));
                            if isempty(M_check)
                                M_check=M;
                            end
                            if ~(P==1 && N==length(args.spec.freq) && M==M_check)
                                error(['The dimensions of the field: ' str ' of spec are incorrect.'])
                            end
                        end
                    end
                 % Calculate the spectra and put it in spec using convolution in 
                 % the frequency domain.
                 [args.spec,args.settings.importantBins]=conv_HB(args.(portCheck{ii}){jj}{kk},args.spec,args.settings.importantBins);
                end
                % Change the cell array to a concatenated string
                args.(portCheck{ii}){jj}{kk}=str_t;
            else
                error(['The ' portCheck{ii} '-cell array contained elements which were not recognized by the function.'])
            end
        end
    end
end

%Update the spectrum for user convenience
spec_update=args.spec;

%Update the settings structure for user convenience
settings_update.importantBins=args.settings.importantBins;
settings_update.zpad=args.settings.zpad;
settings_update.signLvl=args.settings.signLvl;

%% Initiate variables (nu,ny,F,...)

NrBHA=length(args.U);

%Pre-allocation of the G struct
G=cell(1,NrBHA);
% G will contain the following fields:
% - mean
% - CvecG
% - varG
% - Z
% - CZ
    
%% Loop for extracting the different BLA, BMA, .... BHA

for ii=1:NrBHA
        %Get the number of ref,input and output ports
        nu=length(args.U{ii});
        ny=length(args.Y{ii});
        if ~isempty(args.R)
            nr = length(args.R{ii});
            if nu>nr
                error('the amount of reference signals should be at least equal to the amount of inputs')
            end
        else
            nr=0;
        end
        
        %Do an additional check on M, is it a multiple of nr?
        if isempty(M_check)
            [M,~,~]=size(args.spec.(args.U{ii}{1}));
        end
        if rem(M,nr)
            error('The number of realizations (ME) is not a multiple of the number of input ports (nu).')
        end
        
        %Define the correct relevantBins and excitedBins from the
        %importantBins cell array
        Bins=cell(1,2);
        for kk=1:2 %Relevant and ExcitedBins have to be found
            for jj=1:(nu+ny)
                if jj==1
                    I=find(strcmp({args.settings.importantBins{:,1}},args.U{ii}{jj})==1,1);
                    Bins{kk}=args.settings.importantBins{I,kk+1};
                elseif jj>nu
                    I=find(strcmp({args.settings.importantBins{:,1}},args.Y{ii}{jj-nu})==1,1);
                    Bins{kk}=intersect(Bins{kk},args.settings.importantBins{I,kk+1});
                else
                    I=find(strcmp({args.settings.importantBins{:,1}},args.U{ii}{jj})==1,1);
                    Bins{kk}=intersect(Bins{kk},args.settings.importantBins{I,kk+1});
                end
            end
        end
        
        NrelevantBins=length(Bins{2}); %Total amount of bins at which relevant information is present
        NexcitedBins=length(Bins{1}); %Number of bins that are actually excited
        
        %pre-allocation of input data
        if nr~=0
            R=zeros(nr,nr,round(M/nr),NrelevantBins);
        else
            R=[];
        end
        U=zeros(nu,nr,round(M/nr),NrelevantBins);
        Y=zeros(ny,nr,round(M/nr),NrelevantBins);
        
        %Get the spectra (U,Y and R if neccessary) in the correct format
        for mm=1:round(M/nr)
            for rr=1:nr
                for uu=1:nu
                    U(uu,rr,mm,:)=args.spec.(args.U{ii}{uu})(rr+(mm-1)*(nu),1,Bins{1});
                    if ~isempty(args.R)
                         R(uu,rr,mm,:)=args.spec.(args.R{ii}{uu})(rr+(mm-1)*(nu),1,Bins{1});
                    end
                end
                for yy=1:ny
                    Y(yy,rr,mm,:)=args.spec.(args.Y{ii}{yy})(rr+(mm-1)*(nu),1,Bins{1});
                end
            end
        end
        
        %ADD WARNING IF ANY OF THE BINS ARE TOTALLY EMPTY!!!!
        
        %Run the BLA extraction function.
        [G{ii}.mean,G{ii}.CvecG,G{ii}.Z,G{ii}.CvecZ,G{ii}.CvecG_m2]=extractMIMO_BLA_v2(U(:,:,1:round(M/nr),:),Y(:,:,1:round(M/nr),:),'R',R,'excitedBins',Bins{2}-Bins{1}(1)+1);
        
        %Extraction of the total covariance matrix from the robust method
        if M/nr~=1
            G{ii}.varG = zeros(ny, nu, NexcitedBins);
            for ff = 1:NexcitedBins
                G{ii}.varG(:,:,ff) = reshape(diag(squeeze(G{ii}.CvecG(:,:,ff))) , [ ny , nu ] );
            end
        end
        
end           

end
    
%% function to check the spectrum passed to the function
function res = checkSpec(s)
    % s should contain a freq field
    if ~isfield(s,'freq')
        error('spec.freq should be provided');
    end
% if you pass all the checks, you are happy!
res = 1;
end

%% function to check the port data passed to the function
function res=checkUY(s)
    
    %Each of the elements of s should be either a string or another cell array
    %If it is another cell array we delve deeper and recursively call this
    %function again.
    if iscell(s)
        for ii=1:length(s)
            if checkUY(s{ii})
                res=1;
            else
                res=0;
            end
        end
    elseif ischar(s)
        res=1;
    else
        res=0;
    end

end

%% Function to convolute two HB spectra in a time efficient manner (sparse)
function [spec,importantBins_new]=conv_HB(node,spec,importantBins)

%Definition of defaultSpec
    defaultSpec=spec.(node{1});

%Get the frequency resolution
    f0=mode(spec.freq-circshift(spec.freq,[0 1]));

%Extract the different spectral blocks out of the spec.freq
    BinContent=[find(round((spec.freq-circshift(spec.freq,1,2)))~=f0) size(defaultSpec,3)+1];
    %BinDiv contains the boundaries of each of the spectral blocks
    BinDiv=zeros(2,length(BinContent)-1);
    for ii=1:length(BinContent)-1
        BinDiv(1,ii)=BinContent(ii);
        BinDiv(2,ii)=BinContent(ii+1)-1;
    end

% Some pre-allocation
    M=size(defaultSpec,1);
    %The dummy_spec is larger than the normal spec because of zero-padding
    dummy_spec=zeros(length(node),M,1,size(defaultSpec,3)+2*sum(1:length(BinContent)-2));
    %The new bins (containing the extra bins because of zero-padding)
    bin_new=zeros(1,size(defaultSpec,3)+2*sum(1:length(BinContent)-2));
    %The old bins
    bin_old=round(spec.freq/f0)+1;
    %Storage of the resulting (non-extended) spectra
    resultSpec=zeros(size(defaultSpec));

%Extend the relevant spectra and put them in dummy_spec
%Bin_new: Filled with bin_old and the extra zero-padding bins
%dummy_spec: Filled with the old spectra, extra content (extended bins) is zero
    for ii=1:length(node)
        for jj=1:size(BinDiv,2)
            %Definition of the start and end index of dummy_spec
            %The movement takes the zero-padded bins into account
            strt_idx=BinDiv(1,jj)+(jj-1)*(jj-1);
            end_idx=BinDiv(2,jj)+(jj-1)*(jj-1);
            dummy_spec(ii,:,1,strt_idx:end_idx)=spec.(node{ii})(:,1,BinDiv(1,jj):(BinDiv(2,jj)));
            if jj~=1 %Special conditions apply to the spectral content around DC
                bin_new(strt_idx-((jj-1):-1:1))=bin_old(BinDiv(1,jj))-((jj-1):-1:1); %Padding to the left
                bin_new(end_idx+(1:(jj-1)))=bin_old(BinDiv(2,jj))+(1:(jj-1)); %Padding to the right
            end
            bin_new(strt_idx:end_idx)=bin_old(BinDiv(1,jj):(BinDiv(2,jj))); %Old bin content is moved to the new one
        end
    end
    %Find the indices where old and new overlap
    idx_old=ismember(bin_new,bin_old);

%The actual proccess loop which will combine the extended spectra of
%dummy_spec
    for kk=1:M %Each realization will be processed seperately...might be changed for speed in future?
        for ii=1:length(node)
            %Put the dummy_spec of realization ii in dummy_iter
            dummy_iter=squeeze(dummy_spec(ii,kk,1,:));
            %Spectral is extended to negative and positive frequency bins
            dummy_iter=[conj(flip(dummy_iter(2:end)))/2;dummy_iter(1);dummy_iter(2:end)/2];
            if ii>=2 %Time for convolution!
                dummy_saved=conv(dummy_iter,dummy_saved);
                %The spectrum has grown X2, thus we throw away half of this junk
                middle=(length(dummy_saved)+1)/2;
                dummy_saved=dummy_saved((middle-(length(dummy_iter)-1)/2):(middle+(length(dummy_iter)-1)/2));
            else %First node can not be convolved yet...
                dummy_saved=dummy_iter;
            end

        end
        
        %Get the spectra in the correct position
        dummy_saved=fftshift(dummy_saved);
        %The last bin is DC, the rest is spectral content
        dummy_saved=[dummy_saved(end);dummy_saved(1:(end-1)/2)];
            
        %Throw away those annoying extended bins
        dummy_saved=dummy_saved(idx_old);
        
        %Spectral should be multiplied by 2 again because HB does the exact
        %same thing. Except for DC which only exists once!
        dummy_saved(2:end)=2*dummy_saved(2:end);
        
        %Get your spectrum in the resultSpec
        resultSpec(kk,1,:)=dummy_saved;
    end
    %Add the new spectra to the spec struct
    spec.(strcat(node{:}))=resultSpec;

%Now it is time to combine the importantBins cell arrays of the convoluted
%spectra using kronsum, In this way we find the relevant bins of our new
%spectra...
    bin_dummy=cell(1);
    %We add a new row to the importantBins cell array
    importantBins_new=cell(length(importantBins)+1,3);
    %Get the old data in the new cell array
    importantBins_new(1:size(importantBins,1),:)=importantBins;
    %The node name is already known
    importantBins_new{length(importantBins)+1,1}=(strcat(node{:}));
    %NOTE: The minus 1 is there because DC should be at 0 and not 1!
    for jj=1:2
        for ii=1:length(node)
            I=find(strcmp(importantBins(:,1),node{ii})==1,1);
            grid=round(spec.freq(importantBins{I,jj+1})/f0);
            if ii>1 %Time for kronecker sum!
                bin_dummy{1}=unique(kronsum(bin_dummy{1},[-grid grid]));
            else %Only one binarray available....
                %Positive AND negative frequencies need to be accounted
                %for!
                bin_dummy{1}=unique([-grid grid]);
            end
        end
        % Extend importantBins correctly with the new bins
        bin_dummy{1}=unique(abs(bin_dummy{1}));
        for ii=1:length(bin_dummy{1})
            bin_dummy{1}(ii)=find(round(spec.freq/f0)==bin_dummy{1}(ii),1);
        end
        importantBins_new{length(importantBins)+1,jj+1}=bin_dummy{1};
    end

end

%% function to check the settings passed to the function
function res=checkSettings(s)
    %Check the reference signal level
    if isfield(s,'sgnLvl') && ~(isscalar(s.sgnLvl) && isnumeric(s.sgnLvl))
        error('The supplied reference signal level was found to be erroneous.');
    end
    
    %Check if excitedBins, relevantBins or importantBins is present
    %If not the bin array will be extracted from the reference
    %Give a warning if this is the case...
    if ~sum(isfield(s,{'excitedBins','relevantBins','importantBins'}))
        warning('The necessary bin array will be extracted from the reference spectra, make sure that is supplied.')
    end
    res=1;
end

%% Kronecker sum to calculate the grid of two convoluted spectra
function z = kronsum(x, y)
    z = kron(x, ones(size(y))) + kron(ones(size(x)), y);
end

% @generateFunctionHelp
% @author Piet Bronders,
% @author Adam Cooman
% @institution ELEC, VUB

% @tagline calculates the BLA, BMA and consequent transfer functions (user-specified) given a reference spectra, input spectra and output spectra.

% @description Here several valid possibilities for the 'U'-cell array are
% @description given. (1) 'in1', {{'in1'}} or {'in1'} (1 SISO BLA);(2) {'in1','in2'} (1 MISO BLA);(3)
% @description {'in1',{'in2','in3'}} (1 MISO BLA/BMA);(4) U{1}={{'in1'}}, U{2}={{'in2'}}
% @description (2 SISO BLA); combinations of the previous examples are possible...

% @version 16-04-2015  Version 1.
% @version 23-04-2015  Fixed multiple errors in regards to the extraction of negative BMAs.
% @version 20-05-2015  Gigantic overhaul of the entire function. The function can now internally make the convolution of input spectra.

% @output1 Cell array of struct containing the calculated BHA, its variance and much more...
% @outputType1 cell array of structs

% @output2 the updated spec struct for user convenience.
% @outputType2 struct

% @output3 the updated settings struct for user convenience.
% @outputType3 struct

% @extra NOTE: The functionality of settings.zpad has NOT been implemented yet! 

%% generateFunctionHelp: old help, backed up at 20-May-2015. leave this at the end of the function
% calcMIMO_BHA calculates the BLA, BMA and consequent transfer functions (user-specified) given a reference spectra, input spectra and output spectra.
% 
%      [G,varG,CvecG,Z,CZ] = calcMIMO_BHA(spec,settings);
%      [G,varG,CvecG,Z,CZ] = calcMIMO_BHA(spec,settings,'ParamName',paramValue,...);
% 
% In the following, we use F to indicate the amount of tones in the multisine. Length
% of the output cell arrays is entirely  user-defined and can be given to function
% by modifying the  order of one the input. The cell length will be equal to 2*(prod(order))-1.
% The factor 2 being present because both negative and positive BHA have to be extracted.
% 
% Required inputs:
% - spec      check: @(x) checkSpec(x) && isstruct(x)
%      spec structure containing the relevant data fields. (Output
%      of the HB simulation)
% - settings      check: @(x) checkSettings(x) && isstruct(x)
%      Struct containing the several fields with not all of them being
%       necessary. The carrier frequency fc can be given as a setting,
%      if not the  carrier frequency will be extracted using operations
%      on the frequency  grid of spec. This is not recommended as it
%      quite prone to errors.  The excitedBins and relevantBins are
%      two other settings which contain the  excited frequency bins
%      and all relevant (unexcited and excited bins)  respectively.
%      Either the excitedBins or relevantBins-vector should be  given.
%      If only one is given the function assumes that these two vectors
%       are equal.
% 
% Parameter/Value pairs:
% - 'U'     default: struct('nodeNames'  check: {'in'},'order',0),@(x) checkUY(x) && isstruct(x)
%      Struct containing the input node names and the requested harmonic
%      order.  The nodeNames-field contains the names of the nodes
%      of the spectra that  signify the input spectra. This variable
%      should contain a cell array of strings.  The order-field on
%      the other hand is a vector or Matrix containing the harmonic
%       orders around which the HTFs will be extracted. In the case
%      of a single  non-singular dimension, the function will assume
%      that there's only 1 BLA requested.  (length(U.nodeNames) x nrofBLA)
% - 'Y'     default: struct('nodeNames'  check: {'out'},'order',0),@(x) checkUY(x) && isstruct(x)
%      Struct containing the output node names and the requested harmonic
%      order.  See 'U' for the possible fields of this struct.
% - 'R'     default: struct('nodeNames'  check: [],'order',[]),@(x) checkUY(x) & isstruct(x)
%      Struct containing the reference node names and the requested
%      harmonic order.  See 'U' for the possible fields of this struct.
% 
% Outputs:
% - G      Type: cell array of [ny X nu X F]
%      The calculated MIMO BLA.
% - varG      Type: cell array of [ny X nu X F]
%      Total variance of the BLA.
% - CvecG      Type: cell array of [ny*nu X ny*nu X F]
%      the Covariance matrix of the BLA.
% - Z      Type: cell array of [(ny+nu) X nu X F]
%      Stacked input-output spectra, averaged over the realisations.
%      
% - CZ      Type: cell array of [(ny+nu) X (ny+nu) X nu X F]
%      The covariance matrix of Z.
% 
% NOTE: The first bin (1) is always expected to be DC.
% 
% Piet Bronders, Adam Cooman, ELEC, VUB
% 
% Version info:
%  16-04-2015  Version 1.
%  23-04-2015  Fixed multiple errors in regards to the extraction of negative BMAs.
% 
% This documentation was generated with the generateFunctionHelp function at 23-Apr-2015
