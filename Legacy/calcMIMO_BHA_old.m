function [G,varG,CvecG,Z,CZ]=calcMIMO_BHA(spec,settings,varargin)
% calcMIMO_BHA calculates the BLA, BMA and consequent transfer functions (user-specified) given a reference spectra, input spectra and output spectra.
% 
%      [G,varG,CvecG,Z,CZ] = calcMIMO_BHA(spec,settings);
%      [G,varG,CvecG,Z,CZ] = calcMIMO_BHA(spec,settings,'ParamName',paramValue,...);
% 
% In the following, we use F to indicate the amount of tones in the multisine. Length
% of the output cell arrays is entirely  user-defined and can be given to function
% by modifying the  order of one the input. The cell length will be equal to 2*(prod(order))-1.
% The factor 2 being present because both negative and positive BHA have to be extracted.
% 
% Required inputs:
% - spec      check: @(x) checkSpec(x) && isstruct(x)
%      spec structure containing the relevant data fields. (Output
%      of the HB simulation)
% - settings      check: @(x) checkSettings(x) && isstruct(x)
%      Struct containing the several fields with not all of them being
%       necessary. The carrier frequency fc can be given as a setting,
%      if not the  carrier frequency will be extracted using operations
%      on the frequency  grid of spec. This is not recommended as it
%      quite prone to errors.  The excitedBins and relevantBins are
%      two other settings which contain the  excited frequency bins
%      and all relevant (unexcited and excited bins)  respectively.
%      Either the excitedBins or relevantBins-vector should be  given.
%      If only one is given the function assumes that these two vectors
%       are equal.
% 
% Parameter/Value pairs:
% - 'U'     default: struct('nodeNames'  check: {'in'},'order',0),@(x) checkUY(x) && isstruct(x)
%      Struct containing the input node names and the requested harmonic
%      order.  The nodeNames-field contains the names of the nodes
%      of the spectra that  signify the input spectra. This variable
%      should contain a cell array of strings.  The order-field on
%      the other hand is a vector or Matrix containing the harmonic
%       orders around which the HTFs will be extracted. In the case
%      of a single  non-singular dimension, the function will assume
%      that there's only 1 BLA requested.  (length(U.nodeNames) x nrofBLA)
% - 'Y'     default: struct('nodeNames'  check: {'out'},'order',0),@(x) checkUY(x) && isstruct(x)
%      Struct containing the output node names and the requested harmonic
%      order.  See 'U' for the possible fields of this struct.
% - 'R'     default: struct('nodeNames'  check: [],'order',[]),@(x) checkUY(x) & isstruct(x)
%      Struct containing the reference node names and the requested
%      harmonic order.  See 'U' for the possible fields of this struct.
% 
% Outputs:
% - G      Type: cell array of [ny X nu X F]
%      The calculated MIMO BLA.
% - varG      Type: cell array of [ny X nu X F]
%      Total variance of the BLA.
% - CvecG      Type: cell array of [ny*nu X ny*nu X F]
%      the Covariance matrix of the BLA.
% - Z      Type: cell array of [(ny+nu) X nu X F]
%      Stacked input-output spectra, averaged over the realisations.
%      
% - CZ      Type: cell array of [(ny+nu) X (ny+nu) X nu X F]
%      The covariance matrix of Z.
% 
% NOTE: The first bin (1) is always expected to be DC.
% 
% Piet Bronders, Adam Cooman, ELEC, VUB
% 
% Version info:
%  16-04-2015  Version 1.
%  23-04-2015  Fixed multiple errors in regards to the extraction of negative BMAs.
% 
% This documentation was generated with the generateFunctionHelp function at 23-Apr-2015

%% parse the input

p = inputParser();
p.FunctionName = 'calcMIMO_BHA';

% spec structure containing the relevant data fields. (Output of the HB simulation)
p.addRequired('spec',@(x) checkSpec(x) && isstruct(x));

% Struct containing the several fields with not all of them being
% necessary. The carrier frequency fc can be given as a setting, if not the
% carrier frequency will be extracted using operations on the frequency
% grid of spec. This is not recommended as it quite prone to errors.
% The excitedBins and relevantBins are two other settings which contain the
% excited frequency bins and all relevant (unexcited and excited bins)
% respectively. Either the excitedBins or relevantBins-vector should be
% given. If only one is given the function assumes that these two vectors
% are equal.
p.addRequired('settings',@(x) checkSettings(x) && isstruct(x));

% Struct containing the input node names and the requested harmonic order.
% The nodeNames-field contains the names of the nodes of the spectra that 
% signify the input spectra. This variable should contain a cell array of strings.
% The order-field on the other hand is a vector or Matrix containing the harmonic 
% orders around which the HTFs will be extracted. In the case of a single
% non-singular dimension, the function will assume that there's only 1 BLA requested. 
% (length(U.nodeNames) x nrofBLA)
p.addParameter('U',struct('nodeNames',{'in'},'order',0),@(x) checkUY(x) && isstruct(x));

% Struct containing the output node names and the requested harmonic order.
% See 'U' for the possible fields of this struct.
p.addParameter('Y',struct('nodeNames',{'out'},'order',0),@(x) checkUY(x) && isstruct(x));

% Struct containing the reference node names and the requested harmonic order.
% See 'U' for the possible fields of this struct.
p.addParameter('R',struct('nodeNames',[],'order',[]),@(x) checkUY(x) & isstruct(x));

p.StructExpand = true;
p.parse(spec,settings,varargin{:});
args = p.Results();

%% Do additional checks on the supplied input data and add missing fields and data

% R,U & Y
% Add necessary fields to U,Y and R if they are unavailable
portCheck={'U','Y','R'};
default_names={'in','out','ref'};
for ii=1:length(portCheck)
    if ~(isfield(args.(portCheck{ii}),'order') && isfield(args.(portCheck{ii}),'nodeNames')) %order OR nodeNames is not present
        if isfield(args.(portCheck{ii}),'order') %order is present, but nodeNames is not.
            args.(portCheck{ii}).nodeNames=[];
            for kk=1:size(args.(portCheck{ii}).order,1)
                args.(portCheck{ii}).nodeNames=[args.(portCheck{ii}).nodeNames,[default_names{ii} num2str(kk)]];
            end
        elseif isfield(args.(portCheck{ii}),'nodeNames') %nodeNames is present, but order is not.
            args.(portCheck{ii}).order=getOrderFromNodeNames(args.(portCheck{ii}).nodeNames,[]);
            % Check if the number of reference and input nodes are equal, only if the
            % R.nodeNames is not empty. 
            if strcmp((portCheck{ii}),'R') && ~(length(args.R.nodeNames)==length(args.U.nodeNames))
                error('The number of reference and input nodes should be equal. (if R is given)')
            end
        else %Neither nodeNames nor order is present
            args.(portCheck{ii}).order=0;
            args.(portCheck{ii}).nodeNames=[default_names{ii} num2str(1)];
        end
    else %order AND nodeNames are present, check if order is chosen correctly
        args.(portCheck{ii}).order=getOrderFromNodeNames(args.(portCheck{ii}).nodeNames,args.(portCheck{ii}).order); 
    end
end

%get f0 out of the spectral data
f0=median(spec.freq-circshift(spec.freq,[0 1]));

% SPEC
% Check if all the necessary nodes/fields are present in spec
% Meanwhile check the size of these fields as well and the consistency of
% their dimensions
nodeCheck=[args.R.nodeNames args.U.nodeNames args.Y.nodeNames];
M_check=[];
for ii=1:length(nodeCheck)
    if ~isfield(args.spec,nodeCheck{ii});
        error(['spec needs the field: ' nodeCheck{ii,1}])
    end
    [M,P,N]=size(args.spec.(nodeCheck{ii}));
    if isempty(M_check)
        M_check=M;
    end
    if ~(P==1 && N==length(args.spec.freq) && M==M_check)
        error(['The dimensions of the field: ' nodeCheck{ii,1} ' of spec are incorrect.'])
    end
end

%All necessary orders are extracted from the order-fields.
order_all=unique([unique(args.U.order).' unique(args.Y.order).' unique(args.R.order).']);

% SETTINGS.FC
% If fc is not given while there's an order in the input/output vector, it
% needs to be extracted from the spectral data.
if ~isfield(args.settings,'fc') && ~isempty(find(order_all(order_all>0),1))
    dist =((abs(args.spec.freq-circshift(args.spec.freq,[0,1]))));
    I=zeros(3,1);
    jj=1;
    for ii=1:length(dist)
        if round(dist(ii)/f0)~=1
            I(jj)=ii;
            jj=jj+1;
            if jj==4
                break
            end
        end
    end
    args.settings.fc=round(args.spec.freq((I(3)+I(2)-1)/2));
    warning(sprintf('fc was derived from the freq array and estimated to be %0.5e',args.fc)); %#ok
end

% SETTINGS.EXCITEDBINS, SETTINGS.RELEVANTBINS
%Get the size of the excited frequency bins and put it in F
if ~isfield(args.settings,'excitedBins')
    %If excitedBins is empty, the entirety of frequencyBins is expected to be
    %excited content.
    if  isempty(args.R.nodeNames)
        args.settings.excitedBins=args.settings.relevantBins;
    else
        %Extract the excitedBins from the reference
        % I use the first realisation of the first input. The multisine should use the same grid anywhere anyway.
        temp=args.spec.(args.R.nodeNames{1});
        args.settings.excitedBins = find(db(temp(1,1,:))>-400);
        args.settings.excitedBins = args.settings.excitedBins(args.settings.excitedBins~=1);% remove DC
    end
elseif ~isfield(args.settings,'relevantBins')
    args.settings.relevantBins=args.settings.excitedBins;
end

%% Initiate variables (nu,ny,F,...)

%Get the number of ref,input and output ports
nr=length(args.R.nodeNames);
nu=length(args.U.nodeNames);
ny=length(args.Y.nodeNames);

%Do an additional check on M, is it a multiple of nu?
if rem(M,nu)
    error('The number of realizations (M) is not a multiple of the number of input ports (nu).')
end

F=length(args.settings.excitedBins); %Bins at which we want to extract the BLA
F_all=length(args.settings.relevantBins); %Total amount of bins at which relevant information is present

%% Generate the Z-matrix, a matrix containing reference, input and output
% spectra in a stacked fashion
Z_all=zeros(nr+nu+ny,M,N);

for ii=1:(nr+nu+ny)
    if ii<=nr
        Z_all(ii,:,:)=args.spec.(args.R.nodeNames{ii});
    elseif ii<=nr+nu
        Z_all(ii,:,:)=args.spec.(args.U.nodeNames{ii-nr});
    else
        Z_all(ii,:,:)=args.spec.(args.Y.nodeNames{ii-nr-nu});           
    end
end
Z_all=reshape(Z_all,[nr+nu+ny,nu,round(M/nu),N]);

%% Initiate the orderMatrix
% Matrix of the grid conversion necessary when the user is not only
% interested in the baseband BLA but also in succesive BLAs of HTFs.
% -- Example for a SISO system --
% conversionMatrix=[0 0]
%                  [0 0]
%                  [0 2]
% --> Reference and input around DC and output around the DC AND second
% harmonic of fc are used.
% RESULT: Baseband BLA, HTF+2 and HTF-2 are extracted
% NOTE: if reference is not given, there should not be any value for the
% reference in this matrix either.

% Make the combination of all of the posssible orders.
orderMatrix=combvec(args.R.order,args.U.order,args.Y.order);

for jj=order_all(order_all>0) %Do not attempt to find the DC bin...it is zero
    %Find the relevant bins (containing fc,2*fc and so on, depending on what is found in orderMatrix)
    % and replace the harmonic number by the bin itself.
   orderMatrix(orderMatrix==jj)= find(round(args.spec.freq/f0)==jj*args.settings.fc/f0)-1;
   % minus 1 since DC is considered to be bin 1
end

orderMatrix=[orderMatrix -orderMatrix];
orderMatrix=unique(orderMatrix.','rows').';

%% Pre-allocation of the data

iter=size(orderMatrix,2); % Orders higher than 0 require the extraction of NEGATIVE and POSITIVE HTF

%input data

    if nr~=0
        R=zeros(nu,nu,round(M/nu),F_all);
    else
        R=[];
    end
    U=zeros(nu,nu,round(M/nu),F_all);
    Y=zeros(ny,nu,round(M/nu),F_all);

%output data
    G=cell(1,iter);
    CvecG=cell(1,iter);
    Z=cell(1,iter);
    CZ=cell(1,iter);
    varG=cell(1,iter);
    
%% Loop for extracting the different BLA, BMA, .... BHA

for ii=1:iter
        %Select relevant spectra & Shift the spectra if necessary.
        for jj=1:size(orderMatrix,1)
            spectralContent=Z_all(jj,:,:,:);
            %Shift the spectra by -K positions to get the relevant harmonic (k*fc)
            %given by the conversionMatrix to the DC position.
            spectralContent=circshift(spectralContent,-abs(orderMatrix(jj,ii)),4);
            if orderMatrix(jj,ii)<0 %Check if we want to extract a negative or positive HTF
                spectralContent=conj(spectralContent(:,:,:,N-args.settings.relevantBins+2)); % negative HTF.
            else
                spectralContent=spectralContent(:,:,:,args.settings.relevantBins); % positive HTF.
            end
                if jj<=nr %Find to which spectra the content belongs to. 
                    R(jj,:,:,:)=spectralContent;
                elseif jj<=nr+nu
                    U(jj-nr,:,:,:)=spectralContent;
                else
                    Y(jj-nu-nr,:,:,:)=spectralContent;                     
                end
        end 
        
        %Run the BLA extraction function.
        [G{ii},CvecG{ii}, Z{ii},CZ{ii}]=extractMIMO_BLA(U(:,:,1:round(M/nu),:),Y(:,:,1:round(M/nu),:),'R',R,'excitedBins',args.settings.excitedBins);
        
        %Extraction of the total covariance matrix from the robust method
        if M~=1
            varG{ii} = zeros(ny, nu, F);
            for ff = 1:F
                varG{ii}(:,:,ff) = reshape(diag(squeeze(CvecG{ii}(:,:,ff))) , [ ny , nu ] );
            end
        end
        
end           

end
    
%% function to check the spectrum passed to the function
function res = checkSpec(s)
    % s should contain a freq field
    if ~isfield(s,'freq')
        error('spec.freq should be provided');
    end
% if you pass all the checks, you are happy!
res = 1;
end

%% function to check the nodes passed to the function
function res=checkUY(s)
    if ~isfield(s,'nodeNames') && ~isfield(s,'order')
        error('An input or output needs at least one argument to properly function. (Either the nodeNames or the order should be given)');
    end
        
    if isfield(s,'nodeNames')
        if ~iscellstr(s.nodeNames)
            error('The type of one of the nodeNames vectors is incorrect.');
        end 
    end
    if isfield(s,'order')
        if ~isnumeric(s.order) || ~isempty(find(s.order<0,1)) || ~isempty(find(rem(s.order,1)~=0,1))
            error('There is an incorrect value present in one of the order matrix.');
        end
    end

    res=1;
    
end

%% function to check the nodes passed to the function
function res=checkSettings(s)
    if isfield(s,'fc')
        if ~(isscalar(s.fc) && isnumeric(s.fc) && (s.fc>=0))
            error('The carrier frequency was found to be incorrect.');
        end
    end
    if ~isfield(s,'excitedBins') && ~isfield(s,'relevantBins')
        error('Either the relevantBins or excitedBins field should be given.')
    end
    res=1;
end

%% Function that compares the nodeNames and order. The order and nodeNames are modified if necessary
function order=getOrderFromNodeNames(nodeNames,order)
if isempty(order) %If the order is empty, we set the order to a zero column
    order=zeros(length(nodeNames),1);
elseif isscalar(order) && length(nodeNames)>1 %Only one number is given for the order, so we extend the order to the DIM of the nodeNames
    order=order*ones(length(nodeNames),1);
elseif isvector(order) && length(nodeNames)>1 %If it's a vector we need to extend it (if length(nodeNames)>1)
    if isrow(order)
        order=repmat(order,[length(nodeNames),1]);
    else
        order=repmat(order.',[length(nodeNames),1]);
    end
elseif length(size(order))==2
    if size(order,1)~= length(nodeNames)
        error('Size of the order matrix and its nodeNames vector are inconsistent.');
    else %Check if the content in each of the rows is unique.
        [order,Ipre,Ipost] = unique(order.', 'rows');
        if sum(sum(Ipre))~=sum(sum(Ipost))
            warning(['One of the rows of the order-matrix is repeated, please double-check your order-matrix.' ...
                'This redundant row of the order matrix will be removed to speed up the function.']);
        end
        order=order.';
    end
else
    error('The size or type of order is not compatible with this function.')
end
end

% @generateFunctionHelp
% @author Piet Bronders,
% @author Adam Cooman
% @institution ELEC, VUB

% @tagline calculates the BLA, BMA and consequent transfer functions (user-specified) given a reference spectra, input spectra and output spectra.

% @description In the following, we use F to indicate the amount of tones
% @description in the multisine. Length of the output cell arrays is entirely 
% @description user-defined and can be given to function by modifying the 
% @description order of one the input. The cell length will be equal to
% @description 2*(prod(order))-1. The factor 2 being present because both
% @description negative and positive BHA have to be extracted.

% @version 16-04-2015  Version 1.
% @version 23-04-2015  Fixed multiple errors in regards to the extraction of negative BMAs.

% @output1 The calculated MIMO BLA.
% @outputType1 cell array of [ny X nu X F]

% @output2 Total variance of the BLA.
% @outputType2 cell array of [ny X nu X F]

% @output3 the Covariance matrix of the BLA.
% @outputType3 cell array of [ny*nu X ny*nu X F]

% @output4 Stacked input-output spectra, averaged over the realisations. 
% @outputType4 cell array of [(ny+nu) X nu X F]

% @output5 The covariance matrix of Z.
% @outputType5 cell array of [(ny+nu) X (ny+nu) X nu X F]

% @extra NOTE: The first bin (1) is always expected to be DC.

%% generateFunctionHelp: old help, backed up at 23-Apr-2015. leave this at the end of the function
% calcMIMO_BHA calculates the BLA, BMA and consequent transfer functions (user-specified) given a reference spectra, input spectra and output spectra.
% 
%      [G,varG,CvecG,Z,CZ] = calcMIMO_BHA(spec,settings);
%      [G,varG,CvecG,Z,CZ] = calcMIMO_BHA(spec,settings,'ParamName',paramValue,...);
% 
% In the following, we use F to indicate the amount of tones in the multisine. Length
% of the output cell arrays is entirely  user-defined and can be given to function
% by modifying the  order of one the input. The cell length will be equal to 2*(prod(order))-1.
% The factor 2 being present because both negative and positive BHA have to be extracted.
% 
% Required inputs:
% - spec      check: @(x) checkSpec(x) && isstruct(x)
%      spec structure containing the relevant data fields. (Output
%      of the HB simulation)
% - settings      check: @(x) checkSettings(x) && isstruct(x)
%      Struct containing the several fields with not all of them being
%       necessary. The carrier frequency fc can be given as a setting,
%      if not the  carrier frequency will be extracted using operations
%      on the frequency  grid of spec. This is not recommended as it
%      quite prone to errors.  The excitedBins and relevantBins are
%      two other settings which contain the  excited frequency bins
%      and all relevant (unexcited and excited bins)  respectively.
%      Either the excitedBins or relevantBins-vector should be  given.
%      If only one is given the function assumes that these two vectors
%       are equal.
% 
% Parameter/Value pairs:
% - 'U'     default: struct('nodeNames'  check: {'in'},'order',0),@(x) checkUY(x) && isstruct(x)
%      Struct containing the input node names and the requested harmonic
%      order.  The nodeNames-field contains the names of the nodes
%      of the spectra that  signify the input spectra. This variable
%      should contain a cell array of strings.  The order-field on
%      the other hand is a vector or Matrix containing the harmonic
%       orders around which the HTFs will be extracted. In the case
%      of a single  non-singular dimension, the function will assume
%      that there's only 1 BLA requested.  (length(U.nodeNames) x nrofBLA)
% - 'Y'     default: struct('nodeNames'  check: {'out'},'order',0),@(x) checkUY(x) && isstruct(x)
%      Struct containing the output node names and the requested harmonic
%      order.  See 'U' for the possible fields of this struct.
% - 'R'     default: struct('nodeNames'  check: [],'order',[]),@(x) checkUY(x) & isstruct(x)
%      Struct containing the reference node names and the requested
%      harmonic order.  See 'U' for the possible fields of this struct.
% 
% Outputs:
% - G      Type: cell array of [ny X nu X F]
%      The calculated MIMO BLA.
% - varG      Type: cell array of [ny X nu X F]
%      Total variance of the BLA.
% - CvecG      Type: cell array of [ny*nu X ny*nu X F]
%      the Covariance matrix of the BLA.
% - Z      Type: cell array of [(ny+nu) X nu X F]
%      Stacked input-output spectra, averaged over the realisations.
%      
% - CZ      Type: cell array of [(ny+nu) X (ny+nu) X nu X F]
%      The covariance matrix of Z.
% 
% NOTE: The first bin (1) is always expected to be DC.
% 
% Piet Bronders, Adam Cooman, ELEC, VUB
% 
% Version info:
%  16-04-2013  Version 1.
% 
% This documentation was generated with the generateFunctionHelp function at 22-Apr-2015
