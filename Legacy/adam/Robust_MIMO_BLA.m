function [ G , CvecG , Z , CZ ] = Robust_MIMO_BLA( U , Y , R )
% 
%
%      [ G , CvecG , Z , CZ ] = Robust_MIMO_BLA( U , Y , R );
%
%   Input
%
%       U              =  input signal; size [nu x nu x M x F];
%                          
%       Y              =   ouput signal; size [ny x nu x M x F];
%                           
%       R              =   reference signal; size [nu x nu x M x F];
%                            Note: - the phase spectrum varies randomly over the realisations, the experiments,
%                                    and the frequencies, while the amplitude spectrum remains the same.
%               nu : The amount of inputs
%               ny : The amount of outputs
%               M  : The amount of full MIMO realisations
%               F  : The amount of excited frequency lines in the multisine
%
%   Output
%
%	    G		=	estimated frequency response matrix; size [ny x nu x F]
%
%       CvecG   =   covariance matrices of vec(G); 
%                       CvecG.NL = total (noise + nonlinear distortion) covariance matrix of vec(G)
%                                   size [(ny*nu) x (ny*nu) x F]
%                       CvecG.S  = covariance matrix of the stochastic nonlinear distortions in vec(G) w.r.t. one
%                                   realisation and one MIMO experiment.
%                                   size [(ny*nu) x (ny*nu) x F]
%
%       Z       =   sample mean Fourier coefficients with noise and/or system transient removed for all M independent full MIMO experiments
%                   (= nu MIMO experiments), where Z stacks the output and input spectra stacked on top of each other: Z = [Y; U];
%                   size (ny+nu) x nu x F
%                   If the reference signal r(t) is available then
%                       Z is the mean value of the projected output-input spectra over all periods and realisations
%                   else
%                   	Z is empty except if M = 1 then Z is the mean value over all periods
%
%       CZ      =   sample covariance matrices of the sample mean of the Fourier coefficients Z for the nu independent MIMO
%                   experiments, where Z = [Y; U] contains the ouput and input spectra stacked on top of each other;
%                   size (ny+nu) x (ny+nu) x nu x F
%                   struct('NL', [], 'S', [])
%                       CZ.NL           =   total (noise + NL distortion) covariance matrix of Z
%                                               CZ.NL(:, :, ii) = Cov(NZ) /(M*P) + Cov(ZS) / M
%                                               CZ.NL(1:ny, 1:ny, ii) = Cov(NY) /(M*P) + Cov(YS) / M
%                                               CZ.NL(ny+1:end, ny+1:end, ii) = Cov(NU) /(M*P) + Cov(US) / M
%                                               CZ.NL(1:ny, ny+1:end, ii) = Cov(NY, NU)/(M*P) + Cov(YS, US) / M
%                                               CZ.NL(ny+1:end, 1:ny, ii) = Cov(NU, NY)/(M*P) + Cov(US, YS) / M
%                                           with NX the noise on X w.r.t one period and one MIMO experiment, and
%                                           with XS the stochastic NL distortions w.r.t. one realisation and one MIMO experiment
%
%                       CZ.S            =   covariance matrix of the stochastic nonlinear distortions in Z w.r.t. one realisation,
%                                           and one MIMO experiment
%                                               CZ.S(:, :, ii) = Cov(ZS)
%                                               CZ.S(1:ny, 1:ny, ii) = Cov(YS)
%                                               CZ.S(ny+1:end, ny+1:end, ii) = Cov(US)
%                                               CZ.S(1:ny, ny+1:end, ii) = Cov(YS, US)
%                                               CZ.S(ny+1:end, 1:ny, ii) = Cov(US, YS)
%                                           with XS the stochastic NL distortions w.r.t. one realisation and one MIMO experiment
%
%                   Notes:
%                        - CZ.NL, and CZ.S are calculated only if Y, U, and R are available
%                        - if the actuator is linear and there is no nonlinear interaction between the generator and the
%                          NL system, then US = 0
%
% Copyright (c) Rik Pintelon, Vrije Universiteit Brussel - dept. ELEC, 29 October 2009
% All rights reserved.
% Software can be used freely for non-commercial applications only.
% Version 29 March 2010
%
% Stripped-down version of the function, made to determine the mimo_bla in a noiseless simulation environment.
%
% Extra assumptions we can make in the simulator are:
%   - There is no noise
%   - The signals are measured in steady-state
%   - There's only one period provided, immediately in the frequency domain
%   - Adam does not like the LPM. His hatred has no boundaries!
%   - All the bins are excited

if exist('R','var')
    if ~isempty(R)
        Reference = true;
    else
        Reference = false;
    end
else
    Reference = false;
    R = [];
end

% Y is an [ny x nu x M x F] matrix
% U is an [nu x nu x M x F] matrix
% R is an [nu x nu x M x F] matrix or an empty matrix
[ nu , ny , M , F ] = checkInputs(U,Y,R);

Z = zeros(ny+nu, nu, F);
CZ = struct('NL', [], 'S', []);
G = zeros(ny, nu, F);
CvecG = struct('NL', [],  'S', []);

if M > 1
    CvecG.NL = zeros(ny*nu, ny*nu, F);
    CvecG.S = zeros(ny*nu, ny*nu, F);
    if Reference
        CZ.NL = zeros(ny+nu, ny+nu, nu, F);
        CZ.S = zeros(ny+nu, ny+nu, nu, F);
    end
end

Z_all = [Y; U];

switch Reference
    case 1 % the reference signal is available
%%        
        % DFT spectra of the reference signal for all realisations and all experiments
        R_all = R;
        Tphase_all = R_all./abs(R_all)/sqrt(nu);  % unitary matrix of the input phases, equation (10)
        
        % turning the phases of the input-output spectra for all realisations and all frequencies
        if nu == 1
            Z_all = Z_all .* repmat(conj(Tphase_all), [ny+nu, 1, 1, 1]); % equation (44)
        else % nu > 1
            for mm = 1:M
                for ff = 1:F
                    Z_all(:, :, mm, ff) = Z_all(:, :, mm, ff) * squeeze(Tphase_all(:, :, mm, ff))'; % equation (44)
                end 
            end 
        end 
        
        % sample mean input-output spectra over the M realisations. Z_all is [nu+ny x nu x M x F]
        Zmean = mean(Z_all, 3); % equation (46)
        
        % input-output residuals
        Zres_all = Z_all - repmat(Zmean, [1, 1, M, 1]); % equation (45, part 2)
        
        % remove the singular realisation dimension. Zmean is [ny+nu x nu x F]
        Z(:,:,:) = squeeze(Zmean);
        
        % calculate the BLA using the FRFs from reference to input and from reference to output
        [G, SUInv] = FRM(Z); % equation (13)
        
        % calculate the covariance of the Z matrix
        Zres_all = permute(Zres_all, [3 1 2 4]);
        for kk = 1:F
            for ee = 1:nu
                CZ.NL(:, :, ee, kk) = cov(squeeze(Zres_all(:, :, ee, kk))); % equation (45, part 1)
            end
        end
        CZ.NL = CZ.NL/M; % total covariance of the mean value
        
        % calculate the distortion introduced by the system.
        systemdistortion = zeros(1,F);
        temp = zeros(ny,nu,mm);
        temp2 = zeros(ny,mm);
        for ff=1:F
            for mm=1:M
                for ee=1:nu
                    % Ys = Y - G * U
                    temp(:,ee,mm) = [eye(ny) -G(:,:,ff)] * Z_all(:,ee,mm,ff);
                end
                temp2(:,mm) = rms(squeeze(temp(:,:,mm)));
            end
            Ys = rms(temp2);
            % just sum over the outputs to obtain 1 number per frequency
            systemdistortion(ff) = sum(Ys);
        end
        
        % calculate the total distortion at the output node
        outputdistortion = zeros(1,F);
        temp=zeros(ny+nu,nu,F);
        for ff=1:F
            for mm=1:M
                for ee=1:nu
                    temp(:,ee,mm) = Z_all(:,ee,mm,ff) - Z(:,ee,ff);
                end
                temp2(:,mm) = rms(squeeze(temp(:,:,mm)));
            end
            Yt = rms(temp2);
            % just sum over the different outputs as well 
            outputdistortion(ff) = sum(Yt);
        end
        
        % determine whether the system is the dominant source of distortion, or the generator
        if sum(outputdistortion)>20*sum(systemdistortion)
            errorSource='Generator';
        else
            errorSource='System';
        end
        
        disp(['The dominant source of errors is the ' errorSource]);
        
        switch errorSource
            case 'System' % process errors dominant, use the classic method to determine the covariance
                
                % CZ.NL = covariance matrix of the output and input spectra on top of each other of the nu experiments [size (ny+nu) x (ny+nu) x nu x F]
                CvecG.NL = Cov_FRM( G, SUInv, CZ.NL );  % total covariance matrix FRM
                
            case 'Generator' % generator error dominant, special case, use section 3.2.3 from the paper to calculate the covariance
                
                G_m = zeros(ny, nu, M, F); % G_m is the G^[m] of the paper.
                for mm = 1:M
                    G_m(:,:,mm,:) = FRM(squeeze(Z_all(:,:,mm,:))); % FRM and inverse input matrix
                end
                
                % calculate the BLA without reference
                G_noref = squeeze(mean(G_m, 3)); % equation (56)
                
                % calculate the residuals vectors
                vecGres_all = zeros(ny*nu,M,F);
                for mm=1:M
                    for ff=1:F
                        vecGres_all(:,mm,ff) = vec(squeeze(G_m(:,:,mm,ff))) - vec( G_noref(:,:,ff) ); % equation (55), part 2
                    end
                end
                
                % calculate the covariance matrix for all frequencies
                for ff=1:F
                    CvecG.NL(:,:,ff) = cov(squeeze(vecGres_all(:,:,ff)).'); % equation (55), part 1
                end
        end
        
        
  
    case 0 % the reference signal is not available. TODO: finish this
%%        
        
        % FRM for all realisations
        G_all = zeros(ny, nu, M, F);
        Zm = zeros(ny+nu, nu, F);                                       % value input-output spectra of the mth realisation
        for mm = 1:M
            Zm(:,:,:) = squeeze(Z_all(:,:,mm,:));
            Gm = FRM(Zm);                                               % FRM and inverse input matrix
            G_all(:,:,mm,:) = Gm;
        end % mm, realisations
        
        % mean (over the realisations) estimate FRM
        G(:,:,:) = squeeze(mean(G_all, 3));
        
        % estimate total variance of the FRM
        Gres_all = G_all - repmat(reshape(G, [ny,nu,1,F]), [1,1,M,1]);  % FRM residuals; G is an ny x nu x F matrix, G_all is ny x nu x M x F
        
        % this should be done with the reshape function
        vecGres_all = zeros(ny*nu,M,F);
        for mm=1:M
            for ff=1:F
                vecGres_all(:,mm,ff) = vec(Gres_all(:,:,mm,ff));
            end
        end
        
        % calculate the covariance matrix for all frequencies
        for ff=1:F
            CvecG.NL(:,:,ff) = cov(squeeze(vecGres_all(:,:,ff)).');
        end
        
        
        % local polynomial estimate total variance FRM
        data_res = struct('U', [], 'Y', [], 'freq', []);
        data_res.freq = ExcitedHarm;
        method_res = struct('order', [], 'dof', []);
        method_res.order = method.order;
        method_res.dof = ceil(method.dof/(M-1));                         % M-1 independent residuals;
        method_res.transient = 0;                                        % the stochastic NL distortions are periodic => no leakage
        CvecG_all = zeros(ny*nu, ny*nu, M, F);                           % intermediate variable
        for mm = 1:M
            data_res.Y = reshape(Gres_all(:,:,mm,:), [ny*nu, F]);
            [CC, xx1, xx2, xx3, xx4, dofNL, CLNL] = LocalPolyAnal(data_res, method_res);
            CvecG_all(:, :, mm, :) = CC.n;
        end % mm, realisations
        clear xx1 xx2 xx3 xx4
        
        CvecG.NL_lpm(:,:,:) = squeeze(mean(CvecG_all, 3))/(M-1);
        
end

end

function [nu,ny,M,F]=checkInputs(U,Y,R)
    % get all the dimensions out of Y
    [ny,nu,M,F] = size(Y);
    
    % check the dimensions of U
    [nut1,nut2,Mt,Ft]=size(U);
    Uerror = 'size of the input should be [nu x nu x M x F]. Either U of Y is wrong';
    if nut1~=nu
        error(Uerror);
    end
    if nut2~=nu
        error(Uerror);
    end
    if Mt~=M
        error(Uerror);
    end
    if Ft~=F
        error(Uerror);
    end
    
    % if R is passed, Check R
    if ~isempty(R)
        [nut1,nut2,Mt,Ft]=size(R);
        Rerror = 'size of the reference should be [nu x nu x M x F] Either R of Y is wrong';
        if nut1~=nu
            error(Rerror);
        end
        if nut2~=nu
            error(Rerror);
        end
        if Mt~=M
            error(Rerror);
        end
        if Ft~=F
            error(Rerror);
        end
    end
    
end

