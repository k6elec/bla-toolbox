function [G, Y, U, exBins , CovYs , CovZ] = calculateSISO_BLA(spec,varargin)
% calculateSISO_BLA calculates the BLA using Rik's Robust_NL_Anal starting from ADS simulation data
% 
%      [G,Y,U,exBins,CovYs] = calculateSISO_BLA(spec);
%      [G,Y,U,exBins,CovYs] = calculateSISO_BLA(spec,'ParamName',paramValue,...);
% 
% 
% Required inputs:
% - spec      check: @isstruct
%      struct that contains the different MxPxF fields. It should at
%      least  contain the measured output and input in separate MxPxF
%      fields. You can  pass the result of ADSsimulateMS directly here.
% 
% Parameter/Value pairs:
% - 'input'     default: 'in'  check: @(x) all(isfield(spec,x))
%      Name of the field in the spec struct that contains the measured
%      input.  You can pass a cell array of fieldnames if you want
%      to estimate multiple  BLAs in one circuit.
% - 'output'     default: 'out'  check: @(x) all(isfield(spec,x))
%      Name of the field in the spec struct that contains the measured
%      output.  If you are estimating several BLAs in the same circuit,
%      make sure the  amount of outputs equals the amount of inputs
% - 'reference'     default: []  check: @(x) isfield(spec,x)
%      name of the field in the spec struct that contains the reference
%      signal.  This is usually the node where the multisine is connected
%      to. If this one  is left empty, the direct method to estimate
%      the BLA will be used.
% - 'exBins'     default: []  check: @isvector
%      list with the excited bins of the multisine. Use the indices
%      of the  matrix in Matlab (DC is 1, not zero). If this list is
%      not passed, the  excited bins are detected on the reference
%      signal (Only multisines with a  flat amplitude spectrum are
%      supported for the moment). If the reference  singal is not passed,
%      the measured input signal is used. This can  introduce errors.
% 
% Outputs:
% - G      Type: Struct
%      Contains the estimated BLA and the uncertainty. G.mean contains
%      the BLA at the excited bins, G.stdNL contains the standard deviation
%      on the BLA estimate due to non-linear distortion. See Robust_NL_Anal
%      for more information about the fields in this struct The frequency
%      axis is added to the G struct in G.freq
% - Y      Type: Struct
%      Contains the information about the BLA from reference to the
%      measured output. The fields are the same as with the BLA See
%      Robust_NL_Anal for more information about the fields in this
%      struct
% - U      Type: Struct
%      Contains the information about the BLA from reference to the
%      measured input.  See Robust_NL_Anal for more information about
%      the fields in this struct
% - exBins      Type: Vector
%      The detected excited bins.
% - CovYs      Type: Vector
%      The covariance matrix between the different distortion sources
%      in the circuit. This one is only calculated if more than one
%      BLA is estimated
% 
% Example:
%   % generate the multisine using the ExcitationDesign toolbox
%   MSdef = MScreate(1,100);
%   % add the reference node to the multisine
%   MSdef.MSnode = 'ref';
%   % run the simulation in ADS using the ADStoolbox
%   spec = ADSsimulateMS('temp.net',MSdef);
%   % determine the BLA with the function
%   G = calculateSISO_BLA(spec,'input','in','output','out','reference','ref');
%   % plot the result
%   plot(G.freq,G.mean);hold on;plot(G.freq,G.stdNL);
%    
%    
%   The fields in the different structs in the output are the following:
%   G - contains the information about the BLA from U to Y
%     mean        Best Linear Approximation
%     stdNL       standard deviation of the BLA due to non-linear distortion
%     stds        standard deviation due to noise
%     freq        frequency axis of the BLA
%     mean_interp linear interpolation of the BLA to the whole frequency band
%     freq_interp frequency axis of the interpolated BLA
%     Ys          rms of the non-linear distortion introduced by the system
%   
%   Y and U  - contain the following fields
%     mean        Best Linear Approximation from reference to output
%     abs         abs(Y.mean) or, if R is not available, rms(Y) at excited bins
%     stdNL       standard deviation of the BLA form R to Y due to non-linear distortion
%     stds        standard deviation of the BLA form R to Y due to noise
%     freq        frequency axis of the BLA from R to Y
%     disto       rms of the distortion found at Y. This has the same length as the spectrum
% 
% see also: ADSsimulateMS robust_NL_anal ADSsimulateMSwaves
% 
% Adam Cooman, ELEC, VUB
% 
% Version info:
%  16-12-2014 Version 1
%  09-02-2015 The function now calculates the distortion source and I Edited the help of the function
%  13-02-2015 Added the option to estimate several SISO blocks in one circuit
%  06-05-2015 Improved the help and fixed some bugs
% 
% This documentation was generated with the generateFunctionHelp function at 06-May-2015


p = inputParser();
% struct that contains the different MxPxF fields. It should at least
% contain the measured output and input in separate MxPxF fields. You can
% pass the result of ADSsimulateMS directly here.
p.addRequired('spec',@isstruct)
% Name of the field in the spec struct that contains the measured input.
% You can pass a cell array of fieldnames if you want to estimate multiple
% BLAs in one circuit.
p.addParamValue('input','in',@(x) all(isfield(spec,x)));
% Name of the field in the spec struct that contains the measured output.
% If you are estimating several BLAs in the same circuit, make sure the
% amount of outputs equals the amount of inputs
p.addParamValue('output','out',@(x) all(isfield(spec,x)));
% name of the field in the spec struct that contains the reference signal.
% This is usually the node where the multisine is connected to. If this one
% is left empty, the direct method to estimate the BLA will be used. 
p.addParamValue('reference',[],@(x) isfield(spec,x));
% list with the excited bins of the multisine. Use the indices of the
% matrix in Matlab (DC is 1, not zero). If this list is not passed, the
% excited bins are detected on the reference signal (Only multisines with a
% flat amplitude spectrum are supported for the moment). If the reference
% singal is not passed, the measured input signal is used. This can
% introduce errors.
p.addParamValue('exBins',[],@isvector);
p.CaseSensitive = false;
p.FunctionName = 'calculateSISO_BLA';
p.parse(spec,varargin{:});
args = p.Results;
clear spec varargin p

% make sure that the amount of inputs equals the amount of outputs
if ~iscell(args.input)
    args.input = {args.input};
end
if ~iscell(args.output)
    args.output = {args.output};
end
if length(args.input)~=length(args.output)
    error('The amount of provided inputs should be equal to the amount of provided outputs')
end
numBLAs = length(args.input);

% detect the size of the MxPxF vectors in the simulation
[M,P,F] = size(args.spec.(args.output{1}));

% if needed, look for the excited frequency bins
if isempty(args.exBins)
    if isempty(args.reference)
        warning('no reference signal provided and no excited bins given.\r I will try to use the input signal to determine the excited bins%s','')
        args.exBins = find(db(args.spec.(args.input{1})(1,1,:)>max(args.spec.(args.input{1})(1,1,:))-10));
    else
        % I go from 2:end because DC should be ignored at all time.
        args.exBins = find(db(squeeze(args.spec.(args.reference)(1,1,2:end)))>max(db(squeeze(args.spec.(args.reference)(1,1,2:end))))-10)+1;
    end
end

% if the reference is provided, just pass it on
if ~isempty(args.reference)
    R = args.spec.(args.reference)(:,:,args.exBins);
    if P==1
        R(:,2,:)=R(:,1,:);
    end
end

for ii=numBLAs:-1:1
    % get the input and output data out of the struct.
    Ysignal = args.spec.(args.output{ii})(:,end,args.exBins);
    Usignal = args.spec.(args.input{ii})(:,end,args.exBins);
    % Rik's function requires two periods, so just copy the last period
    Ysignal(:,2,:)=Ysignal(:,1,:);
    Usignal(:,2,:)=Usignal(:,1,:);
    % finally, calculate the actual BLA
    if ~isempty(args.reference)
        [G(ii), Y(ii), U(ii), CYU(ii)] = Robust_NL_Anal(Ysignal, Usignal, R);
    else
        [G(ii), Y(ii), U(ii), CYU(ii)] = Robust_NL_Anal(Ysignal, Usignal);
    end
end

% remove the useless noise fields from the result
G = rmfield(G,'stdn');
Y = rmfield(Y,'stdn');
U = rmfield(U,'stdn');
CYU = rmfield(CYU,'n');

% add the frequency axis to the BLA struct, for easy plotting
if isfield(args.spec,'freq')
    [G.freq] = deal(args.spec.freq(args.exBins));
    [U.freq] = deal(args.spec.freq(args.exBins));
    [Y.freq] = deal(args.spec.freq(args.exBins));
end

% get the bins that are inband
inBandBins = min(args.exBins):max(args.exBins);

%% interpolate the BLA to unexcited frequency bins. If the grid is full, this will not do anything
for ii=1:numBLAs
    G(ii).mean_interp = interp1(args.spec.freq(args.exBins).',G(ii).mean(:),args.spec.freq(inBandBins).').';
    Y(ii).mean_interp = interp1(args.spec.freq(args.exBins).',Y(ii).mean(:),args.spec.freq(inBandBins).').';
    U(ii).mean_interp = interp1(args.spec.freq(args.exBins).',U(ii).mean(:),args.spec.freq(inBandBins).').';
end

% frequency vector of the interpolated BLAs
[G.freq_interp] = deal(args.spec.freq(inBandBins));

% Ysbla contains the distortion added by the system itself
Ysbla = zeros(numBLAs,M,length(inBandBins));

% perform the correction Ys = Y - BLA * U
for ii=1:numBLAs
    % Ysy will contain the distortion at the output node. This uses the BLA from reference to output
    Ysy = squeeze(args.spec.(args.output{ii}));
    % Ysu will contain the distortion at the input node. This uses the BLA from reference to output
    Ysu = squeeze(args.spec.(args.input{ii}));
    for mm=1:M
        Ysbla(ii,mm,:) = squeeze(args.spec.(args.output{ii})(mm,1,inBandBins)) - G(ii).mean_interp.' .* squeeze(args.spec.(args.input{ii})(mm,1,inBandBins));
        temp = squeeze(args.spec.(args.reference)(mm,1,args.exBins))./abs(squeeze(args.spec.(args.reference)(1,1,args.exBins)));
        Ysy(mm,args.exBins) = Ysy(mm,args.exBins).' - (Y(ii).mean.') .* temp;
        Ysu(mm,args.exBins) = Ysu(mm,args.exBins).' - (U(ii).mean.') .* temp;
    end
    % calculate the rms of the distortion source(s), in function of frequency
    G(ii).Ys = squeeze(sqrt(mean(abs(Ysbla(ii,:,:)).^2,2)));
    Y(ii).disto = squeeze(sqrt(mean(abs(Ysy).^2,1)));
    U(ii).disto = squeeze(sqrt(mean(abs(Ysu).^2,1)));
end

%% if more than one BLA is estimated, calculate the covariance matrix of the distortion sources
% This is done in a two-step procedure. 
%   1. Calculate the covariance of the SIMO BLA from the reference to the
%       stacked output-input vectors
%   2. Multiply that covariance matrix by [I_N -G_BLA] on the left and right
%       where -G_BLA is the matrix that contains the BLAs on its diagonal
if numBLAs>1
    Zstilde = zeros(2*numBLAs,M,F);
    % the reference signal should exist on all the inband bins
    REF = squeeze(args.spec.(args.reference)(:,end,args.exBins));
    % do the distortion at the output signals first
    for ii=1:numBLAs
        Zstilde(ii,:,:) = squeeze(args.spec.(args.output{ii})(:,end,:));
        for mm=1:M
            % on the excited bins, substract the BLA from reference to the signal
            Zstilde(ii,mm,args.exBins) = squeeze(Zstilde(ii,mm,args.exBins))-(REF(mm,:).*Y(ii).mean./abs(REF(mm,:))).';
        end
    end
    % then do the distortion at the input signals
    for ii=1:numBLAs
        Zstilde(numBLAs+ii,:,:) = squeeze(args.spec.(args.input{ii})(:,end,:));
        for mm=1:M
            % on the excited bins, substract the BLA from reference to the signal
            Zstilde(numBLAs+ii,mm,args.exBins) = squeeze(Zstilde(numBLAs+ii,mm,args.exBins))-(REF(mm,:).*U(ii).mean./abs(REF(mm,:))).';
        end
    end
    
    % now calculate the covariance matrix of Zstilde
    CovZ = zeros(2*numBLAs,2*numBLAs,F);
    for ff=1:F
        CovZ(:,:,ff) = cov(squeeze(Zstilde(:,:,ff)).');
    end
    
    % finally, we can calculate the covariance matrix of Ys
    % We can only do this on the inBandBins, because that's where the BLA is known
    CovYs = zeros(numBLAs,numBLAs,length(inBandBins));
    for ff=1:length(inBandBins)
        % construct the matrix with the BLAs on its diagonal
        BLAdiag = zeros(numBLAs);
        for ii=1:numBLAs
            BLAdiag(ii,ii) = G(ii).mean_interp(ff);
        end
        % construct the matrix [In -GBLA]
        T = [eye(numBLAs) -BLAdiag];
        % multiply CZ on the left with T and on the right with herm(T)
        CovYs(:,:,ff) = T*CovZ(:,:,inBandBins(ff))*T';
    end
else
    CovZ  = [];
    CovYs = [];
end

% % Test to check all the different variances present in the system
% bins = [];
% for ee=1:length(args.exBins)
%     [~,bins(end+1)] = min(abs(args.spec.freq(args.exBins(ee))-args.spec.freq(inBandBins)));
% end
% plot(G(1).freq_interp(bins),db(rmsYs(:,bins)).','-')
% hold on
% % for mm=1:M
% %     plot(G(1).freq_interp(bins),db(squeeze(Ysbla(:,mm,bins))).','+')
% % end
% plot(G(1).freq_interp(bins),db(sqrt(diag_FRM(CovYs(:,:,bins)))).','s')
% for ii=1:numBLAs
%     plot(G(ii).freq,db(G(ii).stdNL),'p');
%     plot(Y(ii).freq,db(Y(ii).stdNL),'d');
% end
% keyboard

% assign exBins for the output
exBins = args.exBins;

end

% @generateFunctionHelp 
% @author Adam Cooman
% @institution ELEC, VUB
% @tagline calculates the BLA using Rik's Robust_NL_Anal starting from ADS simulation data

% @output1 Contains the estimated BLA and the uncertainty. G.mean contains the
% @output1 BLA at the excited bins, G.stdNL contains the standard deviation
% @output1 on the BLA estimate due to non-linear distortion.
% @output1 See Robust_NL_Anal for more information about the fields in this struct
% @output1 The frequency axis is added to the G struct in G.freq
% @outputType1 Struct

% @output2 Contains the information about the BLA from reference to the
% @output2 measured output. The fields are the same as with the BLA
% @output2 See Robust_NL_Anal for more information about the fields in this struct
% @outputType2 Struct

% @output3 Contains the information about the BLA from reference to the measured input. 
% @output3 See Robust_NL_Anal for more information about the fields in this struct
% @outputType3 Struct

% @output4 The detected excited bins.
% @outputType4 Vector

% @output5 The covariance matrix between the different distortion sources
% @output5 in the circuit. This one is only calculated if more than one BLA is estimated
% @outputType5 Vector


% @example % generate the multisine using the ExcitationDesign toolbox
% @example MSdef = MScreate(1,100);
% @example % add the reference node to the multisine
% @example MSdef.MSnode = 'ref';
% @example % run the simulation in ADS using the ADStoolbox
% @example spec = ADSsimulateMS('temp.net',MSdef);
% @example % determine the BLA with the function
% @example G = calculateSISO_BLA(spec,'input','in','output','out','reference','ref');
% @example % plot the result
% @example plot(G.freq,G.mean);hold on;plot(G.freq,G.stdNL);
% @example  
% @example  
% @example The fields in the different structs in the output are the following:
% @example G - contains the information about the BLA from U to Y
% @example   mean        Best Linear Approximation
% @example   stdNL       standard deviation of the BLA due to non-linear distortion
% @example   stds        standard deviation due to noise
% @example   freq        frequency axis of the BLA
% @example   mean_interp linear interpolation of the BLA to the whole frequency band
% @example   freq_interp frequency axis of the interpolated BLA
% @example   Ys          rms of the non-linear distortion introduced by the system
% @example
% @example Y and U  - contain the following fields
% @example   mean        Best Linear Approximation from reference to output
% @example   abs         abs(Y.mean) or, if R is not available, rms(Y) at excited bins
% @example   stdNL       standard deviation of the BLA form R to Y due to non-linear distortion
% @example   stds        standard deviation of the BLA form R to Y due to noise
% @example   freq        frequency axis of the BLA from R to Y
% @example   disto       rms of the distortion found at Y. This has the same length as the spectrum

% @seealso ADSsimulateMS
% @seealso robust_NL_anal
% @seealso ADSsimulateMSwaves
% @version 16-12-2014 Version 1
% @version 09-02-2015 The function now calculates the distortion source and I Edited the help of the function
% @version 13-02-2015 Added the option to estimate several SISO blocks in one circuit
% @version 06-05-2015 Improved the help and fixed some bugs

%% generateFunctionHelp: old help, backed up at 06-May-2015. leave this at the end of the function
% calculateSISO_BLA calculates the BLA using Rik's Robust_NL_Anal starting from ADS simulation data
% 
%      [G,Y,U,exBins,covYs] = calculateSISO_BLA(spec);
%      [G,Y,U,exBins,covYs] = calculateSISO_BLA(spec,'ParamName',paramValue,...);
% 
% 
% Required inputs:
% - spec      check: @isstruct
%      struct that contains the different MxPxF fields. It should at
%      least  contain the measured output and input in separate MxPxF
%      fields. You can  pass the result of ADSsimulateMS directly here.
% 
% Parameter/Value pairs:
% - 'input'     default: 'in'  check: @(x) all(isfield(spec,x))
%      Name of the field in the spec struct that contains the measured
%      input.  You can pass a cell array of fieldnames if you want
%      to estimate multiple  BLAs in one circuit.
% - 'output'     default: 'out'  check: @(x) all(isfield(spec,x))
%      Name of the field in the spec struct that contains the measured
%      output.  If you are estimating several BLAs in the same circuit,
%      make sure the  amount of outputs equals the amount of inputs
% - 'reference'     default: []  check: @(x) isfield(spec,x)
%      name of the field in the spec struct that contains the reference
%      signal.  This is usually the node where the multisine is connected
%      to. If this one  is left empty, the direct method to estimate
%      the BLA will be used.
% - 'exBins'     default: []  check: @isvector
%      list with the excited bins of the multisine. Use the indices
%      of the  matrix in Matlab (DC is 1, not zero). If this list is
%      not passed, the  excited bins are detected on the reference
%      signal (Only multisines with a  flat amplitude spectrum are
%      supported for the moment). If the reference  singal is not passed,
%      the measured input signal is used. This can  introduce errors.
% 
% Outputs:
% - G      Type: Struct
%      Contains the estimated BLA and the uncertainty. G.mean contains
%      the BLA at the excited bins, G.stdNL contains the standard deviation
%      on the BLA estimate due to non-linear distortion. See Robust_NL_Anal
%      for more information about the fields in this struct The frequency
%      axis is added to the G struct in G.freq
% - Y      Type: Struct
%      Contains the information about the BLA from reference to the
%      measured output. The fields are the same as with the BLA See
%      Robust_NL_Anal for more information about the fields in this
%      struct
% - U      Type: Struct
%      Contains the information about the BLA from reference to the
%      measured input.  See Robust_NL_Anal for more information about
%      the fields in this struct
% - exBins      Type: Vector
%      The detected excited bins.
% - covYs      Type: Vector
%      The covariance matrix between the different distortion sources
%      in the circuit. This one is only calculated if more than one
%      BLA is estimated
% 
% Example:
%   % generate the multisine using the ExcitationDesign toolbox
%   MSdef = MScreate(1,100);
%   % add the reference node to the multisine
%   MSdef.MSnode = 'ref';
%   % run the simulation in ADS using the ADStoolbox
%   spec = ADSsimulateMS('temp.net',MSdef);
%   % determine the BLA with the function
%   G = calculateSISO_BLA(spec,'input','in','output','out','reference','ref');
%   % plot the result
%   plot(G.freq,G.mean);hold on;plot(G.freq,G.stdNL);
% 
% see also: ADSsimulateMS robust_NL_anal ADSsimulateMSwaves
% 
% Adam Cooman, ELEC, VUB
% 
% Version info:
%  16-12-2014 Version 1
%  09-02-2015 The function now calculates the distortion source and I Edited the help of the function
%  13-02-2015 Added the option to estimate several SISO blocks in one circuit
% 
% This documentation was generated with the generateFunctionHelp function at 14-Apr-2015
